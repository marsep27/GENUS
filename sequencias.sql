create sequence seq_Persona
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Correo
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Rol
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;

create sequence seq_Tamanno
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Foto
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Color
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Pais
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Provincia
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Canton
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Distrito
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_ZonaAvistamiento
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Clase
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Orden
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_SubOrden
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;

create sequence seq_Familia
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Genero
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Especie
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Bitacora
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Mensaje
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;
    
create sequence seq_Reporte
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;