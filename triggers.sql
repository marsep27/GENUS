-------------------------------Triggers Auditoria-------------------------------
------------------------------Tabla Avistamiento--------------------------------
Create or Replace Trigger administrador.beforeInsertAvistamiento
Before Insert
on administrador.avistamiento
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertAvistamiento;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateAvistamiento
Before Update
on administrador.avistamiento
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateAvistamiento;
/
-----------------------------Fin Tabla Avistamiento-----------------------------
-----------------------------Tabla Bitacora-------------------------------------
create or replace trigger administrador.beforeUpdatePassword
after insert or update of contrasena
on administrador.credencial
for each row
Begin
  insert into bitacora
         (id_bitacora,
         contrasenanueva,contrasenaanterior,usuario,Fec_Ultima_Modificacion,
         Usuario_Ultima_Modificacion)
  values(seq_bitacora.nextval,:new.contrasena, :old.contrasena,
        administrador.PAQ_PERSONA.getUsuarioActual,SYSDATE,USER);
end beforeUpdateSalary;
/
-----------------------------Fin Tabla Bitacora---------------------------------
-----------------------------Inicio Tabla Canton--------------------------------
Create or Replace Trigger administrador.beforeInsertCanton
Before Insert
on administrador.canton
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertCanton;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateCanton
Before Update
on administrador.canton
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateCanton;
/
-----------------------------Fin Tabla Canton-----------------------------------
-----------------------------Inicio Tabla Clase---------------------------------
Create or Replace Trigger administrador.beforeInsertClase
Before Insert
on administrador.clase
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertClase;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateClase
Before Update
on administrador.clase
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateClase;
/
-----------------------------Fin Tabla Clase------------------------------------
-----------------------------Inicio Tabla Color---------------------------------
Create or Replace Trigger administrador.beforeInsertColor
Before Insert
on administrador.color
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertColor;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateColor
Before Update
on administrador.color
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateColor;
/
-----------------------------Fin Tabla Color------------------------------------
-----------------------------Inicio Tabla Correo--------------------------------
Create or Replace Trigger administrador.beforeInsertCorreo
Before Insert
on administrador.correo
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertCorreo;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateCorreo
Before Update
on administrador.correo
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateCorreo;
/
-----------------------------Fin Tabla Correo-----------------------------------
-----------------------------Inicio Tabla Credencial----------------------------
Create or Replace Trigger administrador.beforeInsertCredencial
Before Insert
on administrador.credencial
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertCredencial;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateCredencial
Before Update
on administrador.credencial
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateCredencial;
/
-----------------------------Fin Tabla Credencial-------------------------------
-----------------------------Inicio Tabla Distrito------------------------------
Create or Replace Trigger administrador.beforeInsertDistrito
Before Insert
on administrador.distrito
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertDistrito;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateDistrito
Before Update
on administrador.distrito
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateDistrito;
/
-----------------------------Fin Tabla Distrito---------------------------------
-----------------------------Inicio Tabla Especie-------------------------------
Create or Replace Trigger administrador.beforeInsertEspecie
Before Insert
on administrador.especie
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertEspecie;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateEspecie
Before Update
on administrador.especie
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateEspecie;
/
-----------------------------Fin Tabla Especie----------------------------------
-----------------------------Inicio Tabla Familia-------------------------------
Create or Replace Trigger administrador.beforeInsertFamilia
Before Insert
on administrador.familia
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertEspecie;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateFamilia
Before Update
on administrador.familia
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateFamilia;
/
-----------------------------Fin Tabla Familia----------------------------------
-----------------------------Inicio Tabla Foto----------------------------------
Create or Replace Trigger administrador.beforeInsertFoto
Before Insert
on administrador.foto
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertFoto;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateFoto
Before Update
on administrador.foto
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateFoto;
/
-----------------------------Fin Tabla Foto-------------------------------------
-----------------------------Inicio Tabla Genera_Avistamiento-------------------
Create or Replace Trigger administrador.beforeInsertGeneraAvistamiento
Before Insert
on administrador.generaAvistamiento
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertGeneraAvistamiento;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateGeneraAvistamiento
Before Update
on administrador.generaAvistamiento
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateGeneraAvistamiento;
/
-----------------------------Fin Tabla Genera_Avistamiento----------------------
-----------------------------Inicio Tabla Genero--------------------------------
Create or Replace Trigger administrador.beforeInsertGenero
Before Insert
on administrador.genero
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertGenero;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateGenero
Before Update
on administrador.genero
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateGenero;
/
-----------------------------Fin Tabla Genero-----------------------------------
-----------------------------Inicio Tabla Muestra_Avistamiento------------------
Create or Replace Trigger administrador.beforeInsertMuestraAvis
Before Insert
on administrador.muestraAvistamiento
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertMuestraAvis;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateMuestraAvis
Before Update
on administrador.muestraAvistamiento
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateMuestraAvis;
/
-----------------------------Fin Tabla Muestra_Avistamiento---------------------
-----------------------------Inicio Tabla Orden---------------------------------
Create or Replace Trigger administrador.beforeInsertOrden
Before Insert
on administrador.orden
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertOrden;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateOrden
Before Update
on administrador.orden
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateOrden;
/
-----------------------------Fin Tabla Orden------------------------------------
-----------------------------Inicio Tabla Pais----------------------------------
Create or Replace Trigger administrador.beforeInsertPais
Before Insert
on administrador.pais
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertPais;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdatePais
Before Update
on administrador.pais
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdatePais;
/
-----------------------------Fin Tabla Pais-------------------------------------
-----------------------------Inicio Tabla Parametro_Notificacion----------------
Create or Replace Trigger administrador.beforeInsertParametros
Before Insert
on administrador.parametroNotificacion
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertParametros;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateParametros
Before Update
on administrador.parametroNotificacion
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateParametros;
/
-----------------------------Fin Tabla Parametro_Notificacion-------------------
-----------------------------Inicio Tabla Persona-------------------------------
Create or Replace Trigger administrador.beforeInsertPersona
Before Insert
on administrador.persona
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertPersona;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdatePersona
Before Update
on administrador.persona
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdatePersona;
/
-----------------------------Fin Tabla Persona----------------------------------
-----------------------------Inicio Tabla Provincia-------------------------------
Create or Replace Trigger administrador.beforeInsertProvincia
Before Insert
on administrador.provincia
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertProvincia;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateProvincia
Before Update
on administrador.provincia
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateProvincia;
/
-----------------------------Fin Tabla Provincia--------------------------------
-----------------------------Inicio Tabla Rol-----------------------------------
Create or Replace Trigger administrador.beforeInsertRol
Before Insert
on administrador.rol
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertRol;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateRol
Before Update
on administrador.rol
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateRol;
/
-----------------------------Fin Tabla Rol-------------------
-----------------------------Inicio Tabla Sub_Orden-------------------------------
Create or Replace Trigger administrador.beforeInsertSub_Orden
Before Insert
on administrador.sub_Orden
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertSub_Orden;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateSub_Orden
Before Update
on administrador.sub_Orden
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateSub_Orden;
/
-----------------------------Fin Tabla Sub_Orden--------------------------------
-----------------------------Inicio Tabla Tamanno-------------------------------
Create or Replace Trigger administrador.beforeInsertTamanno
Before Insert
on administrador.tamanno
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertTamanno;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateTamanno
Before Update
on administrador.tamanno
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateTamanno;
/
-----------------------------Fin Tabla Tamanno----------------------------------
-----------------------------Inicio Tabla Zona_Avistamiento---------------------
Create or Replace Trigger administrador.beforeInsertZonaAvistamiento
Before Insert
on administrador.zona_Avistamiento
for each row 
Begin 
  :new.Usuario_creacion:=USER;
  :new.Fec_creacion:=SYSDATE;
End beforeInsertZonaAvistamiento;
/
--------------------------------------------------------------------------------
Create or Replace Trigger administrador.beforeUpdateZonaAvistamiento
Before Update
on administrador.zona_Avistamiento
for each row 
Begin 
  :new.Usuario_ultima_modificacion:=USER;
  :new.Fec_ultima_modificacion:=SYSDATE;
End beforeUpdateZonaAvistamiento;
/
-----------------------------Fin Tabla Zona_Avistamiento------------------------