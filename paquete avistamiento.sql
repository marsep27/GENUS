-------------------------------Inicio Encabezado--------------------------------
create or replace package paq_avistamiento is
    
    avistamientoGlobal number(9) := null;
    extincioncheck char(1);
    
    function get_Clase return record_nom;
    function get_Orden(pID number) return record_nom;
    function get_SubOrden(pID in number) return record_nom;
    function get_Familia(pID in number) return record_nom;
    function get_Genero(pID in number) return record_nom;
    function get_Especie(pID in number) return record_nom;
    function get_Color(pID in number) return record_nom;
    function get_Tamanno(pID in number) return record_nom;
    
    function get_Pais return record_nom;
    function get_Provincia(pID number) return record_nom;
    function get_Canton(pID number) return record_nom;
    function get_Distrito(pID number) return record_nom;
    function get_ZonaAvistamiento(pID number) return record_nom;
    
    Function creaAvistamiento(pClase varchar2,pOrden varchar2, pSubOrden varchar2,
                pFamilia varchar2, pGenero varchar2, pEspecie varchar2,
                pID_ZonaAvistamiento number) return number;
                
    procedure generaReporte(pID_Persona number, pID_Reporte number);
    procedure muestraReporte(pID_Especie number, pID_Reporte number);
end;
/
-------------------------------Fin Encabezado-----------------------------------
-------------------------------Inicio Cuerpo------------------------------------
Create or replace package body paq_avistamiento as
        
    function get_Clase return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select nombre from clase ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Orden(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
        if (pID != 0) then
            for dato in (select nombre from orden where id_clase = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        else 
            for dato in (select nombre from orden) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        end if;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_SubOrden(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
        if (pID != 0) then
            for dato in (select nombre from sub_orden where id_orden = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        else
            for dato in (select nombre from sub_orden) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        end if;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Familia(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
        if (pID != 0) then
            for dato in (select nombre from familia where ID_SUBORDEN = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        else 
            for dato in (select nombre from familia) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        end if;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Genero(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
        if (pID != 0) then 
            for dato in (select nombre from genero where ID_FAMILIA = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        else 
            for dato in (select nombre from genero) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        end if;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Especie(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
        if(pID != 0) then
            for dato in (select nombre from Especie where ID_GENERO = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        else
            for dato in (select nombre from Especie) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
            end loop;
        end if;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Color(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select DESCRIPCION from color where ID_ESPECIE = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.descripcion)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Tamanno(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select DESCRIPCION from tamanno where ID_ESPECIE = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.descripcion)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    function get_Pais return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select nombre from Pais ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Provincia(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select nombre from PROVINCIA where ID_PAIS = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Canton(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select nombre from CANTON where ID_PROVINCIA = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_Distrito(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select nombre from DISTRITO where ID_CANTON = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombre)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
   function get_ZonaAvistamiento(pID in number) return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select NOMBRELUGAR from ZONA_AVISTAMIENTO where ID_DISTRITO = pID ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.nombrelugar)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function creaAvistamiento(pClase varchar2,pOrden varchar2, pSubOrden varchar2,
                pFamilia varchar2, pGenero varchar2, pEspecie varchar2,
                pID_ZonaAvistamiento number) return number is 
    temp_id_persona number := paq_persona.getIDPersonaActual;
    temp_id_especie number;
    temp_id_reporte number;
    begin 
        select seq_reporte.nextval
        into temp_id_reporte
        from dual;
        
        paq_avistamiento.avistamientoGlobal := temp_id_reporte;
        
        select ID_especie
        into temp_id_especie
        from especie
        where nombre = pEspecie;
        
        insert into avistamiento(ID_reporte,clase_reporte,orden_reporte,
                    suborden_reporte,familia_reporte,genero_reporte,
                    especie_reporte,Id_zonaavistamiento)
        values(temp_id_reporte,pClase,pOrden,pSubOrden,pFamilia,pGenero,pEspecie,
                pID_ZonaAvistamiento);
        commit;
        muestraReporte(temp_id_especie,temp_id_reporte);
        generaReporte(temp_id_persona,temp_id_reporte);
    end;
--------------------------------------------------------------------------------
    procedure generaReporte(pID_Persona number, pID_Reporte number)
    is
    begin
    insert into generaAvistamiento(ID_Persona,ID_Reporte)
        values(pID_Persona,pID_Reporte);
        commit;
    end;
    
    procedure muestraReporte(pID_Especie number, pID_Reporte number)
    is
    begin
    insert into muestraAvistamiento(ID_reporte,ID_especie)
        values(pID_Reporte,pID_Especie);
        commit;
    end;
end;
/
-------------------------------Fin Cuerpo---------------------------------------