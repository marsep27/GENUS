-- Conectado desde System
CREATE USER administrador   
       IDENTIFIED BY administrador
       DEFAULT TABLESPACE Admin_Data
       QUOTA 10M ON Admin_Data
       TEMPORARY TABLESPACE temp
       QUOTA 5M ON system;
       
GRANT CONNECT TO administrador;

grant create any index to administrador;

grant create public synonym to administrador;

grant unlimited tablespace to administrador;

Grant create table to administrador;

Grant create sequence to administrador;

Grant create procedure to administrador;

grant create trigger to administrador;

grant create view to administrador;