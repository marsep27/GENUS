Create table Clase(
ID_Clase number(9),
nombre Varchar2(500) constraint clase_nombre_nn not null,
nombreIngles varchar2(500) constraint clase_nombreIngles_nn not null
);

Create table Orden(
ID_Orden number(9),
nombre Varchar2(500) constraint orden_nombre_nn not null,
ID_Clase number(9) constraint Fk_orden_IDClase_nn not null
);

Create table Sub_Orden(
ID_SubOrden number(9),
nombre Varchar2(500)constraint subOrden_nombre_nn not null,
ID_Orden number(9) constraint Fk_suborden_IDOrden_nn not null
);

Create table Familia(
ID_Familia number(9),
nombre Varchar2(500) constraint familia_nombre_nn not null,
ID_SubOrden number(9) constraint Fk_familia_IDSubOrd_nn not null
);

Create table Genero(
ID_Genero number(9),
nombre Varchar2(500) constraint genero_nombre_nn not null,
ID_Familia number(9) constraint Fk_genero_IDFamilia_nn not null
);

Create table Especie(
ID_Especie number(9),
nombre Varchar2(500) constraint especie_nombre_nn not null,
extincion char(1) constraint especie_extincion_nn not null,
ID_Genero number(9) constraint Fk_especie_IDGenero_nn not null
);

Create table Color(
ID_Color number(9),
descripcion Varchar2(500) constraint color_descripcion_nn not null,
ID_Especie number(9) constraint Fk_Color_IDEspecie_nn not null
);

Create table Tamanno(
ID_Tamanno number(9),
descripcion Varchar2(500) constraint tamanno_descripcion_nn not null,
ID_Especie number(9) constraint Fk_tamanno_IDEspecie_nn not null
);

create table Foto(
ID_Foto Number(9),
imagen VARCHAR2(4000 BYTE) constraint foto_imagen_nn not null
);

create table rol(
ID_Rol  number(9),
descripcion varchar2(500) constraint rol_descripcion_nn not null,
                          constraint rol_descripcion_uk unique(Descripcion)
);

Create table Persona(
ID_Persona number(9),
nombre varchar2(500) constraint persona_nombre_nn not null,
primerApellido varchar2(500) constraint persona_primerApellido_nn not null,
segundoApellido varchar2(500) constraint persona_segundoApellido_nn not null,
fecha_Nacimiento date constraint persona_fechanacimiento_nn not null,
profesion varchar2(500),
ID_Foto NUMBER(9) constraint Fk_Persona_IDFoto_nn not null,
ID_Rol number(9) constraint Fk_Persona_IDRol_nn not null
);

create table Credencial(
usuario varchar(500),
contrasena varchar(500) constraint credencial_contrasena_nn not null,
ID_Persona number(9) constraint Fk_Credencial_IDPersona_nn not null
);

Create table Bitacora(
ID_Bitacora number(9),
contrasenaNueva varchar2(500),
contrasenaAnterior varchar2(500),
usuario varchar2(500) constraint Fk_Bitacora_Usuario_nn not null
);

create table correo(
ID_correo number(9),
direccion varchar2(500) constraint correo_direccion_nn not null,
                        constraint correo_direccion_uk unique(direccion),                        
ID_Persona number(9) constraint Fk_Correo_IDPersona_nn not null
);

create table Pais(
ID_Pais number(9),
nombre varchar2(500) constraint pais_nombre_nn not null,
                     constraint pais_nombre_uk unique(nombre)                        
);

create table Provincia(
ID_Provincia number(9),
nombre varchar2(500) constraint provincia_nombre_nn not null,
                     constraint provincia_nombre_uk unique(nombre),                          
ID_Pais number(9) constraint Fk_provincia_IDPais_nn not null
);

create table Canton(
ID_Canton number(9),
nombre varchar2(500) constraint canton_nombre_nn not null,
                     constraint canton_nombre_uk unique(nombre),                       
ID_Provincia number(9) constraint Fk_Canton_IDProvincia_nn not null
);

create table Distrito(
ID_Distrito number(9),
nombre varchar2(500) constraint distrito_nombre_nn not null,
                     constraint distrito_nombre_uk unique(nombre),
ID_Canton number(9) constraint Fk_Distrito_IDCanton_nn not null
);

create table Zona_Avistamiento(
ID_ZonaAvistamiento number(9),
nombreLugar varchar2(500) constraint Zona_nombreLugar_nn not null,
                          constraint Zona_nombreLugar_uk unique(nombreLugar),
ID_Distrito number(9) constraint Fk_Zona_IDDistrico_nn not null
);

create table Avistamiento(
ID_Reporte number(9),
clase_reporte varchar(500) constraint avista_claseReporte_nn not null,
orden_reporte varchar(500) constraint avista_ordenReporte_nn not null,
subOrden_reporte varchar(500) constraint avista_subOrdenReporte_nn not null,
familia_reporte varchar(500) constraint avista_familiaReporte_nn not null,
genero_reporte varchar(500) constraint avista_generoReporte_nn not null,
especie_reporte varchar(500) constraint avista_especieReporte_nn not null,
ID_Foto number(9) constraint Fk_Avista_IDFoto_nn not null,
ID_ZonaAvistamiento number(9) constraint Fk_Avista_IDZona_nn not null
);

create table GeneraAvistamiento(
ID_Persona number(9) constraint Fk_GeneraAvis_IDPersona_nn not null,
ID_Reporte number(9) constraint Fk_GeneraAvis_IDReporte_nn not null
);

Create table MuestraAvistamiento(
ID_Reporte number(9) constraint Fk_MuestraAvis_IDReporte_nn not null,
ID_Especie number(9) constraint Fk_MuestraAvis_IDEspecie_nn not null
);

create table ParametroNotificacion(
ID_Mensaje number(9),
parametro varchar2(500) constraint notificacion_parametro_nn not null,
valor varchar2(4000 byte) constraint notificacion_valor_nn not null
);

