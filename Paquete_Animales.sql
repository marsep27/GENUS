-------------------------------Inicio Encabezado--------------------------------
create or replace package paq_animales is
    Function insertarClase(pNombre in varchar2) return number;
    Function insertarOrden(pOrden in varchar2, pID_Clase in number) return number;
    Function insertarSub_Orden(pSubOrden in varchar2, pID_Orden in number) return number;
    Function insertarFamilia(pFamilia in varchar2, pID_SubOrden in number) return number;
    Function insertarGenero(pGenero in varchar2, pID_Familia in number) return number;
    Function insertarEspecie(pEspecie in varchar2, pExtincion in char,
                            pID_Genero in number) return number;
    
    Function getIDClase(pNombre in varchar2) return number;
    Function getIDOrden(pNombre in varchar2) return number;
    Function getIDSub_Orden(pNombre in varchar2) return number;
    Function getIDFamilia(pNombre in varchar2) return number;
    Function getIDGenero(pNombre in varchar2) return number;
    Function getIDEspecie(pNombre in varchar2) return number;
    
    Function updateClaseNombre(pNombre in varchar2, pID_Cambio in number) return number;
    Function updateOrden(pNombre in varchar2, pID_Cambio in number) return number;
    Function updateSubOrden(pNombre in varchar2, pID_Cambio in number) return number;
    Function updateFamilia(pNombre in varchar2, pID_Cambio in number) return number;
    Function updateGenero(pNombre in varchar2, pID_Cambio in number) return number;
    Function updateEspecie(pNombre in varchar2, pID_Cambio in number) return number;
end;
/
-------------------------------Fin Encabezado-----------------------------------
-------------------------------Inicio Cuerpo------------------------------------
Create or replace package body paq_animales as
    Function insertarClase(pNombre in varchar2) return number is
    cod_clase number;
    begin
        Select seq_clase.nextval 
        into cod_clase
        from dual;
        
        Insert into clase(ID_Clase,Nombre)
        values (cod_clase,upper(pNombre));
        commit;
        return cod_clase;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function insertarOrden(pOrden in varchar2, pID_Clase in number) 
    return number is
    cod_orden number;
    begin
        Select seq_orden.nextval 
        into cod_orden
        from dual;
        
        Insert into Orden(ID_Orden,Nombre, ID_clase)
        values (cod_orden,upper(pOrden),pID_Clase);
        commit;
        return cod_orden;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function insertarSub_Orden(pSubOrden in varchar2, pID_Orden in number) 
    return number is
    cod_SubOrden number;
    begin
        Select seq_SubOrden.nextval 
        into cod_SubOrden
        from dual;
        
        Insert into sub_Orden(ID_SubOrden,Nombre,ID_Orden)
        values (cod_SubOrden,upper(pSubOrden),pID_Orden);
        commit;
        return cod_SubOrden;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function insertarFamilia(pFamilia in varchar2, pID_SubOrden in number) 
    return number is
    cod_familia number;
    begin
        Select seq_familia.nextval 
        into cod_familia
        from dual;
        
        Insert into familia(ID_Familia,Nombre, ID_SubOrden)
        values (cod_familia,upper(pFamilia),pID_SubOrden);
        commit;
        return cod_familia;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function insertarGenero(pGenero in varchar2, pID_Familia in number) 
    return number is
    cod_Genero number;
    begin
        Select seq_Genero.nextval 
        into cod_Genero
        from dual;
        
        Insert into genero(ID_Genero,Nombre, ID_Familia)
        values (cod_Genero,upper(pGenero),pID_Familia);
        commit;
        return cod_Genero;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function insertarEspecie(pEspecie in varchar2, pExtincion in char,
                            pID_Genero in number) return number is
    cod_Especie number;
    begin
        Select seq_Especie.nextval 
        into cod_Especie
        from dual;
        
        Insert into Especie(ID_Especie,Nombre, Extincion,ID_Genero)
        values (cod_Especie,upper(pEspecie),pExtincion,pID_Genero);
        commit;
        return cod_Especie;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function getIDClase(pNombre in varchar2) return number is
    temp_id number;
    begin 
        Select ID_Clase
        into temp_id
        from Clase
        where (Nombre = upper(pNombre));
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDOrden(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Orden
        into temp_id
        from Orden
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDSub_Orden(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_SubOrden
        into temp_id
        from Sub_Orden
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDFamilia(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Familia
        into temp_id
        from Familia
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDGenero(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Genero
        into temp_id
        from Genero
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDEspecie(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Especie
        into temp_id
        from Especie
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function updateClaseNombre(pNombre in varchar2, pID_Cambio in number) 
    return number is
    Begin
        UPDATE Clase
        SET nombre = upper(pNombre)
        WHERE id_Clase = pID_Cambio;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function updateOrden(pNombre in varchar2, pID_Cambio in number) 
    return number is
    Begin
        UPDATE orden
        SET nombre = upper(pNombre)
        WHERE id_Orden = pID_Cambio;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------   
    Function updateSubOrden(pNombre in varchar2, pID_Cambio in number) 
    return number is
    Begin
        UPDATE Sub_Orden
        SET nombre = upper(pNombre)
        WHERE id_SubOrden = pID_Cambio;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------   
    Function updateFamilia(pNombre in varchar2, pID_Cambio in number) 
    return number is
    Begin
        UPDATE Familia
        SET nombre = upper(pNombre)
        WHERE id_Familia = pID_Cambio;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------    
    Function updateGenero(pNombre in varchar2, pID_Cambio in number) 
    return number is
    Begin
        UPDATE Genero
        SET nombre = upper(pNombre)
        WHERE id_Genero = pID_Cambio;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------   
    Function updateEspecie(pNombre in varchar2, pID_Cambio in number) 
    return number is
    Begin
        UPDATE Especie
        SET nombre = upper(pNombre)
        WHERE id_Especie = pID_Cambio;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------    
end;
/
-------------------------------Fin Cuerpo---------------------------------------