-------------------------------Inicio Encabezado--------------------------------
create or replace package paq_reporte is
    procedure reporte_cumpleannos;
    Function consulta_cumpleanos return record_cumpleannos;
    Function consulta_Extincion return record_extincion;
end;
/
-------------------------------Fin Encabezado-----------------------------------
-------------------------------Inicio Cuerpo------------------------------------
create or replace package body paq_reporte as
    procedure reporte_cumpleannos is 
        cont_cumpleannos number;
        fecha_actual varchar2(30);
        fecha_cumpleannos varchar2(30); 
    begin
        select to_char(sysdate,'DD/MM')
        into fecha_actual
        from dual;
        for dato in(
            select pers.NOMBRE,pers.PRIMERAPELLIDO,pers.SegundoApellido, cred.Usuario,pers.FECHA_NACIMIENTO
            from persona pers
            inner join CREDENCIAL cred
            on pers.ID_PERSONA = cred.ID_PERSONA
            where to_char(pers.Fecha_Nacimiento,'DD/MM') = fecha_actual) 
        loop
            Select seq_cumpleannos.nextval
            into cont_cumpleannos
            from dual;
            
            INSERT INTO cumpleannos (id_cumpleannos,nombre,primer_apellido,
            segundo_apellido,usuario,"FECHA_CUMPLEAŅOS")
            VALUES (cont_cumpleannos,dato.NOMBRE,dato.PRIMERAPELLIDO,
            dato.SegundoApellido, dato.Usuario,dato.FECHA_NACIMIENTO);
        end loop;
    end;
--------------------------------------------------------------------------------  
    Function consulta_cumpleanos return record_cumpleannos
    is
       lista_nombres  record_cumpleannos := record_cumpleannos();
    begin
            for dato in (
            select nombre,primer_apellido,segundo_apellido,USUARIO,"FECHA_CUMPLEAŅOS" fecha
            from cumpleannos
            order by nombre asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_cumpleannos(dato.nombre,
                dato.primer_apellido,dato.segundo_apellido,
                dato.usuario,to_char(dato.fecha,'DD/MM/YYYY'))) ;
       end loop;
        return lista_nombres;
    end;
--------------------------------------------------------------------------------
    Function consulta_Extincion return record_extincion
    is
       lista_nombres  record_extincion := record_extincion();
    begin
            for dato in (
            select clase,orden,suborden,familia,genero,especie,pais,provincia,
            canton,distrito,zona,nombre,primer_apellido,segundo_apellido,correo
            from REPORTEEXTINCION
            order by nombre asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_extincion(dato.clase, 
                dato.orden,dato.suborden, dato.familia,dato.genero,dato.especie, 
                dato.pais,dato.provincia,dato.canton,dato.distrito,dato.Zona, 
                dato.nombre,dato.Primer_Apellido, dato.segundo_apellido, dato.correo));
       end loop;
        return lista_nombres;
    end;
end;
/
-------------------------------Fin Cuerpo---------------------------------------