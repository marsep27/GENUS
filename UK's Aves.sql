ALTER TABLE Clase
      ADD  (constraint Clase_nombre_uk unique(nombre),
           constraint Clase_nombreIngles_uk unique(nombreingles));
           
ALTER TABLE Orden
      ADD  constraint Orden_nombre_uk unique(nombre);

ALTER TABLE Sub_Orden
      ADD  constraint SubOrden_nombre_uk unique(nombre);
      
ALTER TABLE Familia
      ADD  constraint Familia_nombre_uk unique(nombre);
      
ALTER TABLE Genero
      ADD  constraint Genero_nombre_uk unique(nombre);
      
ALTER TABLE Especie
      ADD  constraint Especie_nombre_uk unique(nombre);
      
ALTER TABLE Tamanno
      ADD  constraint Tamanno_Descripcion_uk unique(Descripcion);
    
ALTER TABLE Color
      ADD  constraint Color_Descripcion_uk unique(Descripcion);