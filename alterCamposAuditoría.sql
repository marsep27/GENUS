---------------------------Inicio Alter table Auditoria-------------------------
ALTER TABLE avistamiento
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE bitacora
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE canton
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE clase
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE color
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE correo
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE credencial
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE distrito
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE especie
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE familia
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE foto
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE generaAvistamiento
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE genero
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE muestraAvistamiento
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE orden
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE pais
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE parametronotificacion
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE persona
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE provincia
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE rol
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE sub_orden
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE tamanno
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
--------------------------------------------------------------------------------
ALTER TABLE zona_avistamiento
ADD
(Fec_creacion DATE,
Usuario_creacion VARCHAR2(500),
Fec_ultima_modificacion DATE,
Usuario_ultima_modificacion VARCHAR2(500));
-----------------------------Fin Alter table Auditoria-------------------------- 