alter table Orden
    modify ID_Clase number(9) constraint Fk_orden_IDClase_nn not null;
    
alter table Sub_Orden
    modify ID_Orden number(9) constraint Fk_suborden_IDOrden_nn not null;
    
alter table Familia
    modify ID_SubOrden number(9) constraint Fk_familia_IDSubOrd_nn not null;
    
alter table Genero
    modify ID_Familia number(9) constraint Fk_genero_IDFamilia_nn not null;
    
alter table Especie
    modify ID_Genero number(9) constraint Fk_especie_IDGenero_nn not null;
    
alter table Color
    modify ID_Especie number(9) constraint Fk_Color_IDEspecie_nn not null;
    
alter table Tamanno
    modify ID_Especie number(9) constraint Fk_tamanno_IDEspecie_nn not null;
    
alter table Persona
    modify (ID_Foto NUMBER(9) constraint Fk_Persona_IDFoto_nn not null,
           ID_Rol number(9) constraint Fk_Persona_IDRol_nn not null);
    
alter table Credencial
    modify ID_Persona number(9) constraint Fk_Credencial_IDPersona_nn not null;
    
alter table Bitacora
    modify usuario varchar2(500) constraint Fk_Bitacora_Usuario_nn not null;
    
alter table Correo
    modify ID_Persona number(9) constraint Fk_Correo_IDPersona_nn not null;
    
alter table Provincia
    modify ID_Pais number(9) constraint Fk_provincia_IDPais_nn not null;
    
alter table Canton
    modify ID_Provincia number(9) constraint Fk_Canton_IDProvincia_nn not null;
    
alter table Distrito
    modify ID_Canton number(9) constraint Fk_Distrito_IDCanton_nn not null;
    
alter table Zona_Avistamiento
    modify ID_Distrito number(9) constraint Fk_Zona_IDDistrico_nn not null;
    
alter table Avistamiento
    modify (ID_Foto number(9) constraint Fk_Avista_IDFoto_nn not null,
            ID_ZonaAvistamiento number(9) constraint Fk_Avista_IDZona_nn not null);
    
alter table GeneraAvistamiento
    modify (ID_Persona number(9) constraint Fk_GeneraAvis_IDPersona_nn not null,
            ID_Reporte number(9) constraint Fk_GeneraAvis_IDReporte_nn not null);
    
alter table MuestraAvistamiento
    modify (ID_Reporte number(9) constraint Fk_MuestraAvis_IDReporte_nn not null,
            ID_Especie number(9) constraint Fk_MuestraAvis_IDEspecie_nn not null);
            
alter table
   persona
rename column
   segundoapelido  
TO
   segundoapellido;
    
    
