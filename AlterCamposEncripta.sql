alter table credencial
MODIFY (contrasena RAW(2000));

alter table bitacora
MODIFY (contrasenanueva RAW(2000),
        CONTRASENAANTERIOR RAW(2000));