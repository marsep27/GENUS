create table cumpleannos(
id_cumpleannos number(9),
Nombre VARCHAR2(500) constraint cumpleannos_nombre_nn not null,
Primer_apellido varchar(500)constraint cumpleannos_primerapellido_nn not null,
Segundo_apellido VARCHAR2(500)constraint cumpleannos_segundoapellido_nn not null,
Usuario varchar2(500)constraint cumpleannos_usuario_nn not null,
Fecha_Cumpleaņos date constraint cumpleannos_fecha_nn not null
);
/
ALTER TABLE cumpleannos
      ADD CONSTRAINT pk_cumpleannos PRIMARY KEY (ID_Cumpleannos)
      USING INDEX
      TABLESPACE Admin_Ind PCTFREE 20
      STORAGE (INITIAL 10k NEXT 10k PCTINCREASE 0);
      
create sequence seq_cumpleannos
    start with 1
    increment by 1
    minValue 1
    NOCACHE
    NOCYCLE;   