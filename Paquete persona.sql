---------------------------------Inicia Encabezado Paquete----------------------
Create or replace package paq_persona is
    
    Usuario_Conectado varchar2(500);
    Id_PersonaConectada number(9);
    
    Function agregarPersona(nombre_in varchar2,primerapellido_in Varchar2,
    segundoapellido_in Varchar2 ,fecha_nacimiento_in varchar2,profesion_in Varchar2,id_rol_in Number) 
                    return Number;
                    
    Function validaCredencial(pUsuario in varchar2) return Boolean; 
    
    Function validaCorreo(pDireccion in varchar2) return Boolean; 
    
    Procedure agregaCredencial(pUsuario in varchar2,pcontrasena in varchar2, pID_Persona in Number);
    
    Procedure agregaCorreo(pDireccion in varchar2, pID_persona in number);
    
    Procedure setUsuarioActual(pUsuario in varchar2);
    
    Function getUsuarioActual return varchar2;
    
    Function validaUsuario(pUsuario in varchar2) return Boolean;
    
    Function getContrasena(pUsuario in varchar2) return varchar2;
    
    Function updateContrasena(pContrasena in varchar2) return boolean;
    
    Procedure setIdPersonaActual(pUsuario in varchar2);
    
    Function getIDPersonaActual return number;
    
    Function updateNombre(pNombre in varchar2) return boolean;
    
    Function updatePrimerApellido(pPrimer_Apellido in varchar2) return boolean;
    
    Function updateSegundoApellido(pSegundo_Apellido in varchar2) return boolean;
    
    Function updateProfesion(pProfesion in varchar2) return boolean;
    
    Procedure updateFechaNacimiento(pFecha in varchar2);
    
    Function updateCorreo(pCorreo in varchar2) return char;
    
    Function getNombre return varchar2;
    
    Function getPrimerApellido return varchar2;
    
    Function getSegundoApellido return varchar2;
    
    Function getFechaNacimiento return varchar2;
    
    Function getProfesion  return varchar2;
    
End paq_persona;
/
-----------------------------------Fin Encabezado Paquete-----------------------
-----------------------------------Inicio Cuerpo Paquete------------------------
Create or replace package body paq_persona As 

    Function agregarPersona(nombre_in varchar2,primerapellido_in Varchar2,
                            segundoapellido_in Varchar2 ,fecha_nacimiento_in varchar2,
                            profesion_in Varchar2,id_rol_in Number) 
    return Number is
    cod_persona number;
    Begin
        Select seq_persona.nextVal
        into cod_persona
        from Dual;
        Insert into persona(ID_persona,nombre, primerApellido, segundoApellido,
                    fecha_nacimiento, profesion,ID_Rol)
        values(cod_persona,upper(nombre_in),upper(primerapellido_in),
    upper(segundoapellido_in),TO_DATE(fecha_nacimiento_in, 'DD/MM/YYYY'),upper(profesion_in),id_rol_in);
        commit;
         return cod_persona;
    End;
--------------------------------------------------------------------------------
    Function validaCredencial(pUsuario in varchar2) 
    return Boolean is
        contador integer;
    begin
        select count(*)
        into   contador
        from   credencial
        where  usuario = pUsuario
        AND ROWNUM = 1;
    
        return (contador > 0);
    end;
--------------------------------------------------------------------------------
    Function validaCorreo(pDireccion in varchar2) 
    return Boolean is
        contador integer;
    begin
        select count(*)
        into   contador
        from   correo
        where  direccion = upper(pDireccion)
        AND ROWNUM = 1;
    
        return (contador > 0);
    end;
--------------------------------------------------------------------------------
    Procedure agregaCredencial(pUsuario varchar2,pContrasena varchar2, pID_Persona number)
    is 
    
    begin
        PAQ_PERSONA.setUsuarioActual(pUsuario);
        Insert into credencial(Usuario,Contrasena,ID_Persona)
        values(pUsuario,DBCRYPT.encrypt(pContrasena),pID_Persona);
        commit;
        DBMS_OUTPUT.PUT_LINE('Entra');
    end;
--------------------------------------------------------------------------------
    Procedure agregaCorreo(pDireccion in varchar2, pID_Persona in number)
    is 
        cod_correo number;
    Begin
        Select seq_correo.nextVal
        into cod_correo
        from Dual;
        
        Insert into correo(Id_Correo, direccion, ID_persona)
        values(cod_correo, upper(pDireccion), pID_Persona);
        commit;
    End;
--------------------------------------------------------------------------------
    Procedure setUsuarioActual(pUsuario in varchar2) is
        Begin
            Usuario_Conectado := pUsuario;
            commit;
        end;
--------------------------------------------------------------------------------
    Function getUsuarioActual Return Varchar2 is
        Begin
            return Usuario_Conectado;
        end;
--------------------------------------------------------------------------------
    Function validaUsuario(pUsuario in varchar2) return Boolean is
        contador integer;
        begin
            select count(*)
            into   contador
            from   credencial
            where  usuario = pUsuario;
            
            return (contador = 1);
        end;
--------------------------------------------------------------------------------
    Function getContrasena(pUsuario in varchar2) return varchar2 is
        temp_contrasena varchar2(500);
        temp_user varchar2(500) := PAQ_PERSONA.GETUSUARIOACTUAL;
        begin
            select DBCRYPT.DECRYPT(CONTRASENA)
            into temp_contrasena
            from CREDENCIAL
            where usuario = temp_user;
            return temp_contrasena;
        end;
--------------------------------------------------------------------------------
    Function updateContrasena(pContrasena in varchar2) return boolean is
        temp_contra_antes varchar2(500);
        temp_contra_despues varchar2(500);
        temp_user varchar2(500) := PAQ_PERSONA.GETUSUARIOACTUAL;
        begin
            select DBCRYPT.DECRYPT(CONTRASENA)
            into temp_contra_antes
            from CREDENCIAL
            where usuario = temp_user;
            
            UPDATE CREDENCIAL
            SET CONTRASENA = DBCRYPT.ENCRYPT(pContrasena)
            WHERE USUARIO = temp_user;
            
            select DBCRYPT.DECRYPT(CONTRASENA)
            into temp_contra_despues
            from CREDENCIAL
            where usuario = temp_user;
            commit;
            return (temp_contra_antes <> temp_contra_despues);
        end;
--------------------------------------------------------------------------------
    Procedure setIdPersonaActual(pUsuario in varchar2) is 
        begin
            select ID_Persona 
            into Id_PersonaConectada
            from credencial
            where Usuario = pUsuario;
            commit;
        end;
--------------------------------------------------------------------------------   
    Function getIDPersonaActual return number is
        begin 
            return Id_PersonaConectada;
        end;
--------------------------------------------------------------------------------   
    Function updateNombre(pNombre in varchar2) return boolean is
        temp_nombre_despues varchar2(500);
        temp_nombre_antes varchar2(500);
        temp_id number := PAQ_PERSONA.getIDPersonaActual;
        begin
            select nombre
            into temp_nombre_antes
            from persona
            where id_persona = temp_id;
            
            UPDATE persona
            SET nombre = upper(pNombre)
            WHERE id_persona = temp_id;
            
            select nombre
            into temp_nombre_despues
            from persona
            where id_persona = temp_id;
            commit;
            return (temp_nombre_antes <> temp_nombre_despues);
        end;
--------------------------------------------------------------------------------
    Function updatePrimerApellido(pPrimer_Apellido in varchar2) return boolean is
        temp_apellido_antes varchar2(500);
        temp_apellido_despues varchar2(500);
        temp_id number := PAQ_PERSONA.getIDPersonaActual;
        begin
            select PRIMERAPELLIDO
            into temp_apellido_antes
            from persona
            where id_persona = temp_id;
            
            UPDATE persona
            SET PRIMERAPELLIDO = upper(pPrimer_Apellido)
            WHERE id_persona = temp_id;
            
            select PRIMERAPELLIDO
            into temp_apellido_despues
            from persona
            where id_persona = temp_id;
            commit;
            return (temp_apellido_antes <> temp_apellido_despues);
        end;
--------------------------------------------------------------------------------    
    Function updateSegundoApellido(pSegundo_Apellido in varchar2) return boolean is
        temp_apellido_antes varchar2(500);
        temp_apellido_despues varchar2(500);
        temp_id number := PAQ_PERSONA.getIDPersonaActual;
        begin
            select SEGUNDOAPELLIDO
            into temp_apellido_antes
            from persona
            where id_persona = temp_id;
            
            UPDATE persona
            SET SEGUNDOAPELLIDO = upper(pSegundo_Apellido)
            WHERE id_persona = temp_id;
            
            select SEGUNDOAPELLIDO
            into temp_apellido_despues
            from persona
            where id_persona = temp_id;
            commit;
            return (temp_apellido_antes <> temp_apellido_despues);
        end;
--------------------------------------------------------------------------------    
    Function updateProfesion(pProfesion in varchar2) return boolean is
        temp_profesion_antes varchar2(500);
        temp_profesion_despues varchar2(500);
        temp_id number := PAQ_PERSONA.getIDPersonaActual;
        begin
            select PROFESION
            into temp_profesion_antes
            from persona
            where id_persona = temp_id;
            
            UPDATE persona
            SET PROFESION = upper(pProfesion)
            WHERE id_persona = temp_id;
            
            select PROFESION
            into temp_profesion_despues
            from persona
            where id_persona = temp_id;
            commit;
            return (temp_profesion_antes <> temp_profesion_despues);
        end;
--------------------------------------------------------------------------------
    Procedure updateFechaNacimiento(pFecha in varchar2) is 
        temp_id number := PAQ_PERSONA.getIDPersonaActual;
        Begin
            UPDATE persona
            SET Fecha_Nacimiento = TO_DATE(pFecha, 'DD/MM/YYYY')
            WHERE id_persona = temp_id;
        end;
--------------------------------------------------------------------------------   
    Function updateCorreo(pCorreo in varchar2) return char is
    temp_Correo varchar2(500) := upper(pCorreo);
    temp_id number := PAQ_PERSONA.getIDPersonaActual;
    begin 
        if not PAQ_PERSONA.VALIDACORREO(temp_Correo) then
            UPDATE Correo
            SET direccion = upper(temp_Correo)
            WHERE id_persona = temp_id;
            commit;
            return '1';
        else
            rollback;
            return '0';
        end if;
    end;
--------------------------------------------------------------------------------
    Function getNombre return varchar2 is
    temp_id number := PAQ_Persona.getIDPersonaActual;
    temp_nombre varchar(500);
    Begin 
        Select nombre
        into temp_nombre
        from persona
        where ID_persona = temp_id;
        
        return temp_nombre;
    end;    
--------------------------------------------------------------------------------   
    Function getPrimerApellido return varchar2 is
    temp_id number := PAQ_Persona.getIDPersonaActual;
    temp_primer_apellido varchar(500);
    Begin 
        Select PrimerApellido
        into temp_primer_apellido
        from persona
        where ID_persona = temp_id;
        
        return temp_primer_apellido;
    end; 
--------------------------------------------------------------------------------
    Function getSegundoApellido return varchar2 is
    temp_id number := PAQ_Persona.getIDPersonaActual;
    temp_segundo_apellido varchar(500);
    Begin 
        Select SegundoApellido
        into temp_segundo_apellido
        from persona
        where ID_persona = temp_id;
        
        return temp_segundo_apellido;
    end; 
--------------------------------------------------------------------------------
    Function getFechaNacimiento return varchar2 is
    temp_id number := PAQ_Persona.getIDPersonaActual;
    temp_fecha_nacimiento varchar(500);
    temp_fecha_nacimiento_date date;
    Begin 
        Select Fecha_Nacimiento
        into temp_fecha_nacimiento_date
        from persona
        where ID_persona = temp_id;
        
        temp_fecha_nacimiento := TO_Char(temp_fecha_nacimiento_date,'DD/MM/YYYY');
        return temp_fecha_nacimiento;
    end; 
--------------------------------------------------------------------------------
    Function getProfesion return varchar2 is
    temp_id number := PAQ_Persona.getIDPersonaActual;
    temp_profesion varchar(500);
    Begin 
        Select Profesion
        into temp_profesion
        from persona
        where ID_persona = temp_id;
        
        return temp_profesion;
    end; 
--------------------------------------------------------------------------------
End paq_persona;
/
----------------------------------Fin Cuerpo Paquete----------------------------