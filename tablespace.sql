CREATE TABLESPACE Admin_Data
  DATAFILE 'C:\app\Mauricio\oradata\Genus\admindata01.dbf'
  SIZE 10M
  REUSE
  AUTOEXTEND ON
  NEXT 512k
  MAXSIZE 200M;
--
-- PE: INDEX
--
CREATE TABLESPACE Admin_Ind
  DATAFILE 'C:\app\Mauricio\oradata\Genus\adminind01.dbf'
  SIZE 10M
  REUSE
  AUTOEXTEND ON
  NEXT 512k
  MAXSIZE 200M;

--Creaci�n de otros tablespaces...
