alter table Persona
drop column ID_Foto;

alter table Avistamiento
drop column ID_Foto;

alter table foto
Add (ID_Avistamiento Number(9),
    ID_Persona number(9));
    
alter table foto
      ADD constraint fk_foto_persona foreign key (ID_Persona)
      REFERENCES persona(ID_persona);

alter table foto
      ADD constraint fk_foto_avistamiento foreign key (ID_Avistamiento)
      REFERENCES avistamiento(ID_Reporte);
      