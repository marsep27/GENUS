------------------------------------Types---------------------------------------
create or replace type t_nombre AS OBJECT
(
nombre varchar2(500)
); 
/
--------------------------------------------------------------------------------
create or replace type record_nom as table of t_nombre;
/
--------------------------------------------------------------------------------
create or replace type t_administrador as Object(
Nombre varchar2(500),primer_apellido VARCHAR2(500),segundo_apellido varchar(500),
correo varchar2(500), fecha_nacimiento varchar2(100)
);
/
--------------------------------------------------------------------------------
create or replace type t_usuario as OBJECT(
Nombre varchar2(500),primer_apellido VARCHAR2(500),segundo_apellido varchar(500),
correo varchar2(500), fecha_nacimiento varchar2(100), profesion varchar2(500),
rol_nombre varchar(500)
);
/
--------------------------------------------------------------------------------
create or replace type record_administrador as table of t_administrador;
/
--------------------------------------------------------------------------------
create or replace type record_usuario as table of t_usuario;
/
--------------------------------------------------------------------------------
create or replace type t_avistamiento as Object(
clase varchar2(500),orden varchar2(500), sub_orden varchar2(500),
familia varchar2(500), genero varchar2(500), especie varchar2(500),pais varchar(500),
provincia varchar2(500),canton varchar2(500),Distrito varchar2(500),
zonaAvistamiento varchar2(500)
);
/
--------------------------------------------------------------------------------
create or replace type record_avistamiento as table of t_avistamiento;
/
--------------------------------------------------------------------------------
create or replace type t_consulta as Object(
clase varchar2(500),orden varchar2(500), sub_orden varchar2(500),
familia varchar2(500), genero varchar2(500), especie varchar2(500)
);
/
--------------------------------------------------------------------------------
create or replace type record_consulta as table of t_consulta;
/
--------------------------------------------------------------------------------
create or replace type t_consultaColorTamano as Object(
clase varchar2(500),orden varchar2(500), sub_orden varchar2(500),
familia varchar2(500), genero varchar2(500), especie varchar2(500),
descripcion varchar2(500)
);
/
--------------------------------------------------------------------------------
create or replace type record_consultaColorTamano as table of t_consultaColorTamano;
/
--------------------------------------------------------------------------------
create or replace type t_topReportes as Object(
Nombre varchar2(500),primer_apellido VARCHAR2(500),segundo_apellido varchar(500),
usuario varchar2(500),cantidad number(10)
);
/
--------------------------------------------------------------------------------
create or replace type record_topReportes as table of t_topReportes;
/
--------------------------------------------------------------------------------
create or replace type t_extincion as Object(
clase varchar2(500),orden varchar2(500),suborden varchar2(500),
familia varchar2(500),genero varchar2(500),especie varchar2(500),
pais varchar2(500),provincia varchar2(500),canton varchar2(500),
distrito varchar2(500),zona varchar2(500),nombre varchar2(500),
primer_apellido varchar2(500),segundo_apellido varchar2(500),correo varchar2(500)
);
/
--------------------------------------------------------------------------------
create or replace type record_extincion as table of t_extincion;
/

--------------------------------------------------------------------------------
create or replace type t_cumpleannos as Object(
    nombre varchar2(500),
    primer_apellido varchar2(500),
    segundo_apellido varchar2(500),
    usuario varchar2(500),
    FECHA varchar2(500)
);
/
--------------------------------------------------------------------------------
create or replace type record_cumpleannos as table of t_cumpleannos;
/