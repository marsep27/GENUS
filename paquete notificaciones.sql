----------------------------------Inicio Encabezado-----------------------------
create or replace package paq_notificacion is
    procedure creaParametro(pValor in varchar2, pParametro in varchar2);
    function getParametro(pParametro in varchar2) return varchar2;
    procedure updateParametro(pParametro in varchar2, pValor in varchar);
    end;
/
---------------------------------Fin Encabezado---------------------------------
---------------------------------Inicio Cuerpo----------------------------------
create or replace package body paq_notificacion as
    procedure creaParametro(pValor in varchar2,pParametro in varchar2)
    is 
    temp_contador number;
    begin 
        select seq_mensaje.nextval
        into temp_contador
        from dual;
        
        insert into PARAMETRONOTIFICACION(ID_Mensaje,Parametro,valor)
        values(temp_contador,upper(pValor),upper(pParametro));
        commit;
    end;
--------------------------------------------------------------------------------
    function getParametro(pParametro in varchar2) return varchar2 is
    temp_mensaje varchar2(4000 byte);
    begin
        select valor 
        into temp_mensaje
        from parametroNotificacion
        where parametro = upper(pParametro);
        return temp_mensaje;
    end;
--------------------------------------------------------------------------------
    procedure updateParametro(pParametro in varchar2, pValor in varchar) is
    begin
        Update parametroNotificacion
        set valor = upper(pValor)
        where parametro = upper(pParametro);
        commit;
    end;
end;
/
---------------------------------Fin Cuerpo-------------------------------------
