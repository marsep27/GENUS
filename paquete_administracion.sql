-----------------------Inicio Encabezado----------------------------------------
Create or replace package paq_Administracion is
    
    rol_administrador varchar(100) := 'ADMINISTRADOR';
    
    procedure crearRol(pDescripcion In Varchar2);
    
    Function getRolID(pDescripcion In Varchar2) return Number;
    
    Function updateDescripcionRol(pDescripcion in varchar2, pID in number)
    return number;
    
    Function updateRolPersona(pDescripcion in varchar2, pUsuario in varchar2)
    return number;
    
    Function getRoles return record_nom;
    
    Function getAdministradores return record_administrador;
    
    Function getUsuarios return record_usuario;
    
    procedure insertaFotoPerfil(pImagen in varchar2,pID_Persona IN number);
    
    procedure insertaFotoAvistamiento(pImagen in varchar2,pID_Avistamiento IN number);
    
    procedure insertaTamanno(pDescripcion in varchar2, pID_especie in Number);
    
    procedure insertaColor(pDescripcion in varchar2, pID_especie in Number);
    
    Function updateColor(pDescripcion in varchar2, pID in number) return number;

    Function updateTamanno(pDescripcion in varchar2, pID in number) return number;
    
    function getIDColor(pDescripcion in varchar2,pID_Especie in number) return number;

    function getIDTamanno(pDescripcion in varchar2,pID_Especie in number) return number;

end paq_Administracion;
/
-------------------------Fin Encabezado-----------------------------------------
-------------------------Inicio Cuerpo------------------------------------------
Create or Replace Package Body paq_Administracion as
    procedure crearRol(pDescripcion In varchar2) is
    cod_rol number;
    begin 
        select seq_rol.nextVal
        into cod_rol
        from Dual;
        
        Insert into rol(id_rol,descripcion)
        values(cod_rol,Upper(pDescripcion));
        commit;
    exception 
        when OTHERS then 
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------
    Function getRolID(pDescripcion in varchar2) return number
    is 
    cod_rol number;
    begin 
        Select id_rol 
        into cod_rol
        from rol
        where descripcion = upper(pDescripcion);
        return cod_rol;
    end;
--------------------------------------------------------------------------------   
    procedure insertaFotoPerfil(pImagen in varchar2,pID_Persona IN number) 
    is
    cod_imagen number;
    begin 
        Select seq_foto.nextval 
        into cod_imagen 
        from dual;
    
        Insert into foto(ID_foto,Imagen,ID_Persona)
        values (cod_imagen, pImagen,pID_Persona);
        commit;
    exception 
        when OTHERS then 
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------
    procedure insertaFotoAvistamiento(pImagen in varchar2,
    pID_Avistamiento IN number) is
    cod_imagen number;
    begin 
        Select seq_foto.nextval 
        into cod_imagen 
        from dual;
    
        Insert into foto(ID_foto,Imagen,ID_Avistamiento)
        values (cod_imagen, pImagen,pID_Avistamiento);
        commit;
    exception 
        when OTHERS then 
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------
    procedure insertaTamanno(pDescripcion in varchar2, pID_especie in Number)
    is
    cod_Tamanno number;
    begin
        Select seq_tamanno.nextval 
        into cod_Tamanno 
        from dual;
        
        Insert into tamanno(ID_Tamanno, descripcion, ID_especie)
        values (cod_tamanno,upper(pDescripcion),pID_especie);
        commit;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------
    procedure insertaColor(pDescripcion in varchar2, pID_especie in Number)
    is
    cod_color number;
    begin
        Select seq_color.nextval 
        into cod_color
        from dual;
        
        Insert into color(ID_Color, descripcion, ID_especie)
        values (cod_color,upper(pDescripcion),pID_especie);
        commit;
    Exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------
    Function updateDescripcionRol(pDescripcion in varchar2,pID in number) 
    return number is
    Begin
        UPDATE Rol
        SET descripcion = upper(pDescripcion)
        WHERE id_Rol = pID;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function updateRolPersona(pDescripcion in varchar2, pUsuario in varchar2) 
    return number is
    Usuario_actual varchar(500) := paq_persona.getUsuarioActual;
    Begin 
        If (pUsuario <> Usuario_actual) then
            Update Persona
            Set ID_rol = (select id_rol from 
                            Rol where Descripcion = upper(pDescripcion))
            where id_persona = (select id_persona from credencial
                                where Usuario = pUsuario);
            commit;
            return 1;
        else
            return 0;
        end IF;
    end;
--------------------------------------------------------------------------------
    function getRoles return record_nom
    is
       lista_nombres  record_nom := record_nom();
    begin
         for dato in (select DESCRIPCION from Rol) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_nombre(dato.descripcion)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    function getAdministradores return record_administrador
    is
       lista_nombres  record_administrador := record_administrador();
       temp_id number := paq_administracion.getRolID(
                        paq_administracion.rol_administrador);
    begin
         for dato in (select p.NOMBRE,p.PRIMERAPELLIDO,p.SEGUNDOAPELLIDO, 
         c.DIRECCION, TO_CHAR(p.FECHA_NACIMIENTO,'DD/MM/YYYY') fecha
         from persona p ,correo c
         where p.ID_ROL = temp_id and p.ID_PERSONA = c.ID_PERSONA) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_administrador(dato.nombre,
                            dato.primerapellido, dato.segundoapellido, dato.direccion,
                            dato.fecha)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    function getUsuarios return record_usuario
    is
       lista_nombres  record_usuario := record_usuario();
       temp_id number := paq_administracion.getRolID(
                        paq_administracion.rol_administrador);
    begin
         for dato in (select p.NOMBRE,p.PRIMERAPELLIDO,p.SEGUNDOAPELLIDO, 
         c.DIRECCION, TO_CHAR(p.FECHA_NACIMIENTO,'DD/MM/YYYY') fecha, 
         p.PROFESION,d.DESCRIPCION
         from persona p ,correo c, rol d
         where p.ID_ROL <> temp_id and p.ID_PERSONA = c.ID_PERSONA
         and p.ID_ROL = d.ID_ROL) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_usuario(dato.nombre,
                            dato.primerapellido, dato.segundoapellido, dato.direccion,
                            dato.fecha,dato.profesion,dato.descripcion)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function updateColor(pDescripcion in varchar2, pID in number)
    return number is
    begin
        update TAMANNO
        set DESCRIPCION = upper(pDescripcion)
        where ID_TAMANNO = pID;
        commit;
        return 1;
    exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;

    Function updateTamanno(pDescripcion in varchar2, pID in number) 
    return number is
    begin
        update TAMANNO
        set DESCRIPCION = upper(pDescripcion)
        where ID_TAMANNO = pID;
        commit;
        return 1;
    exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------  
    function getIDColor(pDescripcion in varchar2,pID_Especie in number)
    return number is 
        temp_id number;
        begin
            select id_color
            into temp_id
            from color
            where ID_especie = pID_Especie and DESCRIPCION = upper(pDescripcion);
            return temp_id;
        end;
--------------------------------------------------------------------------------
    function getIDTamanno(pDescripcion in varchar2,pID_Especie in number) 
    return number is
        temp_id number;
        begin
            select ID_TAMANNO
            into temp_id
            from TAMANNO
            where ID_especie = pID_Especie and DESCRIPCION = upper(pDescripcion);
            return temp_id;
        end;
end paq_Administracion;
/
--------------------------------Fin Cuerpo--------------------------------------