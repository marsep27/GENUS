-----------------------------Inicio Encabezado----------------------------------
create or replace package paq_locacion is
    function insertarPais(pNombre in varchar2) return number;
    function insertarProvincia(pNombre in Varchar2, pID_Pais in number) return number;
    function insertarCanton(pNombre in Varchar2,pID_Provincia in number) return number;
    FUNCTION insertarDistrito(pNombre in Varchar2, pID_Canton in number) RETURN number;
    function insertarZonaAvistamiento(pNombreLugar in Varchar2, pID_Distrito in number)
                    return number;
                    
    Function getIDPais(pNombre in varchar2) return number;
    Function getIDProvincia(pNombre in varchar2) return number;
    Function getIDCanton(pNombre in varchar2) return number;
    Function getIDDistrito(pNombre in varchar2) return number;
    Function getIDZonaAvistamiento(pNombre in varchar2) return number;
    
    Function updatePais(pNombre in varchar2, pID in number) return number;
    Function updateProvincia(pNombre in varchar2, pID in number) return number;
    Function updateCanton(pNombre in varchar2, pID in number) return number;
    Function updateDistrito(pNombre in varchar2, pID in number) return number;
    Function updateZonaAvistamiento(pNombre in varchar2, pID in number) return number;
end paq_locacion;
/
------------------------------Fin Encabezado------------------------------------
-------------------------------Inicio Cuerpo------------------------------------
create or replace package body paq_locacion as
    function insertarPais(pNombre in varchar2) 
    return number is
    cod_pais number;
    begin
        Select seq_pais.nextval
        into cod_pais 
        from dual;
        
        Insert into pais(ID_pais,nombre)
        values(cod_pais,upper(pNombre));
        commit; 
        return cod_pais;
    exception 
        when OTHERS then 
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------    
    function insertarProvincia(pNombre in Varchar2, pID_Pais in Number)
    return number is
    cod_provincia number;
    begin 
        Select seq_provincia.nextval
        into cod_provincia
        from dual;
        
        insert into provincia(ID_provincia,nombre,ID_pais)
        values(cod_provincia,upper(pNombre),pID_Pais);
        commit;
        return cod_provincia;
    exception
         when OTHERS then 
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------   
    function insertarCanton(pNombre in varchar2, pID_Provincia in number)
    return number is
    cod_canton number;
    begin 
        Select seq_canton.nextval
        into cod_canton
        from dual;
        
        insert into canton(ID_canton, nombre, ID_Provincia)
        values(cod_canton,upper(pNombre),pID_Provincia);
        commit;
        return cod_canton;
    exception
        when OTHERS then 
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------    
    function insertarDistrito(pNombre  in varchar2, pID_Canton in number)
    return number is 
    cod_distrito number;
    begin 
        select seq_distrito.nextval
        into cod_distrito
        from dual;
        
        insert into distrito(ID_Distrito,nombre,ID_Canton)
        values(cod_distrito,upper(pNombre),pID_Canton);
        commit;
        return cod_distrito;
        
    exception 
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------   
    function insertarZonaAvistamiento(pNombreLugar in Varchar2, pID_Distrito in number)
    return number is 
    cod_ZonaAvistamiento number;
    begin 
        Select seq_zonaavistamiento.nextval
        into cod_ZonaAvistamiento
        from dual;
        
        insert into zona_avistamiento(ID_zonaavistamiento,nombrelugar,id_distrito)
        values(cod_ZonaAvistamiento,upper(pNombreLugar),pID_Distrito);
        commit;
        return cod_ZonaAvistamiento;
        
    exception
        when OTHERS then
            DBMS_OUTPUT.PUT_LINE('Dato Duplicado');
            rollback;
    end;
--------------------------------------------------------------------------------
    Function getIDPais(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Pais
        into temp_id
        from Pais
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDProvincia(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Provincia
        into temp_id
        from Provincia
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDCanton(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Canton
        into temp_id
        from Canton
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDDistrito(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_Distrito
        into temp_id
        from Distrito
        where Nombre = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------
    Function getIDZonaAvistamiento(pNombre in varchar2) return number is
    temp_id number;
    begin 
        select ID_ZonaAvistamiento
        into temp_id
        from Zona_Avistamiento
        where NombreLugar = upper(pNombre);
        
        return temp_id;
    end;
--------------------------------------------------------------------------------   
    Function updatePais(pNombre in varchar2, pID in number) return number is
    Begin
        UPDATE Pais
        SET nombre = upper(pNombre)
        WHERE id_Pais = pID;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function updateProvincia(pNombre in varchar2, pID in number) 
    return number is
    Begin
        UPDATE Provincia
        SET nombre = upper(pNombre)
        WHERE id_Provincia = pID;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function updateCanton(pNombre in varchar2, pID in number)
    return number is
    Begin
        UPDATE Canton
        SET nombre = upper(pNombre)
        WHERE id_Canton = pID;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function updateDistrito(pNombre in varchar2, pID in number)
    return number is
    Begin
        UPDATE Distrito
        SET nombre = upper(pNombre)
        WHERE id_Distrito = pID;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
    Function updateZonaAvistamiento(pNombre in varchar2, pID in number) 
    return number is
    Begin
        UPDATE ZONA_AVISTAMIENTO
        SET nombrelugar = upper(pNombre)
        WHERE id_ZonaAvistamiento = pID;
        commit;
        return 1;
    exception 
        When OTHERS then
            rollback;
            return 0;
    end;
--------------------------------------------------------------------------------
end paq_locacion;
/
------------------------------Fin Cuerpo----------------------------------------