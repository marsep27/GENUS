-------------------------------Inicio Encabezado--------------------------------
create or replace package paq_consulta is
    Function consulta_X_Clase(pClase in varchar2) return record_consulta;
    Function consulta_X_Orden(pOrden in varchar2) return record_consulta;
    Function consulta_X_SubOrden(pSubOrden in varchar2) return record_consulta;
    Function consulta_X_Familia(pFamilia in varchar2)return record_consulta;
    Function consulta_X_Genero(pGenero in varchar2)return record_consulta;
    Function consulta_X_Especie(pEspecie in varchar2)return record_consulta;
    Function consulta_X_Tamanno(pTamano in varchar2) return record_consultaColorTamano;
    Function consulta_X_Color(pColor in varchar2) return record_consultaColorTamano;
    
    Function consulta_X_ZonaVida(pPais in varchar2,pProvincia in varchar2,
    pCanton in varchar2, pDistrito in varchar2, pZona in varchar2) return 
    record_avistamiento;
    
    Function consulta_TopPersonas return record_topReportes;
    --Function consulta_X_Provincia(pPais in varchar2,pProvincia in varchar2,
    --pCanton in varchar2, pDistrito in varchar2, pZona in varchar2) return 
    --record_avistamiento;
    --Function consulta_X_Canton(pPais in varchar2,pProvincia in varchar2,
   -- pCanton in varchar2, pDistrito in varchar2, pZona in varchar2) return 
    --record_avistamiento;
    --Function consulta_X_Distrito(pPais in varchar2,pProvincia in varchar2,
    --pCanton in varchar2, pDistrito in varchar2, pZona in varchar2) return 
    --record_avistamiento;
    --Function consulta_top_persona 
end;
/
-------------------------------Fin Encabezado-----------------------------------
-------------------------------Inicio Cuerpo------------------------------------
create or replace package body paq_consulta as
    Function consulta_X_Clase(pClase in varchar2) return record_consulta
    is
       lista_nombres  record_consulta := record_consulta();
    begin
            for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie
            from clase clas
            inner join orden ord
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            where clas.NOMBRE = pClase
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consulta(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_Orden(pOrden in varchar2) return record_consulta
    is
       lista_nombres  record_consulta := record_consulta();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie
            from orden ord
            inner join clase clas
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            where ord.NOMBRE = upper(pOrden)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consulta(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_SubOrden(pSubOrden in varchar2) return record_consulta
    is
       lista_nombres  record_consulta := record_consulta();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie
            from orden ord
            inner join clase clas
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            where su.NOMBRE = upper(pSubOrden)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consulta(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_Familia(pFamilia in varchar2)return record_consulta
    is
       lista_nombres  record_consulta := record_consulta();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie
            from orden ord
            inner join clase clas
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            where fa.NOMBRE = upper(pFamilia)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consulta(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_Genero(pGenero in varchar2)return record_consulta
    is
       lista_nombres  record_consulta := record_consulta();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie
            from orden ord
            inner join clase clas
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            where gen.NOMBRE = upper(pGenero)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consulta(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie)) ;
       end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_Especie(pEspecie in varchar2)return record_consulta
    is
       lista_nombres  record_consulta := record_consulta();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie
            from clase clas
            inner join orden ord
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            where esp.NOMBRE = upper(pEspecie)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consulta(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie)) ;
        end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_Tamanno(pTamano in varchar2) 
    return record_consultaColorTamano is
       lista_nombres  record_consultaColorTamano := record_consultaColorTamano();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie, tam.DESCRIPCION tamanno
            from clase clas
            inner join orden ord
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            Inner join Tamanno tam
            on esp.ID_ESPECIE = tam.ID_ESPECIE
            where tam.DESCRIPCION = upper(pTamano)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consultaColorTamano(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie,
                dato.tamanno)) ;
        end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_Color(pColor in varchar2) 
    return record_consultaColorTamano is
       lista_nombres  record_consultaColorTamano := record_consultaColorTamano();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie, color.DESCRIPCION Color
            from clase clas
            inner join orden ord
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            Inner join Color color
            on esp.ID_ESPECIE = color.ID_ESPECIE
            where color.DESCRIPCION = upper(pColor)
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_consultaColorTamano(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie,
                dato.color)) ;
        end loop;
        return lista_nombres;
   end;
--------------------------------------------------------------------------------
    Function consulta_X_ZonaVida(pPais in varchar2,pProvincia in varchar2,
    pCanton in varchar2, pDistrito in varchar2, pZona in varchar2) return record_avistamiento 
    is
    lista_nombres  record_avistamiento := record_avistamiento();
    begin
         for dato in (
            select clas.nombre clase, ord.nombre orden, su.nombre suborden, fa.nombre familia, 
            gen.nombre genero, esp.nombre especie, pais.nombre pais, 
            prov.nombre provincia,cant.NOMBRE canton,dis.nombre distrito,
            zona.nombrelugar Zona
            from clase clas
            inner join orden ord
            on  clas.ID_CLASE = ord.ID_Clase
            inner join SUB_ORDEN su
            on ord.ID_ORDEN = su.ID_ORDEN
            Inner join familia fa
            on su.ID_SUBORDEN = fa.ID_SUBORDEN
            Inner join genero gen
            on fa.ID_FAMILIA = gen.ID_FAMILIA
            Inner join ESPECIE esp
            on gen.ID_GENERO = esp.ID_GENERO
            Inner join Color color
            on esp.ID_ESPECIE = color.ID_ESPECIE
            Inner join MUESTRAAVISTAMIENTO mues
            on esp.ID_Especie = mues.ID_especie
            Inner join AVISTAMIENTO avis
            on mues.ID_Reporte = avis.ID_REPORTE
            Inner join ZONA_AVISTAMIENTO zona
            on avis.ID_ZONAAVISTAMIENTO = zona.ID_ZONAAVISTAMIENTO
            Inner join DISTRITO dis
            on zona.ID_DISTRITO = dis.ID_DISTRITO
            inner join Canton cant
            on dis.ID_canton = cant.ID_CANTON
            inner join PROVINCIA prov
            on cant.ID_provincia = prov.ID_provincia
            inner join Pais pais
            on pais.ID_Pais = prov.ID_Pais
            where (pais.Nombre = nvl(upper(pPais),pais.nombre)) or 
            (prov.Nombre = nvl(upper(pProvincia),prov.NOMBRE)) or 
            (cant.NOMBRE = nvl(upper(pCanton),cant.Nombre)) or
            (dis.NOMBRE = nvl(upper(pDistrito),dis.NOMBRE))
            order by esp.NOMBRE asc
         ) loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_avistamiento(dato.clase,
                dato.orden,dato.suborden,dato.familia,dato.genero, dato.especie,
                dato.pais,dato.provincia,dato.canton,dato.distrito,dato.zona)) ;
        end loop;
        return lista_nombres;
    end;
--------------------------------------------------------------------------------
   Function consulta_TopPersonas return record_topReportes is
   lista_nombres  record_topReportes := record_topReportes();
   begin 
        for dato in (Select pers.nombre,pers.PRIMERAPELLIDO,pers.SEGUNDOAPELLIDO,
            cred.USUARIO, seleccion.cant
            from (Select count(ID_persona) cant,ID_PERSONA
                from GENERAAVISTAMIENTO 
                group by ID_PERSONA
                order by cant desc) seleccion
            inner join persona pers
            on seleccion.ID_PERSONA = pers.ID_PERSONA
            inner join credencial cred
            on pers.ID_PERSONA = cred.ID_PERSONA
            where pers.ID_PERSONA = seleccion.ID_PERSONA AND ROWNUM <= 5
        )loop
                lista_nombres.EXTEND;
                lista_nombres(lista_nombres.count) := (t_topReportes(dato.nombre,
                dato.PRIMERAPELLIDO,dato.SEGUNDOAPELLIDO, dato.USUARIO, dato.cant)) ;
        end loop;
   end;
end;
/
-------------------------------Fin Cuerpo---------------------------------------
