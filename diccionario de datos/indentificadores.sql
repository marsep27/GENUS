
NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE            9          3          9                8
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_ADMINISTRADOR                TYPE          REFERENCE           11          3         40               10
FECHA_NACIMIENTO               002AE27D6106F0E52A0BAFE856C4BDCB VARIABLE           T_ADMINISTRADOR                TYPE          DECLARATION         10          3         23                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_ADMINISTRADOR                TYPE          REFERENCE            9          3          8                8
CORREO                         E2A7F6B99E317ECF4C8103912276C451 VARIABLE           T_ADMINISTRADOR                TYPE          DECLARATION          8          3          1                1
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            T_ADMINISTRADOR                TYPE          REFERENCE            7          2         69                6
SEGUNDO_APELLIDO               59385DED387A3B60503C30EB4A3D0139 VARIABLE           T_ADMINISTRADOR                TYPE          DECLARATION          6          2         52                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_ADMINISTRADOR                TYPE          REFERENCE            5          2         38                4
PRIMER_APELLIDO                010A4FB024D50C7A715D15993189C591 VARIABLE           T_ADMINISTRADOR                TYPE          DECLARATION          4          2         22                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_ADMINISTRADOR                TYPE          REFERENCE            3          2          8                2
NOMBRE                         5CEAFD7561BBEBB3D42554BE62C87FCA VARIABLE           T_ADMINISTRADOR                TYPE          DECLARATION          2          2          1                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
T_ADMINISTRADOR                548B407727FD344B693DEF7D90DAC0D8 OBJECT             T_ADMINISTRADOR                TYPE          DECLARATION          1          1          6                0
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            T_USUARIO                      TYPE          REFERENCE           15          4         12               14
ROL_NOMBRE                     7314317B5438205723F6AC75A72FFF69 VARIABLE           T_USUARIO                      TYPE          DECLARATION         14          4          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_USUARIO                      TYPE          REFERENCE           13          3         65               12
PROFESION                      D8E4E7FF4C96977E3E9507D8996393A8 VARIABLE           T_USUARIO                      TYPE          DECLARATION         12          3         55                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_USUARIO                      TYPE          REFERENCE           11          3         40               10
FECHA_NACIMIENTO               68422064FCDFD000B3D7185ECCE485DA VARIABLE           T_USUARIO                      TYPE          DECLARATION         10          3         23                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_USUARIO                      TYPE          REFERENCE            9          3          8                8
CORREO                         B4B6492BCCF5FEC2C6DD72C3E57FDD6C VARIABLE           T_USUARIO                      TYPE          DECLARATION          8          3          1                1
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            T_USUARIO                      TYPE          REFERENCE            7          2         69                6
SEGUNDO_APELLIDO               C1889137E7B73D35B489BB1FAC026BB0 VARIABLE           T_USUARIO                      TYPE          DECLARATION          6          2         52                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_USUARIO                      TYPE          REFERENCE            5          2         38                4
PRIMER_APELLIDO                5448C4F6A63E1C78DC70800321AD1FBF VARIABLE           T_USUARIO                      TYPE          DECLARATION          4          2         22                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_USUARIO                      TYPE          REFERENCE            3          2          8                2
NOMBRE                         DF4B2D5A6B6AE3095B001DE4909C0402 VARIABLE           T_USUARIO                      TYPE          DECLARATION          2          2          1                1
T_USUARIO                      E5A51E5C4AB81D4B4F04D12B3C7D528C OBJECT             T_USUARIO                      TYPE          DECLARATION          1          1          6                0
T_ADMINISTRADOR                548B407727FD344B693DEF7D90DAC0D8 OBJECT             RECORD_ADMINISTRADOR           TYPE          REFERENCE            2          1         39                1
RECORD_ADMINISTRADOR           D181281C2D3CEE42A86E586E96E71AF7 NESTED TABLE       RECORD_ADMINISTRADOR           TYPE          DECLARATION          1          1          6                0
T_USUARIO                      E5A51E5C4AB81D4B4F04D12B3C7D528C OBJECT             RECORD_USUARIO                 TYPE          REFERENCE            2          1         33                1
RECORD_USUARIO                 012A8663BC9DE10FE22969A3A0CD4843 NESTED TABLE       RECORD_USUARIO                 TYPE          DECLARATION          1          1          6                0
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           76         35         14               39
LARGO                          2872AAA1024ADDA47D764F1A6A349DAB VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           75         34         91               73

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           74         34         72               73
SUBSTR                         271A8E464A35E9722FC949E272A39BF5 FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                73         34         65               72
ASCII                          B9E95DB6C4711345972E80C572061471 FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                72         34         59               69
LARGO                          2872AAA1024ADDA47D764F1A6A349DAB VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           71         34         53               69
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           70         34         34               69
RPAD                           332A6A152EAEC9F58F0E37B17D1C7B12 FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                69         34         29               68
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          68         34          7               39
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           67         33         23               66
LENGTH                         6AA7292AD9C470D3719AD4B4F8CBE0AD FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                66         33         16               65
LARGO                          2872AAA1024ADDA47D764F1A6A349DAB VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          65         33          7               39
RAW_CODIFICATION               ECDBB210267C4AAC869F39036BCE82C9 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           64         32         54               63

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          62         32          7               39
RAW_CODIFICATION               ECDBB210267C4AAC869F39036BCE82C9 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           61         31         35               58
RAW_KEY                        169664F6722EBFA4425A7F217F755BA4 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           60         30         35               58
P_DATA                         2924B6C05276C48C6DC147C454508931 FORMAL IN          DBCRYPT                        PACKAGE BODY  REFERENCE           59         29         35               58
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE DBCRYPT                        PACKAGE BODY  REFERENCE           57         26         28               56
PASSWORD_DECRYPTED             F9B29B55708FF90C2D70DB7978C34EB5 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         56         26          9               39
P_DATA                         2924B6C05276C48C6DC147C454508931 FORMAL IN          DBCRYPT                        PACKAGE BODY  REFERENCE           55         25         84               54
RAW_CODIFICATION               ECDBB210267C4AAC869F39036BCE82C9 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          52         25          9               50
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           51         25         26               50
RAW_CODIFICATION               ECDBB210267C4AAC869F39036BCE82C9 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         50         25          9               39
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    DBCRYPT                        PACKAGE BODY  REFERENCE           49         24         15               48

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LARGO                          2872AAA1024ADDA47D764F1A6A349DAB VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         48         24          9               39
ENCRYPT_KEY                    9E5281FAB4A51C7AC8412A4F3A222037 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           47         23         47               46
RAW_KEY                        169664F6722EBFA4425A7F217F755BA4 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          45         23          9               43
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           44         23         17               43
RAW_KEY                        169664F6722EBFA4425A7F217F755BA4 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         43         23          9               39
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE DBCRYPT                        PACKAGE BODY  REFERENCE           42         21         46               39
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           41         21         33               40
P_DATA                         2924B6C05276C48C6DC147C454508931 FORMAL IN          DBCRYPT                        PACKAGE BODY  DECLARATION         40         21         23               39
DECRYPT                        919F064CD87E01FD4470CC244F8B7A01 FUNCTION           DBCRYPT                        PACKAGE BODY  DEFINITION          39         21         14                1
PASSWORD_ENCRYPTED             0FA615503DAA2B15AD4E854909438537 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           38         18         16                5
PASSWORD_ENCRYPTED             0FA615503DAA2B15AD4E854909438537 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           37         17         34               34

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RAW_KEY                        0A65A6DFC22E27A2513CB94C5E35D967 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           36         16         34               34
PASSWORD_ENCRYPTED             0FA615503DAA2B15AD4E854909438537 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           35         15         34               34
ENTERO                         49DBD1A2D16D126AE0223D1D88C5AC21 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           33         13         89               32
CHR                            A9B6E73A8F1124A865983C3C69FB8306 FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                32         13         85               28
ENTERO                         49DBD1A2D16D126AE0223D1D88C5AC21 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           31         13         77               28
ENTERO                         49DBD1A2D16D126AE0223D1D88C5AC21 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           30         13         68               29
CHR                            A9B6E73A8F1124A865983C3C69FB8306 FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                29         13         64               28
RPAD                           8EC6602DB5923974CD2B9D7EF156BF2D FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                28         13         59               26
P_DATA                         BC1A796234C2C5C75C6AA6F4D0670FFC FORMAL IN          DBCRYPT                        PACKAGE BODY  REFERENCE           27         13         51               26
PASSWORD_ENCRYPTED             0FA615503DAA2B15AD4E854909438537 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          25         13          9                5
LARGO                          7BF6FC60430CCA55A14B695292BCCB68 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           24         12         25               23

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
ENTERO                         49DBD1A2D16D126AE0223D1D88C5AC21 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          23         12          9                5
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             RECORD_NOM                     TYPE          REFERENCE            2          1         29                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       RECORD_NOM                     TYPE          DECLARATION          1          1          6                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_NOMBRE                       TYPE          REFERENCE            3          3          8                2
NOMBRE                         73D98C6CF9DB7EE4A9E6FB8DEA498584 VARIABLE           T_NOMBRE                       TYPE          DECLARATION          2          3          1                1
FAMILIA                        1BE821EABFF8C84A0E480FCF83E9EBA9 VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION          8          3          1                1
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             T_NOMBRE                       TYPE          DECLARATION          1          1          6                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE            7          2         52                6
SUB_ORDEN                      4D80E1EEE40303C163EF43CAA3E33BFB VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION          6          2         42                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE            5          2         27                4
ORDEN                          19532C5FE75290EA0501C2B22C6C74C4 VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION          4          2         21                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE            3          2          7                2
CLASE                          C9EED202A7834F867BA008B654C77A03 VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION          2          2          1                1
T_CONSULTACOLORTAMANO          415968482F8C50C3C7F657CE2C0AEDDA OBJECT             T_CONSULTACOLORTAMANO          TYPE          DECLARATION          1          1          6                0
PNOMBRE                        E01FF58E9CF3579FDA2E43F07EDC984A FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         51         15         25               50
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           79         19         80               74
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           78         19         65               77
PID                            517E82B769461A2A12F721A368B82D5B FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         77         19         58               74
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           76         19         48               75
PNOMBRE                        59B208CEF0F0C63FBA9C355D1A9DBCBD FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         75         19         37               74
UPDATEZONAAVISTAMIENTO         7F5E2614FC06D46DD6A87DCB2814AE25 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         74         19         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           73         18         72               68

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           72         18         57               71
PID                            7571F037C19D55655353F1B3199514BD FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         71         18         50               68
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           70         18         40               69
PNOMBRE                        00DDC383E8F77A210F12B00E893C635C FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         69         18         29               68
UPDATEDISTRITO                 729980EE0E87D7211795C10868419F7B FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         68         18         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           67         17         70               62
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           66         17         55               65
PID                            CC4D1F0E26F89CAA7E05AFE073572BCF FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         65         17         48               62
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           64         17         38               63
PNOMBRE                        2A6F2ACBFB6AF208EDBAC2D58310230A FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         63         17         27               62
UPDATECANTON                   3A9998A608142D92DB61D253350716A1 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         62         17         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           61         16         73               56
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           60         16         58               59
PID                            4FDA6AB59A61C78EFAD006B4716BD603 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         59         16         51               56
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           58         16         41               57
PNOMBRE                        9BD3D805D6BE948EC1F454FC229ED825 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         57         16         30               56
UPDATEPROVINCIA                E6341B93BDB06AB7C5F852E18A9AC9A5 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         56         16         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           55         15         68               50
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           54         15         53               53
PID                            4440F25D9AB2AF21D627E8A992AB72AE FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         53         15         46               50
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           52         15         36               51
PNOMBRE                        4A5B6B7C5183ED2CF5EFED64204A493E FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          153        208         33              146

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_ID                        14E05B43A592C239485AE0D257D27036 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           95        124         16               87
TEMP_ID                        14E05B43A592C239485AE0D257D27036 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          94        120         14               87
PNOMBRE                        751EEE103F3066306FFB249F4376CE4D FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           93        122         30               87
PID                            C1386C58B7662A15D240E4C90507E886 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          152        209         37              146
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          151        205         12              146
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          150        204         65              149
PID                            C1386C58B7662A15D240E4C90507E886 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        149        204         58              146
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE          148        204         48              147
PNOMBRE                        4A5B6B7C5183ED2CF5EFED64204A493E FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        147        204         37              146
UPDATEZONAAVISTAMIENTO         7F5E2614FC06D46DD6A87DCB2814AE25 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION         146        204         14                1
PNOMBRE                        3DEE6019D5059617E583ECA8FC00B599 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          145        194         28              138

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID                            8C74870AE5D7154619F454721F2E6F20 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          144        195         29              138
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          143        191         12              138
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          142        190         57              141
PID                            8C74870AE5D7154619F454721F2E6F20 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        141        190         50              138
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE          140        190         40              139
PNOMBRE                        3DEE6019D5059617E583ECA8FC00B599 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        139        190         29              138
UPDATEDISTRITO                 729980EE0E87D7211795C10868419F7B FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION         138        190         14                1
PNOMBRE                        D2A4C63D6770195C33CA173C59E5529E FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          137        180         28              130
PID                            E171E2C3F73C79BEA908F6A8D7867889 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          136        181         27              130
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          135        177         12              130
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          134        176         55              133

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID                            E171E2C3F73C79BEA908F6A8D7867889 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        133        176         48              130
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE          132        176         38              131
PNOMBRE                        D2A4C63D6770195C33CA173C59E5529E FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        131        176         27              130
UPDATECANTON                   3A9998A608142D92DB61D253350716A1 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION         130        176         14                1
PNOMBRE                        1BE483659599842652FB7CE3234584D4 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          129        166         28              122
PID                            C102169B1A17B0E528370025ABA1DF53 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          128        167         30              122
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          127        163         12              122
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          126        162         58              125
PID                            C102169B1A17B0E528370025ABA1DF53 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        125        162         51              122
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE          124        162         41              123
PNOMBRE                        1BE483659599842652FB7CE3234584D4 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        123        162         30              122

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
UPDATEPROVINCIA                E6341B93BDB06AB7C5F852E18A9AC9A5 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION         122        162         14                1
PNOMBRE                        E414344D4CEC26CC1A639D3027F9BB99 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          121        152         28              114
PID                            610AD97DD4419E4A9C427706A9C9812C FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          120        153         25              114
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          119        149         68              114
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          118        149         53              117
PID                            610AD97DD4419E4A9C427706A9C9812C FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        117        149         46              114
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE          116        149         36              115
PNOMBRE                        E414344D4CEC26CC1A639D3027F9BB99 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        115        149         25              114
UPDATEPAIS                     10B1B8775045D42108F8BFED13F877AF FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION         114        149         14                1
TEMP_ID                        0EB5FF3582C6157B9D9F50E225D83986 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE          113        146         16              105
TEMP_ID                        0EB5FF3582C6157B9D9F50E225D83986 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT         112        142         14              105

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        7A18291D131071544F138CD81FEABCC6 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          111        144         35              105
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          110        139         13              109
TEMP_ID                        0EB5FF3582C6157B9D9F50E225D83986 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION        109        139          5              105
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          108        138         64              105
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE          107        138         47              106
PNOMBRE                        7A18291D131071544F138CD81FEABCC6 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION        106        138         36              105
GETIDZONAAVISTAMIENTO          3E666F3EC24415794E59274AFC269502 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION         105        138         14                1
TEMP_ID                        854F97DFB85F59A9C278EC3C761F34C1 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE          104        135         16               96
TEMP_ID                        854F97DFB85F59A9C278EC3C761F34C1 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT         103        131         14               96
PNOMBRE                        7DAE0D4AD0EB88D5C6019FEEF916E8D4 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE          102        133         30               96
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE          101        128         13              100

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_ID                        854F97DFB85F59A9C278EC3C761F34C1 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION        100        128          5               96
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           99        127         56               96
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           98        127         39               97
PNOMBRE                        7DAE0D4AD0EB88D5C6019FEEF916E8D4 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         97        127         28               96
GETIDDISTRITO                  8D6FD8E6F4CF19DFCC3A7BEBAF8403F2 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          96        127         14                1
PDESCRIPCION                   C3ECBAA251942A8938506816A014BB85 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         13          9         35               12
UPDATEDESCRIPCIONROL           14D948E2F2C7947BDC80B750EE7A33D5 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         12          9         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           11          7         56                8
LISTA_NOMBRES                  3D15371E7245A95D0A4DDA0DBBDF7E34 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        141        159          8              139
RECORD_USUARIO                 012A8663BC9DE10FE22969A3A0CD4843 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          140        157         33              139
GETUSUARIOS                    9B5C5DF84316E9AEE81F71D997AD55AB FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION         139        157         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  415D10E917E91CC567780DE6FACC3E0A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          138        154         16              107
DATO                           F8D0C80F8AEFEF600F61C73EDBCFB449 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          136        152         29              127
DATO                           F8D0C80F8AEFEF600F61C73EDBCFB449 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          134        151         72              127
DATO                           F8D0C80F8AEFEF600F61C73EDBCFB449 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          132        151         50              127
DATO                           F8D0C80F8AEFEF600F61C73EDBCFB449 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          130        151         29              127
DATO                           F8D0C80F8AEFEF600F61C73EDBCFB449 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          128        150         72              127
T_ADMINISTRADOR                548B407727FD344B693DEF7D90DAC0D8 OBJECT             PAQ_ADMINISTRACION             PACKAGE BODY  CALL               127        150         56              124
LISTA_NOMBRES                  415D10E917E91CC567780DE6FACC3E0A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          126        150         31              125
LISTA_NOMBRES                  415D10E917E91CC567780DE6FACC3E0A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         124        150         17              120
LISTA_NOMBRES                  415D10E917E91CC567780DE6FACC3E0A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          123        149         17              122
TEMP_ID                        E169EC7EA5A752F9DED16C4B7CB3717F VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          121        148         27              120

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           F8D0C80F8AEFEF600F61C73EDBCFB449 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        120        145         14              107
ROL_ADMINISTRADOR              22B20230F8D778554987A563304C6E12 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          119        143         44              118
PAQ_ADMINISTRACION             EEAC7F41E96B0E0E0BA343B3097DEA29 PACKAGE            PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          118        143         25              117
GETROLID                       24A8FC7E663D7D74C2C6D6C0B776C525 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  CALL               117        142         45              116
PAQ_ADMINISTRACION             EEAC7F41E96B0E0E0BA343B3097DEA29 PACKAGE            PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          116        142         26              115
TEMP_ID                        E169EC7EA5A752F9DED16C4B7CB3717F VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         115        142          8              113
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          114        142         16              113
TEMP_ID                        E169EC7EA5A752F9DED16C4B7CB3717F VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        113        142          8              107
RECORD_ADMINISTRADOR           D181281C2D3CEE42A86E586E96E71AF7 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  CALL               112        141         47              111
LISTA_NOMBRES                  415D10E917E91CC567780DE6FACC3E0A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         111        141          8              109
RECORD_ADMINISTRADOR           D181281C2D3CEE42A86E586E96E71AF7 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          110        141         23              109

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  415D10E917E91CC567780DE6FACC3E0A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        109        141          8              107
RECORD_ADMINISTRADOR           D181281C2D3CEE42A86E586E96E71AF7 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          108        139         40              107
GETADMINISTRADORES             51ADB2FD85340F62643599F015429D66 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION         107        139         14                1
LISTA_NOMBRES                  BF56BD47DA1B1CC360366E6EB4A73451 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          106        136         16               91
DATO                           D63ACE743006413AD647FCE354D41C81 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          104        134         65              103
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_ADMINISTRACION             PACKAGE BODY  CALL               103        134         56              100
LISTA_NOMBRES                  BF56BD47DA1B1CC360366E6EB4A73451 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          102        134         31              101
LISTA_NOMBRES                  BF56BD47DA1B1CC360366E6EB4A73451 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         100        134         17               97
LISTA_NOMBRES                  BF56BD47DA1B1CC360366E6EB4A73451 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           99        133         17               98
DATO                           D63ACE743006413AD647FCE354D41C81 ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         97        132         14               91
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  CALL                96        130         37               95

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  BF56BD47DA1B1CC360366E6EB4A73451 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          95        130          8               93
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           94        130         23               93
LISTA_NOMBRES                  BF56BD47DA1B1CC360366E6EB4A73451 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         93        130          8               91
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           92        128         30               91
GETROLES                       523CAE98C06FE5A931521AC0152211E4 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          91        128         14                1
PUSUARIO                       32317F49777C88C8D62ECA0769989178 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           90        120         49               76
PDESCRIPCION                   BED308D371AA5D1BA18D12713201C368 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           89        118         59               76
USUARIO_ACTUAL                 C009243F6BFAF273C2EBB7A6728D4F98 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           88        115         25               76
PUSUARIO                       32317F49777C88C8D62ECA0769989178 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           87        115         13               76
GETUSUARIOACTUAL               23407D986441AB14C7033A9AE4BCCFD7 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  CALL                86        113         48               85
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           85        113         36               84

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
USUARIO_ACTUAL                 C009243F6BFAF273C2EBB7A6728D4F98 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          84        113          5               82
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           83        113         20               82
USUARIO_ACTUAL                 C009243F6BFAF273C2EBB7A6728D4F98 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         82        113          5               76
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           81        112         12               76
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           80        111         69               79
PUSUARIO                       32317F49777C88C8D62ECA0769989178 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         79        111         57               76
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           78        111         47               77
PDESCRIPCION                   BED308D371AA5D1BA18D12713201C368 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         77        111         31               76
UPDATEROLPERSONA               D066F69697AEAD31C5A4E75A9EABC424 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          76        111         14                1
PDESCRIPCION                   847E5245737FE87FC230BE929989F0DC FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           75        101         33               68
PID                            BFB14773C215056E0E67C5D6D5FF075C FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           74        102         24               68

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           73         98         12               68
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           72         97         67               71
PID                            BFB14773C215056E0E67C5D6D5FF075C FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         71         97         60               68
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           70         97         51               69
PDESCRIPCION                   847E5245737FE87FC230BE929989F0DC FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         69         97         35               68
UPDATEDESCRIPCIONROL           14D948E2F2C7947BDC80B750EE7A33D5 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          68         97         14                1
COD_COLOR                      ED3D861E1134EBA36C86FE5A36CBCC6B VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           66         89         17               56
PDESCRIPCION                   355868C494DC6B41B7D0D06CB65BBC24 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           65         89         33               56
PID_ESPECIE                    0D8C44E5AD0386CC1EC032F9DE43291D FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           64         89         47               56
COD_COLOR                      ED3D861E1134EBA36C86FE5A36CBCC6B VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          63         85         14               56
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           62         82         15               61

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
COD_COLOR                      ED3D861E1134EBA36C86FE5A36CBCC6B VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         61         82          5               56
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           60         80         69               59
PID_ESPECIE                    0D8C44E5AD0386CC1EC032F9DE43291D FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         59         80         54               56
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           58         80         44               57
PDESCRIPCION                   355868C494DC6B41B7D0D06CB65BBC24 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         57         80         28               56
INSERTACOLOR                   B4D05865224018839DA47BF38718C59D PROCEDURE          PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          56         80         15                1
COD_TAMANNO                    DD9CB6F5BD672608635C29932D217AA1 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           54         72         17               44
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           34         21         68               33
PID_PERSONA                    5A983B319A6FD6DF18D1E194CBAFAF2C FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         33         21         53               30
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           32         21         44               31
PIMAGEN                        6073C9BFE8EC0B11FA6D674F33037D15 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         31         21         33               30

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
INSERTAFOTOPERFIL              4D14B752640795D8EFDFA4C44F0F890A PROCEDURE          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         30         21         15                1
RECORD_USUARIO                 012A8663BC9DE10FE22969A3A0CD4843 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE       REFERENCE           29         19         33               28
GETUSUARIOS                    9B5C5DF84316E9AEE81F71D997AD55AB FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         28         19         14                1
RECORD_ADMINISTRADOR           D181281C2D3CEE42A86E586E96E71AF7 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE       REFERENCE           27         17         40               26
GETADMINISTRADORES             51ADB2FD85340F62643599F015429D66 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         26         17         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE       REFERENCE           25         15         30               24
GETROLES                       523CAE98C06FE5A931521AC0152211E4 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         24         15         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           23         13         12               18
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           22         12         69               21
PUSUARIO                       30DB6F3028F05974013098EB2C53F8C9 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         21         12         57               18
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           20         12         47               19

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDESCRIPCION                   A2C2E912A3320E1AC83C7221DF9CC0B7 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         19         12         31               18
UPDATEROLPERSONA               D066F69697AEAD31C5A4E75A9EABC424 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         18         12         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           17         10         12               12
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           16          9         68               15
PID                            4D9E2D58F38AE9DF7DCB0DCA9833AB9B FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         15          9         61               12
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           14          9         51               13
PDESCRIPCION                   6FC73FF7F6EB0ED827AF9BA324DAD161 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           53         72         35               44
PID_ESPECIE                    0F59565B06FE28920ED4806A1311855B FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           52         72         49               44
COD_TAMANNO                    DD9CB6F5BD672608635C29932D217AA1 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          51         68         14               44
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           50         65         17               49
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           10          7         39                9

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDESCRIPCION                   4189D3C68C348A2B1F95AF8114E6F25E FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION          9          7         23                8
GETROLID                       24A8FC7E663D7D74C2C6D6C0B776C525 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION          8          7         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE            7          5         40                6
PDESCRIPCION                   24FD288FF21E429C7ECAFE4AB8C6E766 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION          6          5         24                5
CREARROL                       415B8210C64EA47464A9C727D0B17DEB PROCEDURE          PAQ_ADMINISTRACION             PACKAGE       DECLARATION          5          5         15                1
ROL_ADMINISTRADOR              22B20230F8D778554987A563304C6E12 VARIABLE           PAQ_ADMINISTRACION             PACKAGE       ASSIGNMENT           4          3          5                2
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_ADMINISTRACION             PACKAGE       REFERENCE            3          3         23                2
ROL_ADMINISTRADOR              22B20230F8D778554987A563304C6E12 VARIABLE           PAQ_ADMINISTRACION             PACKAGE       DECLARATION          2          3          5                1
PAQ_ADMINISTRACION             EEAC7F41E96B0E0E0BA343B3097DEA29 PACKAGE            PAQ_ADMINISTRACION             PACKAGE       DECLARATION          1          1          9                0
COD_TAMANNO                    DD9CB6F5BD672608635C29932D217AA1 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         49         65          5               44
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           48         63         71               47

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_ESPECIE                    0F59565B06FE28920ED4806A1311855B FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         47         63         56               44
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           46         63         46               45
PDESCRIPCION                   6FC73FF7F6EB0ED827AF9BA324DAD161 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         45         63         30               44
INSERTATAMANNO                 9A2FF51F36D3A2D19B3F8BBC5D919E3C PROCEDURE          PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          44         63         15                1
COD_IMAGEN                     3089C18772677C963888CCE5C7648E37 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           42         55         17               32
PIMAGEN                        C1126C7787B754ED1AAD50D64DFC796A FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           41         55         29               32
PID_AVISTAMIENTO               EF37DB25BE45640D437B9C67CA098972 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           40         55         37               32
COD_IMAGEN                     3089C18772677C963888CCE5C7648E37 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          39         51         14               32
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           38         48         16               37
COD_IMAGEN                     3089C18772677C963888CCE5C7648E37 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         37         48          5               32
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           36         47         25               35

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_AVISTAMIENTO               EF37DB25BE45640D437B9C67CA098972 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         35         47          5               32
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           34         46         50               33
PIMAGEN                        C1126C7787B754ED1AAD50D64DFC796A FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         33         46         39               32
INSERTAFOTOAVISTAMIENTO        33B6218B6CBD66EF59CA99CEF1F4311C PROCEDURE          PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          32         46         15                1
COD_IMAGEN                     536C610CB4A18D0457505725A4F9CF81 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           30         38         17               20
PIMAGEN                        56434393BAC0E643F0BCD5B9DB1AD5A6 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           29         38         29               20
PID_PERSONA                    4E1D30E4BDBA8CA729D844D1C15B5B4E FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           28         38         37               20
COD_IMAGEN                     536C610CB4A18D0457505725A4F9CF81 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          27         34         14               20
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           26         31         16               25
COD_IMAGEN                     536C610CB4A18D0457505725A4F9CF81 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         25         31          5               20
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           24         29         68               23

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_PERSONA                    4E1D30E4BDBA8CA729D844D1C15B5B4E FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         23         29         53               20
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           22         29         44               21
PIMAGEN                        56434393BAC0E643F0BCD5B9DB1AD5A6 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         21         29         33               20
INSERTAFOTOPERFIL              4D14B752640795D8EFDFA4C44F0F890A PROCEDURE          PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          20         29         15                1
COD_ROL                        9E41D8803C2E04FF5A02F9CB611D68CB VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           19         26         16               11
COD_ROL                        9E41D8803C2E04FF5A02F9CB611D68CB VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT          18         23         14               11
PDESCRIPCION                   19F96FCE755547BBA6CF4761803D752D FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           17         25         35               11
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           16         20         13               15
COD_ROL                        9E41D8803C2E04FF5A02F9CB611D68CB VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         15         20          5               11
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           14         18         56               11
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE           13         18         39               12

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDESCRIPCION                   19F96FCE755547BBA6CF4761803D752D FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION         12         18         23               11
GETROLID                       24A8FC7E663D7D74C2C6D6C0B776C525 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION          11         18         14                1
COD_ROL                        64959E53D93F35FCEE2CB475F0816816 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE            9         10         16                2
PDESCRIPCION                   06D603988E482E21AC412DD725C28ADA FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE            8         10         30                2
COD_ROL                        64959E53D93F35FCEE2CB475F0816816 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT           7          6         14                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE            6          3         13                5
COD_ROL                        64959E53D93F35FCEE2CB475F0816816 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION          5          3          5                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE            4          2         40                3
PDESCRIPCION                   06D603988E482E21AC412DD725C28ADA FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION          3          2         24                2
CREARROL                       415B8210C64EA47464A9C727D0B17DEB PROCEDURE          PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION           2          2         15                1
PAQ_ADMINISTRACION             EEAC7F41E96B0E0E0BA343B3097DEA29 PACKAGE            PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION           1          1         14                0

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTA                     TYPE          REFERENCE           13          3         54               12
ESPECIE                        494B4C2608256BB3F91631B416891B0D VARIABLE           T_CONSULTA                     TYPE          DECLARATION         12          3         46                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTA                     TYPE          REFERENCE           11          3         31               10
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_NOTIFICACION               PACKAGE       REFERENCE           15          4         65               14
PVALOR                         93CAB72293D63E21EF4F7E1565520151 FORMAL IN          PAQ_NOTIFICACION               PACKAGE       DECLARATION         14          4         55               11
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE       REFERENCE           13          4         45               12
PPARAMETRO                     CD80C2D1E44D022B33C037AD85FA5B8F FORMAL IN          PAQ_NOTIFICACION               PACKAGE       DECLARATION         12          4         31               11
UPDATEPARAMETRO                DC1FE0740BDA8B14A6B322651B34D3AA PROCEDURE          PAQ_NOTIFICACION               PACKAGE       DECLARATION         11          4         15                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE       REFERENCE           10          3         58                7
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE       REFERENCE            9          3         41                8
PPARAMETRO                     330503567A69A01B6E3E12A59CAC0462 FORMAL IN          PAQ_NOTIFICACION               PACKAGE       DECLARATION          8          3         27                7

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GETPARAMETRO                   238D599EB0896BB16801E67B3975301A FUNCTION           PAQ_NOTIFICACION               PACKAGE       DECLARATION          7          3         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE       REFERENCE            6          2         63                5
PPARAMETRO                     592B496BCAC277E20B532C45C6DB0782 FORMAL IN          PAQ_NOTIFICACION               PACKAGE       DECLARATION          5          2         49                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE       REFERENCE            4          2         39                3
PVALOR                         70F51EB133B43ABF19B85E901E250A5A FORMAL IN          PAQ_NOTIFICACION               PACKAGE       DECLARATION          3          2         29                2
CREAPARAMETRO                  AE91784790C997D363AE7B5A596AD8A1 PROCEDURE          PAQ_NOTIFICACION               PACKAGE       DECLARATION          2          2         15                1
PAQ_NOTIFICACION               CA08D5829C5DD2A7B65A0DC1C4D4589B PACKAGE            PAQ_NOTIFICACION               PACKAGE       DECLARATION          1          1          9                0
PVALOR                         6260E51A4F638BDE2525D0105A853775 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           28         28         27               22
PPARAMETRO                     8191253EFC5720FA42AAE1679D7B8221 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           27         29         33               22
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           26         25         65               25
PVALOR                         6260E51A4F638BDE2525D0105A853775 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION         25         25         55               22

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           24         25         45               23
PPARAMETRO                     8191253EFC5720FA42AAE1679D7B8221 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION         23         25         31               22
UPDATEPARAMETRO                DC1FE0740BDA8B14A6B322651B34D3AA PROCEDURE          PAQ_NOTIFICACION               PACKAGE BODY  DEFINITION          22         25         15                1
TEMP_MENSAJE                   578EC712A6546DF08D25F511EC8FD26B VARIABLE           PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           21         22         16               13
TEMP_MENSAJE                   578EC712A6546DF08D25F511EC8FD26B VARIABLE           PAQ_NOTIFICACION               PACKAGE BODY  ASSIGNMENT          20         19         14               13
PPARAMETRO                     A1161680478A5C3BCBFF47FA461A7A71 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           19         21         33               13
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           18         16         18               17
TEMP_MENSAJE                   578EC712A6546DF08D25F511EC8FD26B VARIABLE           PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION         17         16          5               13
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           16         15         58               13
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           15         15         41               14
PPARAMETRO                     A1161680478A5C3BCBFF47FA461A7A71 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION         14         15         27               13

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GETPARAMETRO                   238D599EB0896BB16801E67B3975301A FUNCTION           PAQ_NOTIFICACION               PACKAGE BODY  DEFINITION          13         15         14                1
TEMP_CONTADOR                  1D7EE26FB0AE6B4E2657B2DC32CB4F2C VARIABLE           PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           12         11         16                2
PVALOR                         B8AAF3243AEE7FE0BED946831C80EB77 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           11         11         36                2
PPARAMETRO                     4A2F453E20EF74BEB7BA971306891260 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE           10         11         50                2
TEMP_CONTADOR                  1D7EE26FB0AE6B4E2657B2DC32CB4F2C VARIABLE           PAQ_NOTIFICACION               PACKAGE BODY  ASSIGNMENT           9          7         14                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE            8          4         19                7
TEMP_CONTADOR                  1D7EE26FB0AE6B4E2657B2DC32CB4F2C VARIABLE           PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION          7          4          5                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE            6          2         62                5
PPARAMETRO                     4A2F453E20EF74BEB7BA971306891260 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION          5          2         48                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_NOTIFICACION               PACKAGE BODY  REFERENCE            4          2         39                3
PVALOR                         B8AAF3243AEE7FE0BED946831C80EB77 FORMAL IN          PAQ_NOTIFICACION               PACKAGE BODY  DECLARATION          3          2         29                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
CREAPARAMETRO                  AE91784790C997D363AE7B5A596AD8A1 PROCEDURE          PAQ_NOTIFICACION               PACKAGE BODY  DEFINITION           2          2         15                1
PAQ_NOTIFICACION               CA08D5829C5DD2A7B65A0DC1C4D4589B PACKAGE            PAQ_NOTIFICACION               PACKAGE BODY  DEFINITION           1          1         14                0
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE       REFERENCE           11          3         58                8
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE       REFERENCE           10          3         44                9
PIDPERSONA                     8E7ECF8B8EF4ECF349A333F211B64168 FORMAL IN          PAQ_LOGIN                      PACKAGE       DECLARATION          9          3         33                8
VALIDAR_ADMINISTRADOR          13D5D91F7A6F30C0E6011CA969313396 FUNCTION           PAQ_LOGIN                      PACKAGE       DECLARATION          8          3         11                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE       REFERENCE            7          2         72                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOGIN                      PACKAGE       REFERENCE            6          2         55                5
PCONTRASENA                    72D7D4B2584955E36D07893B5D49C4C3 FORMAL IN          PAQ_LOGIN                      PACKAGE       DECLARATION          5          2         43                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOGIN                      PACKAGE       REFERENCE            4          2         34                3
PNOMBRE                        ECBCEC2426DD94E8513BF7F1FFC91D62 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          187        254         28              180

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_CAMBIO                     6D5751C3FA1A7B0ABA6E13322996EC20 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          186        255         28              180
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          185        251         12              180
NO_DATA_FOUND                  771CF79A4633F282711247BCCC801E03 EXCEPTION          PAQ_LOGIN                      PACKAGE BODY  REFERENCE           30         27         10               21
TEMP_IDROL_USUARIO             E27FD0A398328909C76BEA24537614B6 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  REFERENCE           29         22          5               21
TEMP_IDROL_USUARIO             E27FD0A398328909C76BEA24537614B6 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  ASSIGNMENT          28         21         24               21
PIDPERSONA                     118C697E761DA46EE051BA039648B3F4 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  REFERENCE           27         21         75               21
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE BODY  REFERENCE           26         19         24               25
TEMP_IDROL_USUARIO             E27FD0A398328909C76BEA24537614B6 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  DECLARATION         25         19          5               21
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE BODY  REFERENCE           24         18         59               21
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE BODY  REFERENCE           23         18         45               22
PIDPERSONA                     118C697E761DA46EE051BA039648B3F4 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  DECLARATION         22         18         34               21

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VALIDAR_ADMINISTRADOR          13D5D91F7A6F30C0E6011CA969313396 FUNCTION           PAQ_LOGIN                      PACKAGE BODY  DEFINITION          21         18         12                1
NO_DATA_FOUND                  771CF79A4633F282711247BCCC801E03 EXCEPTION          PAQ_LOGIN                      PACKAGE BODY  REFERENCE           20         14         14                2
PUSUARIO                       2C39CF97706E21486FE142FCB403D4A5 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  REFERENCE           19          9         40               18
SETIDPERSONAACTUAL             B7F341C767789A1F821E380A980C72D5 PROCEDURE          PAQ_LOGIN                      PACKAGE BODY  CALL                18          9         21               17
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_LOGIN                      PACKAGE BODY  REFERENCE           17          9          9                2
PCONTRASENA                    8CD2F4BB8DD75960192D1E1DA6C275C5 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  REFERENCE           16          8         24                2
TEMP_CONTRASENA                133E4C9C3E52F5E9118B20B869450D45 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  REFERENCE           15          8          8                2
PUSUARIO                       2C39CF97706E21486FE142FCB403D4A5 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  REFERENCE           14          6         50               13
GETCONTRASENA                  26F7EA199E06D1132251A99DE8DF2D17 FUNCTION           PAQ_LOGIN                      PACKAGE BODY  CALL                13          6         36               12
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_LOGIN                      PACKAGE BODY  REFERENCE           12          6         24               11
TEMP_CONTRASENA                133E4C9C3E52F5E9118B20B869450D45 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  ASSIGNMENT          11          6          5                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_CONTRASENA                133E4C9C3E52F5E9118B20B869450D45 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  ASSIGNMENT          10          3          5                8
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOGIN                      PACKAGE BODY  REFERENCE            9          3         21                8
TEMP_CONTRASENA                133E4C9C3E52F5E9118B20B869450D45 VARIABLE           PAQ_LOGIN                      PACKAGE BODY  DECLARATION          8          3          5                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOGIN                      PACKAGE BODY  REFERENCE            7          2         73                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOGIN                      PACKAGE BODY  REFERENCE            6          2         56                5
PCONTRASENA                    8CD2F4BB8DD75960192D1E1DA6C275C5 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  DECLARATION          5          2         44                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOGIN                      PACKAGE BODY  REFERENCE            4          2         35                3
PUSUARIO                       2C39CF97706E21486FE142FCB403D4A5 FORMAL IN          PAQ_LOGIN                      PACKAGE BODY  DECLARATION          3          2         26                2
VALIDAR_LOGIN                  32A752902AE98BEE231EA0F792225C51 FUNCTION           PAQ_LOGIN                      PACKAGE BODY  DEFINITION           2          2         12                1
PAQ_LOGIN                      11E51E6D7EAFA9E83ED6A0DD86DC99D6 PACKAGE            PAQ_LOGIN                      PACKAGE BODY  DEFINITION           1          1         14                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE DBCRYPT                        PACKAGE       REFERENCE            9          3         44                6

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE       REFERENCE            8          3         31                7
P_DATA                         B9EE916A4C8A6C1563D89FD1223B95E5 FORMAL IN          DBCRYPT                        PACKAGE       DECLARATION          7          3         21                6
DECRYPT                        919F064CD87E01FD4470CC244F8B7A01 FUNCTION           DBCRYPT                        PACKAGE       DECLARATION          6          3         12                1
PUSUARIO                       B8D065EF7A2E4474767D79C4DE8A6814 FORMAL IN          PAQ_LOGIN                      PACKAGE       DECLARATION          3          2         25                2
VALIDAR_LOGIN                  32A752902AE98BEE231EA0F792225C51 FUNCTION           PAQ_LOGIN                      PACKAGE       DECLARATION          2          2         11                1
PAQ_LOGIN                      11E51E6D7EAFA9E83ED6A0DD86DC99D6 PACKAGE            PAQ_LOGIN                      PACKAGE       DECLARATION          1          1          9                0
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           97         22         78               92
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           96         22         63               95
PID_CAMBIO                     1E75AC698639DD51D2E5CABFCB76B258 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         95         22         49               92
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           94         22         39               93
PNOMBRE                        BAA327B11C9EF65974F136A52A5D21E5 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         93         22         28               92

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
UPDATEESPECIE                  BEFB554AC90DA3EC5A6A54D3ADF54735 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         92         22         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           91         21         77               86
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           90         21         62               89
PID_CAMBIO                     ED2E5BFDF4953C914A7195BC9F11DF7F FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         89         21         48               86
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           88         21         38               87
PNOMBRE                        D1484C349BB076490FA68C12A84BA9D9 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         87         21         27               86
UPDATEGENERO                   E71C685846DFBAA35F5B6BB810D1F19D FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         86         21         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           85         20         78               80
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           84         20         63               83
PID_CAMBIO                     6FD04964EAF8C654581B3E5584CAA910 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         83         20         49               80
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           82         20         39               81

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        C1D7BADF8695BF6F7F87C9644F8C1708 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         81         20         28               80
UPDATEFAMILIA                  F15EE5BAFF90661D07DFC5FBE1E6CF29 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         80         20         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           79         19         79               74
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           78         19         64               77
PID_CAMBIO                     F48A0F9A1DACCE4EBD4FC315F8F32410 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         77         19         50               74
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           76         19         40               75
PNOMBRE                        ADBF56B7F807C341F889B869D964208A FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         75         19         29               74
UPDATESUBORDEN                 A6C5F7511D7F68F08607E46CAA338229 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         74         19         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           73         18         76               68
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           72         18         61               71
PID_CAMBIO                     E682E7AE636651DAD0CD58B30D72D6EC FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         71         18         47               68

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           70         18         37               69
PNOMBRE                        49E643053E9BC4377E171E30C2A7CDF1 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         69         18         26               68
UPDATEORDEN                    E78AD0FF542499E2A66ACCD21651D01E FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         68         18         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           67         17         82               62
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           66         17         67               65
PID_CAMBIO                     CF0B6493E7CB8935158505EE897D3F5A FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         65         17         53               62
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           64         17         43               63
PNOMBRE                        CA1825E8BE1F8385F4C027219FDE3D44 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         63         17         32               62
UPDATECLASENOMBRE              5848A5A0B97524894DFDBD7BB770FB9F FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         62         17         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           61         15         55               58
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           60         15         38               59

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        04AD18156A68E04CFDDFFFBD5ADD4369 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         59         15         27               58
GETIDESPECIE                   F1E5C89F9EA019984AFDEFCA9B23759B FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         58         15         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           57         14         54               54
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           56         14         37               55
PNOMBRE                        86D59B15CEFDCAD31A4E778C98D21BBD FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         55         14         26               54
GETIDGENERO                    44F9CEEBE99340A99578D3BF00F4DBEB FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         54         14         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           53         13         55               50
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           52         13         38               51
PNOMBRE                        6163E831B02FBCF192051DEDD2AC35B8 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         51         13         27               50
GETIDFAMILIA                   BB5FCC695006E27EAAFEC74A389CD4A4 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         50         13         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           49         12         57               46

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           48         12         40               47
PNOMBRE                        28D622904FE9E72CA9A781B223EE9D92 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         47         12         29               46
GETIDSUB_ORDEN                 C82E89E024221D49051F822CA1D0A0EE FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         46         12         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           45         11         53               42
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           44         11         36               43
PNOMBRE                        FDB62C1E11EE75A74F9F3ABAE145B1C9 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         43         11         25               42
GETIDORDEN                     9FEA4ADC0C58FD77F24EDF36BBE1981D FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         42         11         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           41         10         53               38
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           40         10         36               39
PNOMBRE                        489C136F3307248CBC0A7F8092CFC235 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         39         10         25               38
GETIDCLASE                     5F20EF9E028383620DF01901D748AC26 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         38         10         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           37          8         58               30
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           36          8         43               35
PID_GENERO                     2084BD6D02D17EF3D403F61CBE7E4998 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         35          8         29               30
CHAR                           827DC51E93B66D1B683E1965CB2103FD SUBTYPE            PAQ_ANIMALES                   PACKAGE       REFERENCE           34          7         66               33
PEXTINCION                     68AF39041B34B955A2C5987E36F7B27B FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         33          7         52               30
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           32          7         42               31
PESPECIE                       0CAD026B37425EDD26835401C661F471 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         31          7         30               30
INSERTARESPECIE                3D2865A48C78BE4FA50236DB5C005381 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         30          7         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           29          6         80               24
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           28          6         65               27
PID_FAMILIA                    1544AFEAF5A8EBFA1202F47C6C39DD37 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         27          6         50               24

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           26          6         40               25
TEMP_ID                        9D0293D8735D9D6621590B96749501D7 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         90        116          5               86
PGENERO                        2F7B309C08D12C99A1605C734E4C2091 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         25          6         29               24
INSERTARGENERO                 B30B9B73DCE9D4B4149B81C63C5C7A32 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         24          6         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           23          5         83               18
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           22          5         68               21
PID_SUBORDEN                   C5A07DA4933FDB2778032AD6D0547E6E FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         21          5         52               18
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           20          5         42               19
PFAMILIA                       B242E9C85366189875F1A788CCCFDCE5 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         19          5         30               18
INSERTARFAMILIA                2387AA98DD588022B23F1F7EE1121AC5 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         18          5         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           17          4         83               12

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           16          4         68               15
PID_ORDEN                      A9252CAA28AF1120F7642FEA497B2580 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         15          4         55               12
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE           14          4         45               13
PSUBORDEN                      A91F9D4E50808FC4F333B039C8C24787 FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION         13          4         32               12
INSERTARSUB_ORDEN              FAA5847565C45351B90110514C577A92 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION         12          4         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           11          3         76                6
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE           10          3         61                9
PID_CLASE                      5AE5B54374BD661A0536D8D357274E5B FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION          9          3         48                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE            8          3         38                7
PORDEN                         B8D549B9BB9D2DA672E31AA5132DA4AC FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION          7          3         28                6
INSERTARORDEN                  30F0FBC90C31DB1B1163EB0FF604F3ED FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION          6          3         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE       REFERENCE            5          2         56                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE       REFERENCE            4          2         39                3
PNOMBRE                        5A5F0FE2C5F67998EFB789B777A1A07D FORMAL IN          PAQ_ANIMALES                   PACKAGE       DECLARATION          3          2         28                2
INSERTARCLASE                  3F49EF15FA4311ED254D7D48F9DB76D2 FUNCTION           PAQ_ANIMALES                   PACKAGE       DECLARATION          2          2         14                1
PAQ_ANIMALES                   CA26D34F4D8C84D90D37E711DAEFD335 PACKAGE            PAQ_ANIMALES                   PACKAGE       DECLARATION          1          1          9                0
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          184        250         63              183
PID_CAMBIO                     6D5751C3FA1A7B0ABA6E13322996EC20 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        183        250         49              180
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          182        250         39              181
PNOMBRE                        ECBCEC2426DD94E8513BF7F1FFC91D62 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        181        250         28              180
UPDATEESPECIE                  BEFB554AC90DA3EC5A6A54D3ADF54735 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         180        250         14                1
PNOMBRE                        A60A5C947D3947F554827FC9B1E4AAFF FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          179        240         28              172

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_CAMBIO                     B255F29A33153A9FF4C2531A4CCF9EA1 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          178        241         27              172
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          177        237         12              172
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          176        236         62              175
PID_CAMBIO                     B255F29A33153A9FF4C2531A4CCF9EA1 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        175        236         48              172
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          174        236         38              173
PNOMBRE                        A60A5C947D3947F554827FC9B1E4AAFF FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        173        236         27              172
UPDATEGENERO                   E71C685846DFBAA35F5B6BB810D1F19D FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         172        236         14                1
PNOMBRE                        D1E11EC773CC5C31AAC9DF9BC20399AC FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          171        226         28              164
PID_CAMBIO                     DDC370838D8A1BFEA4482553628EF374 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          170        227         28              164
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          169        223         12              164
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          168        222         63              167

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_CAMBIO                     DDC370838D8A1BFEA4482553628EF374 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        167        222         49              164
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          166        222         39              165
PNOMBRE                        D1E11EC773CC5C31AAC9DF9BC20399AC FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        165        222         28              164
UPDATEFAMILIA                  F15EE5BAFF90661D07DFC5FBE1E6CF29 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         164        222         14                1
PNOMBRE                        A31C9996B8399C05CD0936213EA14BC9 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          163        212         28              156
PID_CAMBIO                     E54536F3AA8AFCB1CA038B27E9A6A459 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          162        213         29              156
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          161        209         12              156
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          160        208         64              159
PID_CAMBIO                     E54536F3AA8AFCB1CA038B27E9A6A459 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        159        208         50              156
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          158        208         40              157
PNOMBRE                        A31C9996B8399C05CD0936213EA14BC9 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        157        208         29              156

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
UPDATESUBORDEN                 A6C5F7511D7F68F08607E46CAA338229 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         156        208         14                1
PNOMBRE                        5CB0E3544768679120CE3AE124682AAB FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          155        198         28              148
PID_CAMBIO                     54DCEF464D77A9D5418E297F11136153 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          154        199         26              148
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          153        195         12              148
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          152        194         61              151
PID_CAMBIO                     54DCEF464D77A9D5418E297F11136153 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        151        194         47              148
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          150        194         37              149
PNOMBRE                        5CB0E3544768679120CE3AE124682AAB FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        149        194         26              148
UPDATEORDEN                    E78AD0FF542499E2A66ACCD21651D01E FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         148        194         14                1
PNOMBRE                        800B15594FD9BAE5CABC40C1C9868DBE FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          147        184         28              140
PID_CAMBIO                     BEE0635770BF9DA566904410A6C2DD6D FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          146        185         26              140

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          145        181         12              140
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          144        180         67              143
PID_CAMBIO                     BEE0635770BF9DA566904410A6C2DD6D FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        143        180         53              140
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          142        180         43              141
PNOMBRE                        800B15594FD9BAE5CABC40C1C9868DBE FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        141        180         32              140
UPDATECLASENOMBRE              5848A5A0B97524894DFDBD7BB770FB9F FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         140        180         14                1
TEMP_ID                        190D6211259BCE1900A1D1C29538A1D9 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          139        177         16              131
TEMP_ID                        190D6211259BCE1900A1D1C29538A1D9 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT         138        173         14              131
PNOMBRE                        E18002C7F31759F92200E195E00FFD8B FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          137        175         30              131
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          136        170         13              135
TEMP_ID                        190D6211259BCE1900A1D1C29538A1D9 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        135        170          5              131

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          134        169         55              131
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          133        169         38              132
PNOMBRE                        E18002C7F31759F92200E195E00FFD8B FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        132        169         27              131
GETIDESPECIE                   F1E5C89F9EA019984AFDEFCA9B23759B FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         131        169         14                1
TEMP_ID                        3950AE03D36BC6F4DAFE6F365970FD54 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          130        166         16              122
TEMP_ID                        3950AE03D36BC6F4DAFE6F365970FD54 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT         129        162         14              122
PNOMBRE                        21A9A63AE1358EA72A5EE85AD49BF28F FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          128        164         30              122
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          127        159         13              126
TEMP_ID                        3950AE03D36BC6F4DAFE6F365970FD54 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        126        159          5              122
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          125        158         54              122
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          124        158         37              123

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        21A9A63AE1358EA72A5EE85AD49BF28F FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        123        158         26              122
GETIDGENERO                    44F9CEEBE99340A99578D3BF00F4DBEB FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         122        158         14                1
TEMP_ID                        3D1E637135BB623E5136068B2A4D9B4A VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          121        155         16              113
TEMP_ID                        3D1E637135BB623E5136068B2A4D9B4A VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT         120        151         14              113
PNOMBRE                        D0072540BC259B670B7C4397410315B7 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          119        153         30              113
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          118        148         13              117
TEMP_ID                        3D1E637135BB623E5136068B2A4D9B4A VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        117        148          5              113
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          116        147         55              113
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          115        147         38              114
PNOMBRE                        D0072540BC259B670B7C4397410315B7 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        114        147         27              113
GETIDFAMILIA                   BB5FCC695006E27EAAFEC74A389CD4A4 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         113        147         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_ID                        1C4F25ECDBE8B6003B70B288B756F3B4 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          112        144         16              104
TEMP_ID                        1C4F25ECDBE8B6003B70B288B756F3B4 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT         111        140         14              104
PNOMBRE                        D612F1362A562F8BAA9E82C9CA1ACC8D FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          110        142         30              104
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          109        137         13              108
TEMP_ID                        1C4F25ECDBE8B6003B70B288B756F3B4 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        108        137          5              104
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          107        136         57              104
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          106        136         40              105
PNOMBRE                        D612F1362A562F8BAA9E82C9CA1ACC8D FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION        105        136         29              104
GETIDSUB_ORDEN                 C82E89E024221D49051F822CA1D0A0EE FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION         104        136         14                1
TEMP_ID                        56C47CF8E9AABA4B9D0BF960ACCDDA50 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          103        133         16               95
TEMP_ID                        56C47CF8E9AABA4B9D0BF960ACCDDA50 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT         102        129         14               95

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        2F8F5674C7773FDE018411572BD31242 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          101        131         30               95
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE          100        126         13               99
TEMP_ID                        56C47CF8E9AABA4B9D0BF960ACCDDA50 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         99        126          5               95
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           98        125         53               95
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           97        125         36               96
PNOMBRE                        2F8F5674C7773FDE018411572BD31242 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         96        125         25               95
GETIDORDEN                     9FEA4ADC0C58FD77F24EDF36BBE1981D FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          95        125         14                1
TEMP_ID                        9D0293D8735D9D6621590B96749501D7 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           94        122         16               86
TEMP_ID                        9D0293D8735D9D6621590B96749501D7 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT          93        119         14               86
PNOMBRE                        AA52263AA942CD0D57317F0C83285D28 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           92        121         31               86
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           91        116         13               90

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           89        115         53               86
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           88        115         36               87
PNOMBRE                        AA52263AA942CD0D57317F0C83285D28 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         87        115         25               86
GETIDCLASE                     5F20EF9E028383620DF01901D748AC26 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          86        115         14                1
COD_ESPECIE                    E4F83839A0910A740CED68BF6F02EA75 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           84        107         16               69
COD_ESPECIE                    E4F83839A0910A740CED68BF6F02EA75 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           83        105         17               69
PESPECIE                       9915E70B072E6A7EFBB38EDD32922F07 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           82        105         35               69
PEXTINCION                     26C4C2004A3C932516ABB02B09C25142 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           81        105         45               69
PID_GENERO                     3DDE20C40CF36259C96990EEFB4BE481 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           80        105         56               69
COD_ESPECIE                    E4F83839A0910A740CED68BF6F02EA75 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT          79        101         14               69
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           78         98         17               77

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
COD_ESPECIE                    E4F83839A0910A740CED68BF6F02EA75 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         77         98          5               69
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           76         97         58               69
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           75         97         43               74
PID_GENERO                     3DDE20C40CF36259C96990EEFB4BE481 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         74         97         29               69
CHAR                           827DC51E93B66D1B683E1965CB2103FD SUBTYPE            PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           73         96         66               72
PEXTINCION                     26C4C2004A3C932516ABB02B09C25142 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         72         96         52               69
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           71         96         42               70
PESPECIE                       9915E70B072E6A7EFBB38EDD32922F07 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         70         96         30               69
INSERTARESPECIE                3D2865A48C78BE4FA50236DB5C005381 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          69         96         14                1
COD_GENERO                     B247B9034F60F95B2F8504BEC886D215 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           67         88         16               55
COD_GENERO                     B247B9034F60F95B2F8504BEC886D215 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           66         86         17               55

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PGENERO                        D0D7E686F5AA1CF04A6D5B9BAC743180 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           65         86         34               55
PID_FAMILIA                    50841C60EB8A6838D7F41EE87C148C84 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           64         86         43               55
COD_GENERO                     B247B9034F60F95B2F8504BEC886D215 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT          63         82         14               55
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           62         79         16               61
COD_GENERO                     B247B9034F60F95B2F8504BEC886D215 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         61         79          5               55
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           60         78         12               55
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           59         77         65               58
PID_FAMILIA                    50841C60EB8A6838D7F41EE87C148C84 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         58         77         50               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           57         77         40               56
PGENERO                        D0D7E686F5AA1CF04A6D5B9BAC743180 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         56         77         29               55
INSERTARGENERO                 B30B9B73DCE9D4B4149B81C63C5C7A32 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          55         77         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
COD_FAMILIA                    3277362618146A54ED496FE164BD0CCA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           53         69         16               41
COD_FAMILIA                    3277362618146A54ED496FE164BD0CCA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           52         67         17               41
PFAMILIA                       1D195FB172A47C4FBE398466D63A6126 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           51         67         35               41
PID_SUBORDEN                   0BB1345E68035758B64D88B1376098D0 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           50         67         45               41
COD_FAMILIA                    3277362618146A54ED496FE164BD0CCA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT          49         63         14               41
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           48         60         17               47
COD_FAMILIA                    3277362618146A54ED496FE164BD0CCA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         47         60          5               41
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           46         59         12               41
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           45         58         68               44
PID_SUBORDEN                   0BB1345E68035758B64D88B1376098D0 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         44         58         52               41
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           43         58         42               42

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PFAMILIA                       1D195FB172A47C4FBE398466D63A6126 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         42         58         30               41
INSERTARFAMILIA                2387AA98DD588022B23F1F7EE1121AC5 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          41         58         14                1
COD_SUBORDEN                   420926516874BF9347A792EF28F682BB VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           39         50         16               27
COD_SUBORDEN                   420926516874BF9347A792EF28F682BB VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           38         48         17               27
PSUBORDEN                      DED3E2C5F9EF7C69C5958F48200FF732 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           37         48         36               27
PID_ORDEN                      91C85DC879CBD6130AC10DC545AC20FF FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           36         48         47               27
COD_SUBORDEN                   420926516874BF9347A792EF28F682BB VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT          35         44         14               27
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           34         41         18               33
COD_SUBORDEN                   420926516874BF9347A792EF28F682BB VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         33         41          5               27
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           32         40         12               27
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           31         39         68               30

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_ORDEN                      91C85DC879CBD6130AC10DC545AC20FF FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         30         39         55               27
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           29         39         45               28
PSUBORDEN                      DED3E2C5F9EF7C69C5958F48200FF732 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         28         39         32               27
INSERTARSUB_ORDEN              FAA5847565C45351B90110514C577A92 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          27         39         14                1
COD_ORDEN                      2AE7348E27EF2ED3B795DAC452C4EA18 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           25         31         16               13
COD_ORDEN                      2AE7348E27EF2ED3B795DAC452C4EA18 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           24         29         17               13
PORDEN                         47629F0D1C4C7F186916F4C23B879E8A FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           23         29         33               13
PID_CLASE                      89186F5BD5DD4D56D805AC8F8D9C07AF FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           22         29         41               13
COD_ORDEN                      2AE7348E27EF2ED3B795DAC452C4EA18 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT          21         25         14               13
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           20         22         15               19
COD_ORDEN                      2AE7348E27EF2ED3B795DAC452C4EA18 VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         19         22          5               13

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           18         21         12               13
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           17         20         61               16
PID_CLASE                      89186F5BD5DD4D56D805AC8F8D9C07AF FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         16         20         48               13
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           15         20         38               14
PORDEN                         47629F0D1C4C7F186916F4C23B879E8A FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION         14         20         28               13
INSERTARORDEN                  30F0FBC90C31DB1B1163EB0FF604F3ED FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION          13         20         14                1
COD_CLASE                      D83AD526D19310F0694E054DE9197DEA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           11         12         16                2
COD_CLASE                      D83AD526D19310F0694E054DE9197DEA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  REFERENCE           10         10         17                2
PNOMBRE                        76AC13A77061161AAD03A9FBB1E974B6 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  REFERENCE            9         10         33                2
COD_CLASE                      D83AD526D19310F0694E054DE9197DEA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  ASSIGNMENT           8          6         14                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE            7          3         15                6

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
COD_CLASE                      D83AD526D19310F0694E054DE9197DEA VARIABLE           PAQ_ANIMALES                   PACKAGE BODY  DECLARATION          6          3          5                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ANIMALES                   PACKAGE BODY  REFERENCE            5          2         56                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ANIMALES                   PACKAGE BODY  REFERENCE            4          2         39                3
PNOMBRE                        76AC13A77061161AAD03A9FBB1E974B6 FORMAL IN          PAQ_ANIMALES                   PACKAGE BODY  DECLARATION          3          2         28                2
INSERTARCLASE                  3F49EF15FA4311ED254D7D48F9DB76D2 FUNCTION           PAQ_ANIMALES                   PACKAGE BODY  DEFINITION           2          2         14                1
PAQ_ANIMALES                   CA26D34F4D8C84D90D37E711DAEFD335 PACKAGE            PAQ_ANIMALES                   PACKAGE BODY  DEFINITION           1          1         14                0
GENERO                         C6CCA18E0B3E9F76A3AD6D99927FF3C0 VARIABLE           T_CONSULTA                     TYPE          DECLARATION         10          3         24                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTA                     TYPE          REFERENCE            9          3          9                8
FAMILIA                        9B2D3279787A6156DBDC9F40147676B0 VARIABLE           T_CONSULTA                     TYPE          DECLARATION          8          3          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTA                     TYPE          REFERENCE            7          2         52                6
SUB_ORDEN                      71797EEBB25AFD633CC816FD03F32A91 VARIABLE           T_CONSULTA                     TYPE          DECLARATION          6          2         42                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTA                     TYPE          REFERENCE            5          2         27                4
ORDEN                          80E848CD69E4CFFDC664D155FFD54140 VARIABLE           T_CONSULTA                     TYPE          DECLARATION          4          2         21                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTA                     TYPE          REFERENCE            3          2          7                2
CLASE                          CE2419E7D2FFA6558A4831C26A9B7E1B VARIABLE           T_CONSULTA                     TYPE          DECLARATION          2          2          1                1
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             T_CONSULTA                     TYPE          DECLARATION          1          1          6                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTCLASE              TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTCLASE              TRIGGER       CALL                 3          6         26                2
BEFOREINSERTCLASE              29A6A4F9343725ECD88F0C8E8BB3EA54 TRIGGER            BEFOREINSERTCLASE              TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTCLASE              29A6A4F9343725ECD88F0C8E8BB3EA54 TRIGGER            BEFOREINSERTCLASE              TRIGGER       DECLARATION          1          1         23                0
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE       REFERENCE            5          2         49                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE DBCRYPT                        PACKAGE       REFERENCE            4          2         31                3

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
P_DATA                         5D2076E027D6B8456B96D2A58A125A23 FORMAL IN          DBCRYPT                        PACKAGE       DECLARATION          3          2         21                2
ENCRYPT                        61ECD8FF3EA8962888A0BBB5DE4D7C50 FUNCTION           DBCRYPT                        PACKAGE       DECLARATION          2          2         12                1
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            DBCRYPT                        PACKAGE       DECLARATION          1          1          9                0
INTEGER                        4EEE7F402CC9B7D56CC6D637688184D6 SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           22         10         21               21
ENTERO                         49DBD1A2D16D126AE0223D1D88C5AC21 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         21         10          7                5
P_DATA                         BC1A796234C2C5C75C6AA6F4D0670FFC FORMAL IN          DBCRYPT                        PACKAGE BODY  REFERENCE           20          9         38               19
LENGTH                         6AA7292AD9C470D3719AD4B4F8CBE0AD FUNCTION           DBCRYPT                        PACKAGE BODY  CALL                19          9         31               18
LARGO                          7BF6FC60430CCA55A14B695292BCCB68 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          18          9          7               16
INTEGER                        4EEE7F402CC9B7D56CC6D637688184D6 SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           17          9         20               16
LARGO                          7BF6FC60430CCA55A14B695292BCCB68 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         16          9          7                5
ENCRYPT_KEY                    9E5281FAB4A51C7AC8412A4F3A222037 VARIABLE           DBCRYPT                        PACKAGE BODY  REFERENCE           15          8         45               14

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RAW_KEY                        0A65A6DFC22E27A2513CB94C5E35D967 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT          13          8          7               11
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           12          8         15               11
RAW_KEY                        0A65A6DFC22E27A2513CB94C5E35D967 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION         11          8          7                5
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE           10          7         26                9
PASSWORD_ENCRYPTED             0FA615503DAA2B15AD4E854909438537 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION          9          7          7                5
RAW                            95A1982C9F8DA81D97D2EEB941A4FB4F SUBTYPE            DBCRYPT                        PACKAGE BODY  REFERENCE            8          5         51                5
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE DBCRYPT                        PACKAGE BODY  REFERENCE            7          5         33                6
P_DATA                         BC1A796234C2C5C75C6AA6F4D0670FFC FORMAL IN          DBCRYPT                        PACKAGE BODY  DECLARATION          6          5         23                5
ENCRYPT                        61ECD8FF3EA8962888A0BBB5DE4D7C50 FUNCTION           DBCRYPT                        PACKAGE BODY  DEFINITION           5          5         14                1
ENCRYPT_KEY                    9E5281FAB4A51C7AC8412A4F3A222037 VARIABLE           DBCRYPT                        PACKAGE BODY  ASSIGNMENT           4          3          5                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE DBCRYPT                        PACKAGE BODY  REFERENCE            3          3         17                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
ENCRYPT_KEY                    9E5281FAB4A51C7AC8412A4F3A222037 VARIABLE           DBCRYPT                        PACKAGE BODY  DECLARATION          2          3          5                1
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            DBCRYPT                        PACKAGE BODY  DEFINITION           1          1         14                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CUMPLEANNOS                  TYPE          REFERENCE           11          6         11               10
FECHA                          0BE4608EFC2860A08B3043E1EBC24C97 VARIABLE           T_CUMPLEANNOS                  TYPE          DECLARATION         10          6          5                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CUMPLEANNOS                  TYPE          REFERENCE            9          5         13                8
USUARIO                        317F30C0B97BE63550BD86E2003B4B44 VARIABLE           T_CUMPLEANNOS                  TYPE          DECLARATION          8          5          5                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CUMPLEANNOS                  TYPE          REFERENCE            7          4         22                6
SEGUNDO_APELLIDO               EE4E63AAA9EC6CEA7ED811651AAD1C28 VARIABLE           T_CUMPLEANNOS                  TYPE          DECLARATION          6          4          5                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CUMPLEANNOS                  TYPE          REFERENCE            5          3         21                4
PRIMER_APELLIDO                12728C6598EEB753391ED718A4578686 VARIABLE           T_CUMPLEANNOS                  TYPE          DECLARATION          4          3          5                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CUMPLEANNOS                  TYPE          REFERENCE            3          2         12                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NOMBRE                         F867B03D99211B4DF2973A7F3077C606 VARIABLE           T_CUMPLEANNOS                  TYPE          DECLARATION          2          2          5                1
T_CUMPLEANNOS                  BBB285D1C11DD2886F84E91876105684 OBJECT             T_CUMPLEANNOS                  TYPE          DECLARATION          1          1          6                0
T_CUMPLEANNOS                  BBB285D1C11DD2886F84E91876105684 OBJECT             RECORD_CUMPLEANNOS             TYPE          REFERENCE            2          1         37                1
RECORD_CUMPLEANNOS             25B7C654C7A65EAF4F80BE715E28F13B NESTED TABLE       RECORD_CUMPLEANNOS             TYPE          DECLARATION          1          1          6                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE           23          5         18               22
ZONAAVISTAMIENTO               27905FAFBCF56B677F7B7FB7E5ECF539 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         22          5          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE           21          4         55               20
DISTRITO                       32D4011563604D0FEE29770EE47CE0E2 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         20          4         46                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE           19          4         32               18
CANTON                         01885E7488702BB989499E02B4637AAA VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         18          4         25                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE           17          4         11               16

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PROVINCIA                      74F1964734EAE681F528BDCC057BF193 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         16          4          1                1
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            T_AVISTAMIENTO                 TYPE          REFERENCE           15          3         73               14
PAIS                           0602E0743F3156DBE007F10927BBC717 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         14          3         68                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE           13          3         54               12
ESPECIE                        707F93A3DE2041A4DFD5AD5696AF6762 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         12          3         46                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE           11          3         31               10
GENERO                         B528C1C9C69E7D73CF746A4E4190052A VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION         10          3         24                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE            9          3          9                8
FAMILIA                        954512E5138AFEBE75803189B1F2C2D0 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION          8          3          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE            7          2         52                6
SUB_ORDEN                      5B80E0229128EC4C7A663B1FB4F79CEA VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION          6          2         42                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE            5          2         27                4
ORDEN                          AF3B7BB982947FF4D3ACDE1C35D7AC6E VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION          4          2         21                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_AVISTAMIENTO                 TYPE          REFERENCE            3          2          7                2
CLASE                          1AD78B7FB863C4C32FC404A8AEE72210 VARIABLE           T_AVISTAMIENTO                 TYPE          DECLARATION          2          2          1                1
T_AVISTAMIENTO                 FEC2B08FAD2A99EC12992045A04EF0CF OBJECT             T_AVISTAMIENTO                 TYPE          DECLARATION          1          1          6                0
T_AVISTAMIENTO                 FEC2B08FAD2A99EC12992045A04EF0CF OBJECT             RECORD_AVISTAMIENTO            TYPE          REFERENCE            2          1         38                1
RECORD_AVISTAMIENTO            EFA462166569ADCA9A6367E4C3CB12B7 NESTED TABLE       RECORD_AVISTAMIENTO            TYPE          DECLARATION          1          1          6                0
UPDATEPAIS                     10B1B8775045D42108F8BFED13F877AF FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         50         15         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           49         13         64               46
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           48         13         47               47
PNOMBRE                        87C0487D9D5F21EB021963BE32F61BF8 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         47         13         36               46

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GETIDZONAAVISTAMIENTO          3E666F3EC24415794E59274AFC269502 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         46         13         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           45         12         56               42
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           44         12         39               43
PNOMBRE                        B9C7F262942E9D3AAC5C7C4DE5D57AC7 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         43         12         28               42
GETIDDISTRITO                  8D6FD8E6F4CF19DFCC3A7BEBAF8403F2 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         42         12         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           41         11         54               38
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           40         11         37               39
PNOMBRE                        365C2A71E7D18BAA98E80FC77F79C917 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         39         11         26               38
GETIDCANTON                    22B5D2A1A742EA2903BC3653FDBBC8C4 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         38         11         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           37         10         57               34
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           36         10         40               35

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        65C8EA58C1CA7C6F1B8D27899021A8C9 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         35         10         29               34
GETIDPROVINCIA                 34DA728421157AF89915CF6B52059B52 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         34         10         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           33          9         52               30
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           32          9         35               31
PNOMBRE                        1F6128F700AE14630EB4D16B6D163E9E FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         31          9         24               30
GETIDPAIS                      B6F3E400DE38FC70B2FC064685DBC45B FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         30          9         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           29          7         28               24
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           28          6         81               27
PID_DISTRITO                   A7F80DF7AF25DFCB6781787E4A840F40 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         27          6         65               24
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           26          6         55               25
PNOMBRELUGAR                   B6DD6AB9AC89840C932DEC90967CDD1F FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         25          6         39               24

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
INSERTARZONAAVISTAMIENTO       23882A16C52290C2D8D639BA0976D308 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         24          6         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           23          5         81               18
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           22          5         66               21
PID_CANTON                     BA46C2A2DD9D1C990DF419A53F9D4E60 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         21          5         52               18
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           20          5         42               19
PNOMBRE                        7D465A0F4472787C295848F60088DBDD FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         19          5         31               18
INSERTARDISTRITO               EDC3254D612B8D541E6A82CA647F5466 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         18          5         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           17          4         81               12
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           16          4         66               15
PID_PROVINCIA                  7F13CF903C1E4A0D9D95484FE9CBE2CE FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         15          4         49               12
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE           14          4         40               13

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PNOMBRE                        53C98D5248B7E3C8322AF8F150A596BD FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION         13          4         29               12
INSERTARCANTON                 44B2261E70836FCA168EA46852A05541 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION         12          4         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           11          3         80                6
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE           10          3         65                9
PID_PAIS                       CA99979224E85F2667A8EDCAAA8E777F FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION          9          3         53                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE            8          3         43                7
PNOMBRE                        710464206D6EF1B7F113891784912006 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION          7          3         32                6
INSERTARPROVINCIA              A4C8849C4320EA59694762AFE862FAA7 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION          6          3         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE       REFERENCE            5          2         55                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE       REFERENCE            4          2         38                3
PNOMBRE                        7D6F477AE249D1A81DB9898AE415E5B6 FORMAL IN          PAQ_LOCACION                   PACKAGE       DECLARATION          3          2         27                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
INSERTARPAIS                   9B7D0F8AF2A199B373E820F76129A387 FUNCTION           PAQ_LOCACION                   PACKAGE       DECLARATION          2          2         14                1
PAQ_LOCACION                   B9D78BFA74F8E013D4502F10E9801058 PACKAGE            PAQ_LOCACION                   PACKAGE       DECLARATION          1          1          9                0
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           92        117         13               91
TEMP_ID                        14E05B43A592C239485AE0D257D27036 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         91        117          5               87
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           90        116         54               87
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           89        116         37               88
PNOMBRE                        751EEE103F3066306FFB249F4376CE4D FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         88        116         26               87
GETIDCANTON                    22B5D2A1A742EA2903BC3653FDBBC8C4 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          87        116         14                1
TEMP_ID                        80260509B8A7595641FC47ED1DE2E1A7 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           86        113         16               78
TEMP_ID                        80260509B8A7595641FC47ED1DE2E1A7 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          85        109         14               78
PNOMBRE                        5EE5D38D643B8D336572CB66169D811D FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           84        111         30               78

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           83        106         13               82
TEMP_ID                        80260509B8A7595641FC47ED1DE2E1A7 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         82        106          5               78
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           81        105         57               78
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           80        105         40               79
PNOMBRE                        5EE5D38D643B8D336572CB66169D811D FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         79        105         29               78
GETIDPROVINCIA                 34DA728421157AF89915CF6B52059B52 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          78        105         14                1
TEMP_ID                        BF469F005D28173CAF30B6D4C85EA11A VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           77        102         16               69
TEMP_ID                        BF469F005D28173CAF30B6D4C85EA11A VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          76         98         14               69
PNOMBRE                        491296550AC5B74AE87047E580FC43A6 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           75        100         30               69
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           74         95         13               73
TEMP_ID                        BF469F005D28173CAF30B6D4C85EA11A VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         73         95          5               69

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           72         94         52               69
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           71         94         35               70
PNOMBRE                        491296550AC5B74AE87047E580FC43A6 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         70         94         24               69
GETIDPAIS                      B6F3E400DE38FC70B2FC064685DBC45B FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          69         94         14                1
COD_ZONAAVISTAMIENTO           4D75BD100F13914186E641A647E25278 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           67         86         16               55
COD_ZONAAVISTAMIENTO           4D75BD100F13914186E641A647E25278 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           66         84         16               55
PNOMBRELUGAR                   C37866E15BF158FB5105564528B4FA1A FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           65         84         43               55
PID_DISTRITO                   BF879E0ABC3DE235889EDCFFFDFA9682 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           64         84         57               55
COD_ZONAAVISTAMIENTO           4D75BD100F13914186E641A647E25278 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          63         80         14               55
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           62         77         26               61
COD_ZONAAVISTAMIENTO           4D75BD100F13914186E641A647E25278 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         61         77          5               55

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           60         76         12               55
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           59         75         81               58
PID_DISTRITO                   BF879E0ABC3DE235889EDCFFFDFA9682 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         58         75         65               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           57         75         55               56
PNOMBRELUGAR                   C37866E15BF158FB5105564528B4FA1A FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         56         75         39               55
INSERTARZONAAVISTAMIENTO       23882A16C52290C2D8D639BA0976D308 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          55         75         14                1
COD_DISTRITO                   56AD14E3EC5BBFCC9FB8B275DD5B9179 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           53         67         16               41
COD_DISTRITO                   56AD14E3EC5BBFCC9FB8B275DD5B9179 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           52         65         16               41
PNOMBRE                        B9AAFCDAF386A6B02E5CEC4E86CE4A56 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           51         65         35               41
PID_CANTON                     670E1D726C869CD76E94765D15DA2C65 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           50         65         44               41
COD_DISTRITO                   56AD14E3EC5BBFCC9FB8B275DD5B9179 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          49         61         14               41

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           48         58         18               47
COD_DISTRITO                   56AD14E3EC5BBFCC9FB8B275DD5B9179 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         47         58          5               41
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           46         57         12               41
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           45         56         67               44
PID_CANTON                     670E1D726C869CD76E94765D15DA2C65 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         44         56         53               41
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           43         56         43               42
PNOMBRE                        B9AAFCDAF386A6B02E5CEC4E86CE4A56 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         42         56         31               41
INSERTARDISTRITO               EDC3254D612B8D541E6A82CA647F5466 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          41         56         14                1
COD_CANTON                     F43D21ADCC3E0B67F5E256F4D5A7AF65 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           39         49         16               27
COD_CANTON                     F43D21ADCC3E0B67F5E256F4D5A7AF65 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           38         47         16               27
PNOMBRE                        618B116DA847D30EC5436B915F674209 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           37         47         33               27

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_PROVINCIA                  30B8A82F5328A23A1DA1D7F2FD27ACC3 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           36         47         42               27
COD_CANTON                     F43D21ADCC3E0B67F5E256F4D5A7AF65 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          35         43         14               27
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           34         40         16               33
COD_CANTON                     F43D21ADCC3E0B67F5E256F4D5A7AF65 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         33         40          5               27
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           32         39         12               27
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           31         38         67               30
PID_PROVINCIA                  30B8A82F5328A23A1DA1D7F2FD27ACC3 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         30         38         50               27
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           29         38         40               28
PNOMBRE                        618B116DA847D30EC5436B915F674209 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         28         38         29               27
INSERTARCANTON                 44B2261E70836FCA168EA46852A05541 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          27         38         14                1
COD_PROVINCIA                  F5EAF05F7E203020C17EC337CA61E25E VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           25         31         16               13

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
COD_PROVINCIA                  F5EAF05F7E203020C17EC337CA61E25E VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           24         29         16               13
PNOMBRE                        F824B567A59969F34769627B6A614352 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           23         29         36               13
PID_PAIS                       73CA4E729F0B2576B1D87B09FE2F84A6 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE           22         29         45               13
COD_PROVINCIA                  F5EAF05F7E203020C17EC337CA61E25E VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT          21         25         14               13
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           20         22         19               19
COD_PROVINCIA                  F5EAF05F7E203020C17EC337CA61E25E VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION         19         22          5               13
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           18         21         12               13
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE           17         20         65               16
PID_PAIS                       73CA4E729F0B2576B1D87B09FE2F84A6 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         16         20         53               13
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE           15         20         43               14
PNOMBRE                        F824B567A59969F34769627B6A614352 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION         14         20         32               13

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
INSERTARPROVINCIA              A4C8849C4320EA59694762AFE862FAA7 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION          13         20         14                1
COD_PAIS                       A855964FA2DF57563B2582E00B03AEC3 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           11         13         16                2
COD_PAIS                       A855964FA2DF57563B2582E00B03AEC3 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  REFERENCE           10         11         16                2
PNOMBRE                        77D027A3886F96F713BAFE3D781C4F13 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  REFERENCE            9         11         31                2
COD_PAIS                       A855964FA2DF57563B2582E00B03AEC3 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  ASSIGNMENT           8          7         14                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE            7          4         14                6
COD_PAIS                       A855964FA2DF57563B2582E00B03AEC3 VARIABLE           PAQ_LOCACION                   PACKAGE BODY  DECLARATION          6          4          5                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_LOCACION                   PACKAGE BODY  REFERENCE            5          3         12                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_LOCACION                   PACKAGE BODY  REFERENCE            4          2         38                3
PNOMBRE                        77D027A3886F96F713BAFE3D781C4F13 FORMAL IN          PAQ_LOCACION                   PACKAGE BODY  DECLARATION          3          2         27                2
INSERTARPAIS                   9B7D0F8AF2A199B373E820F76129A387 FUNCTION           PAQ_LOCACION                   PACKAGE BODY  DEFINITION           2          2         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PAQ_LOCACION                   B9D78BFA74F8E013D4502F10E9801058 PACKAGE            PAQ_LOCACION                   PACKAGE BODY  DEFINITION           1          1         14                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE           11          3         31               10
GENERO                         8193471416FA0AC53A65039C00481708 VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION         10          3         24                1
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             RECORD_CONSULTA                TYPE          REFERENCE            2          1         34                1
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       RECORD_CONSULTA                TYPE          DECLARATION          1          1          6                0
T_CONSULTACOLORTAMANO          415968482F8C50C3C7F657CE2C0AEDDA OBJECT             RECORD_CONSULTACOLORTAMANO     TYPE          REFERENCE            2          1         45                1
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       RECORD_CONSULTACOLORTAMANO     TYPE          DECLARATION          1          1          6                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          109        104         29              108
TEMP_CONTRA_DESPUES            7112CE1B0797B981B8F26D61EC95DBEE VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        108        104          9              102
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          107        103         27              106
TEMP_CONTRA_ANTES              E6CACD8330F47217CFBE240DE71411E5 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        106        103          9              102

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE          105        102         63              102
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          104        102         46              103
PCONTRASENA                    FCEDFCCAE4AE57C45CB06BA7D5DA3574 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        103        102         31              102
UPDATECONTRASENA               EDA7C7B00F88CDF92A360E1E37DD9D62 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         102        102         14                1
TEMP_CONTRASENA                60021936F009357F9D32FA7287979733 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          101         99         20               91
DECRYPT                        919F064CD87E01FD4470CC244F8B7A01 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               100         95         28               99
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE           99         95         20               98
TEMP_CONTRASENA                60021936F009357F9D32FA7287979733 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          98         96         18               91
PUSUARIO                       DBB975105E8B51F5D1DC6977099FEFC0 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           97         98         29               91
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           96         93         25               95
TEMP_CONTRASENA                60021936F009357F9D32FA7287979733 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION         95         93          9               91

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           94         92         57               91
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           93         92         40               92
PUSUARIO                       DBB975105E8B51F5D1DC6977099FEFC0 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         92         92         28               91
GETCONTRASENA                  26F7EA199E06D1132251A99DE8DF2D17 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION          91         92         14                1
CONTADOR                       DC22527A2FEB0FEE641641CFF662C3AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           90         89         21               82
CONTADOR                       DC22527A2FEB0FEE641641CFF662C3AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          89         85         20               82
PUSUARIO                       A5C2C8D57800EC430B07067E02CC4063 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           88         87         30               82
INTEGER                        4EEE7F402CC9B7D56CC6D637688184D6 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE           87         82         18               86
CONTADOR                       DC22527A2FEB0FEE641641CFF662C3AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION         86         82          9               82
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE           85         81         57               82
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           84         81         40               83

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PUSUARIO                       A5C2C8D57800EC430B07067E02CC4063 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         83         81         28               82
VALIDAUSUARIO                  E0921AAB219A9939A6E172F360C4000D FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION          82         81         14                1
USUARIO_CONECTADO              C58AEEC553D0DE5A54B9F63CD591AAA6 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           81         78         20               79
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           80         76         38               79
GETUSUARIOACTUAL               23407D986441AB14C7033A9AE4BCCFD7 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION          79         76         14                1
PUSUARIO                       7EFA523EA66BEBA1501BC8F8FA95895A FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           78         72         34               77
USUARIO_CONECTADO              C58AEEC553D0DE5A54B9F63CD591AAA6 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          77         72         13               74
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           76         70         44               75
PUSUARIO                       7EFA523EA66BEBA1501BC8F8FA95895A FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         75         70         32               74
SETUSUARIOACTUAL               6E0268A2743664A38A14ECC8911D6468 PROCEDURE          PAQ_PERSONA                    PACKAGE BODY  DEFINITION          74         70         15                1
COD_CORREO                     6D592DF7D6DA201E288153860F4B5B1C VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           73         66         16               63

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDIRECCION                     E870609205589B388BC28DC43D6CE8B4 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           72         66         34               63
PID_PERSONA                    6C64CDEB572836423916055FB2FC0D9C FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           71         66         47               63
COD_CORREO                     6D592DF7D6DA201E288153860F4B5B1C VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          70         62         14               63
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE           69         59         20               68
COD_CORREO                     6D592DF7D6DA201E288153860F4B5B1C VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION         68         59          9               63
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE           67         57         67               66
PID_PERSONA                    6C64CDEB572836423916055FB2FC0D9C FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         66         57         52               63
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           65         57         42               64
PDIRECCION                     E870609205589B388BC28DC43D6CE8B4 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         64         57         28               63
AGREGACORREO                   0D8AE40370F7C36DF16002844C0B4481 PROCEDURE          PAQ_PERSONA                    PACKAGE BODY  DEFINITION          63         57         15                1
PCONTRASENA                    52D7D685AF0939219B4F059228B1C978 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           61         52         41               60

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
ENCRYPT                        61ECD8FF3EA8962888A0BBB5DE4D7C50 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL                60         52         33               59
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE           59         52         25               47
PUSUARIO                       AD45CE06E53EBD0082A8B0F1A5612FCB FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           58         52         16               47
PID_PERSONA                    96E57F7BCAE20006D019C44B0806FAC4 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           57         52         54               47
PUSUARIO                       AD45CE06E53EBD0082A8B0F1A5612FCB FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           56         50         38               55
SETUSUARIOACTUAL               6E0268A2743664A38A14ECC8911D6468 PROCEDURE          PAQ_PERSONA                    PACKAGE BODY  CALL                55         50         21               54
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE           54         50          9               47
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE           53         46         84               52
PID_PERSONA                    96E57F7BCAE20006D019C44B0806FAC4 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         52         46         72               47
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           51         46         62               50
PCONTRASENA                    52D7D685AF0939219B4F059228B1C978 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         50         46         50               47

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           49         46         41               48
PUSUARIO                       AD45CE06E53EBD0082A8B0F1A5612FCB FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         48         46         32               47
AGREGACREDENCIAL               0AD5F2316263D734757101A5B8164EB2 PROCEDURE          PAQ_PERSONA                    PACKAGE BODY  DEFINITION          47         46         15                1
CONTADOR                       314F7CD6055BE2687239461D74172C04 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           46         43         17               37
CONTADOR                       314F7CD6055BE2687239461D74172C04 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          45         38         16               37
ROWNUM                         65C4C98CB2DD1DA9D438AAEFCCC8BD58 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL                44         41         13               37
PDIRECCION                     50E8023B0C5B51A54FAB24F2DAC86053 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           43         40         34               37
INTEGER                        4EEE7F402CC9B7D56CC6D637688184D6 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE           42         35         18               41
CONTADOR                       314F7CD6055BE2687239461D74172C04 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION         41         35          9               37
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE           40         34         12               37
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           39         33         41               38

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDIRECCION                     50E8023B0C5B51A54FAB24F2DAC86053 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         38         33         27               37
VALIDACORREO                   49CE30E62EB7DCD65BD4FD7A0EB84404 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION          37         33         14                1
CONTADOR                       356BDE0DC81913C208D3EBBB1B278412 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           36         30         17               27
CONTADOR                       356BDE0DC81913C208D3EBBB1B278412 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          35         25         16               27
ROWNUM                         65C4C98CB2DD1DA9D438AAEFCCC8BD58 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL                34         28         13               27
PUSUARIO                       7E995C94B953DFEB6D901AD2FD96CE28 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           33         27         26               27
INTEGER                        4EEE7F402CC9B7D56CC6D637688184D6 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE           32         22         18               31
CONTADOR                       356BDE0DC81913C208D3EBBB1B278412 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION         31         22          9               27
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE           30         21         12               27
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           29         20         43               28
PUSUARIO                       7E995C94B953DFEB6D901AD2FD96CE28 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         28         20         31               27

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VALIDACREDENCIAL               B404BCF99CCB9B38D530DAC2CFDC08BB FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION          27         20         14                1
COD_PERSONA                    C3FA5C798610E1A84DFF01F0D1A6C1AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           26         17         17                2
COD_PERSONA                    C3FA5C798610E1A84DFF01F0D1A6C1AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE           25         14         16                2
NOMBRE_IN                      968225C71D25478C90415E601BCA6DC0 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           24         14         34                2
PRIMERAPELLIDO_IN              02C8B32FD02AC66D02E039AE486535DF FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           23         14         51                2
SEGUNDOAPELLIDO_IN             761A588D0DCA9459449669CF0A56E822 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           22         15         11                2
FECHA_NACIMIENTO_IN            991151B70F7076E7CF7C16A4F9A135CB FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           21         15         39                2
PROFESION_IN                   BF506F2C4100DA02AA5154C1B3B238FA FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           20         15         80                2
ID_ROL_IN                      43FEB34898B4232B098E070CBCD17B36 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE           19         15         94                2
COD_PERSONA                    C3FA5C798610E1A84DFF01F0D1A6C1AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT          18         10         14                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE           17          7         17               16

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
COD_PERSONA                    C3FA5C798610E1A84DFF01F0D1A6C1AA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION         16          7          5                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE           15          6         12                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE           14          5         61               13
ID_ROL_IN                      43FEB34898B4232B098E070CBCD17B36 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         13          5         51                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           12          5         42               11
PROFESION_IN                   BF506F2C4100DA02AA5154C1B3B238FA FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION         11          5         29                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE           10          4         78                9
FECHA_NACIMIENTO_IN            991151B70F7076E7CF7C16A4F9A135CB FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION          9          4         58                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE            8          4         48                7
SEGUNDOAPELLIDO_IN             761A588D0DCA9459449669CF0A56E822 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION          7          4         29                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE            6          3         66                5

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PRIMERAPELLIDO_IN              02C8B32FD02AC66D02E039AE486535DF FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION          5          3         48                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE            4          3         39                3
NOMBRE_IN                      968225C71D25478C90415E601BCA6DC0 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION          3          3         29                2
AGREGARPERSONA                 7DA8B75D9B6D9E69DD7864F31EE0DE77 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION           2          3         14                1
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  DEFINITION           1          1         14                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTCANTON             TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTCANTON             TRIGGER       CALL                 3          6         26                2
BEFOREINSERTCANTON             B45C22D9C6F5ED4B170A0434D44D81A0 TRIGGER            BEFOREINSERTCANTON             TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTCANTON             B45C22D9C6F5ED4B170A0434D44D81A0 TRIGGER            BEFOREINSERTCANTON             TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATECANTON             TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATECANTON             TRIGGER       CALL                 3          6         37                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREUPDATECANTON             ACA82BBD39AF7D484F9B11565E661757 TRIGGER            BEFOREUPDATECANTON             TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATECANTON             ACA82BBD39AF7D484F9B11565E661757 TRIGGER            BEFOREUPDATECANTON             TRIGGER       DECLARATION          1          1         23                0
TEMP_PROFESION                 488FA0B5741D1A826A8FE4D3782A746D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          313        309         16              302
TEMP_PROFESION                 488FA0B5741D1A826A8FE4D3782A746D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         312        305         14              302
TEMP_ID                        E5DF37093F33624CFB862006D04CB43B VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          311        307         28              302
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          310        302         20              309
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATECLASE              TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATECLASE              TRIGGER       CALL                 3          6         37                2
BEFOREUPDATECLASE              0EF2E3484628BD9ACBF741145866DCB5 TRIGGER            BEFOREUPDATECLASE              TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATECLASE              0EF2E3484628BD9ACBF741145866DCB5 TRIGGER            BEFOREUPDATECLASE              TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTCOLOR              TRIGGER       CALL                 4          7         22                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTCOLOR              TRIGGER       CALL                 3          6         26                2
BEFOREINSERTCOLOR              6EF0EAEB63F4C14437DA792DBD9B4569 TRIGGER            BEFOREINSERTCOLOR              TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTCOLOR              6EF0EAEB63F4C14437DA792DBD9B4569 TRIGGER            BEFOREINSERTCOLOR              TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATECOLOR              TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATECOLOR              TRIGGER       CALL                 3          6         37                2
BEFOREUPDATECOLOR              9E9C9059E91421A6985E6B8E894B8DE4 TRIGGER            BEFOREUPDATECOLOR              TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATECOLOR              9E9C9059E91421A6985E6B8E894B8DE4 TRIGGER            BEFOREUPDATECOLOR              TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTCORREO             TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTCORREO             TRIGGER       CALL                 3          6         26                2
BEFOREINSERTCORREO             ED7FAE2E680830D4130EDA1594E565B9 TRIGGER            BEFOREINSERTCORREO             TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTCORREO             ED7FAE2E680830D4130EDA1594E565B9 TRIGGER            BEFOREINSERTCORREO             TRIGGER       DECLARATION          1          1         23                0

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATECORREO             TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATECORREO             TRIGGER       CALL                 3          6         37                2
BEFOREUPDATECORREO             745DEFAA156833052FEB256CDE33BAE8 TRIGGER            BEFOREUPDATECORREO             TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATECORREO             745DEFAA156833052FEB256CDE33BAE8 TRIGGER            BEFOREUPDATECORREO             TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTCREDENCIAL         TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTCREDENCIAL         TRIGGER       CALL                 3          6         26                2
BEFOREINSERTCREDENCIAL         F4CDBE6A8F3604CCB48E860E4929082D TRIGGER            BEFOREINSERTCREDENCIAL         TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTCREDENCIAL         F4CDBE6A8F3604CCB48E860E4929082D TRIGGER            BEFOREINSERTCREDENCIAL         TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATECREDENCIAL         TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATECREDENCIAL         TRIGGER       CALL                 3          6         37                2
BEFOREUPDATECREDENCIAL         25A721478D42D41F3D97165EB1543A47 TRIGGER            BEFOREUPDATECREDENCIAL         TRIGGER       DEFINITION           2          1         23                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREUPDATECREDENCIAL         25A721478D42D41F3D97165EB1543A47 TRIGGER            BEFOREUPDATECREDENCIAL         TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTDISTRITO           TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTDISTRITO           TRIGGER       CALL                 3          6         26                2
BEFOREINSERTDISTRITO           CB27ED15A23129AD2BC2FF73A2D4F898 TRIGGER            BEFOREINSERTDISTRITO           TRIGGER       DEFINITION           2          1         23                1
TEMP_PROFESION                 488FA0B5741D1A826A8FE4D3782A746D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        309        302          5              302
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               308        301         35              307
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          307        301         23              306
TEMP_ID                        E5DF37093F33624CFB862006D04CB43B VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         306        301          5              304
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          305        301         13              304
TEMP_ID                        E5DF37093F33624CFB862006D04CB43B VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        304        301          5              302
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          303        300         34              302

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GETPROFESION                   33CB6C7156E5C41698B898EEF63F1DB6 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         302        300         14                1
TEMP_FECHA_NACIMIENTO          EF9AFA44CD146658B22C0C85482F5F69 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          301        297         16              286
TEMP_FECHA_NACIMIENTO_DATE     47E001AA3DF4201031606DD20DF82CEA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          300        296         42              299
TEMP_FECHA_NACIMIENTO          EF9AFA44CD146658B22C0C85482F5F69 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         299        296          9              286
TEMP_FECHA_NACIMIENTO_DATE     47E001AA3DF4201031606DD20DF82CEA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         298        292         14              286
TEMP_ID                        435C8D69C05AFEAB3E0E4B89A3BF0E3F VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          297        294         28              286
DATE                           FA77BD0879D1705E554C13500728C1D6 DATE DATATYPE      PAQ_PERSONA                    PACKAGE BODY  REFERENCE          296        289         32              295
TEMP_FECHA_NACIMIENTO_DATE     47E001AA3DF4201031606DD20DF82CEA VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        295        289          5              286
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          294        288         27              293
TEMP_FECHA_NACIMIENTO          EF9AFA44CD146658B22C0C85482F5F69 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        293        288          5              286
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               292        287         35              291

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          291        287         23              290
TEMP_ID                        435C8D69C05AFEAB3E0E4B89A3BF0E3F VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         290        287          5              288
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          289        287         13              288
TEMP_ID                        435C8D69C05AFEAB3E0E4B89A3BF0E3F VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        288        287          5              286
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          287        286         40              286
GETFECHANACIMIENTO             39BC4AEDB85C6E425121CFDEC20104E5 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         286        286         14                1
TEMP_SEGUNDO_APELLIDO          521432D4CDBD05091DE80B64D9AE56C1 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          285        283         16              274
TEMP_SEGUNDO_APELLIDO          521432D4CDBD05091DE80B64D9AE56C1 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         284        279         14              274
TEMP_ID                        DA473453C08D2E5A21B8E3A16E2A945D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          283        281         28              274
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          282        276         27              281
TEMP_SEGUNDO_APELLIDO          521432D4CDBD05091DE80B64D9AE56C1 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        281        276          5              274

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               280        275         35              279
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          279        275         23              278
TEMP_ID                        DA473453C08D2E5A21B8E3A16E2A945D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         278        275          5              276
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          277        275         13              276
TEMP_ID                        DA473453C08D2E5A21B8E3A16E2A945D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        276        275          5              274
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          275        274         40              274
GETSEGUNDOAPELLIDO             62F085223C2E1CFBA9A293845CCE3698 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         274        274         14                1
TEMP_PRIMER_APELLIDO           27E2EDE2E520514F5111BDC46F98EB10 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          273        271         16              262
TEMP_PRIMER_APELLIDO           27E2EDE2E520514F5111BDC46F98EB10 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         272        267         14              262
TEMP_ID                        4882623A7B51C021598E629682B4752E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          271        269         28              262
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          270        264         26              269

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_PRIMER_APELLIDO           27E2EDE2E520514F5111BDC46F98EB10 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        269        264          5              262
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               268        263         35              267
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          267        263         23              266
TEMP_ID                        4882623A7B51C021598E629682B4752E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         266        263          5              264
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          265        263         13              264
TEMP_ID                        4882623A7B51C021598E629682B4752E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        264        263          5              262
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          263        262         39              262
GETPRIMERAPELLIDO              B28EF48D9D71118EAC2E76C8B18A06EC FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         262        262         14                1
TEMP_NOMBRE                    112AB3115A29F5CEB9FEB0FAFE5496C7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          261        259         16              250
TEMP_NOMBRE                    112AB3115A29F5CEB9FEB0FAFE5496C7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         260        255         14              250
TEMP_ID                        B5208E12F73F70A05D394B7225060BF7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          259        257         28              250

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          258        252         17              257
TEMP_NOMBRE                    112AB3115A29F5CEB9FEB0FAFE5496C7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        257        252          5              250
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               256        251         35              255
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          255        251         23              254
TEMP_ID                        B5208E12F73F70A05D394B7225060BF7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         254        251          5              252
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          253        251         13              252
TEMP_ID                        B5208E12F73F70A05D394B7225060BF7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        252        251          5              250
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          251        250         31              250
GETNOMBRE                      80386C15F8282412FD317945E992D5D8 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         250        250         14                1
TEMP_CORREO                    6146F53841E63F43D18F2B7900B7B236 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          249        240         35              231
TEMP_ID                        8EED56B4A844739BFBEEE3C86AA55165 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          248        241         32              231

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_CORREO                    6146F53841E63F43D18F2B7900B7B236 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          247        238         41              246
VALIDACORREO                   49CE30E62EB7DCD65BD4FD7A0EB84404 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               246        238         28              245
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          245        238         16              231
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               244        236         35              243
BEFOREUPDATEAVISTAMIENTO       1ED6D223BBF5939BAEDAF8EA31DD6086 TRIGGER            BEFOREUPDATEAVISTAMIENTO       TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEAVISTAMIENTO       1ED6D223BBF5939BAEDAF8EA31DD6086 TRIGGER            BEFOREUPDATEAVISTAMIENTO       TRIGGER       DECLARATION          1          1         23                0
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEPASSWORD           TRIGGER       CALL                 6         11         60                2
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEPASSWORD           TRIGGER       CALL                 5         11         52                2
GETUSUARIOACTUAL               23407D986441AB14C7033A9AE4BCCFD7 FUNCTION           BEFOREUPDATEPASSWORD           TRIGGER       CALL                 4         11         35                3
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            BEFOREUPDATEPASSWORD           TRIGGER       REFERENCE            3         11         23                2
BEFOREUPDATEPASSWORD           7D540F9098CB8C874CC6E85676C59F7C TRIGGER            BEFOREUPDATEPASSWORD           TRIGGER       DEFINITION           2          1         23                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREUPDATEPASSWORD           7D540F9098CB8C874CC6E85676C59F7C TRIGGER            BEFOREUPDATEPASSWORD           TRIGGER       DECLARATION          1          1         23                0
BEFOREINSERTDISTRITO           CB27ED15A23129AD2BC2FF73A2D4F898 TRIGGER            BEFOREINSERTDISTRITO           TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEDISTRITO           TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEDISTRITO           TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEDISTRITO           23D0B9176B2C2B9BDF8B966C12CABCCA TRIGGER            BEFOREUPDATEDISTRITO           TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEDISTRITO           23D0B9176B2C2B9BDF8B966C12CABCCA TRIGGER            BEFOREUPDATEDISTRITO           TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTESPECIE            TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTESPECIE            TRIGGER       CALL                 3          6         26                2
BEFOREINSERTESPECIE            76950E73A299E9872EA0593F78A5DAEE TRIGGER            BEFOREINSERTESPECIE            TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTESPECIE            76950E73A299E9872EA0593F78A5DAEE TRIGGER            BEFOREINSERTESPECIE            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEESPECIE            TRIGGER       CALL                 4          7         33                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEESPECIE            TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEESPECIE            ACF159BA4A8E842756F9CA47891BD9B1 TRIGGER            BEFOREUPDATEESPECIE            TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEESPECIE            ACF159BA4A8E842756F9CA47891BD9B1 TRIGGER            BEFOREUPDATEESPECIE            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTFAMILIA            TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTFAMILIA            TRIGGER       CALL                 3          6         26                2
BEFOREINSERTFAMILIA            EF18377B3852C4EADCB21354EDDF2119 TRIGGER            BEFOREINSERTFAMILIA            TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTFAMILIA            EF18377B3852C4EADCB21354EDDF2119 TRIGGER            BEFOREINSERTFAMILIA            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEFAMILIA            TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEFAMILIA            TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEFAMILIA            0F10B14851FBD5B7358B7EE810147D65 TRIGGER            BEFOREUPDATEFAMILIA            TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEFAMILIA            0F10B14851FBD5B7358B7EE810147D65 TRIGGER            BEFOREUPDATEFAMILIA            TRIGGER       DECLARATION          1          1         23                0

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTFOTO               TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTFOTO               TRIGGER       CALL                 3          6         26                2
BEFOREINSERTFOTO               7BA71E8D2CB886BCFAE915048DB64799 TRIGGER            BEFOREINSERTFOTO               TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTFOTO               7BA71E8D2CB886BCFAE915048DB64799 TRIGGER            BEFOREINSERTFOTO               TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEFOTO               TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEFOTO               TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEFOTO               9172A6AB4EC064FF39A390CCCEB57938 TRIGGER            BEFOREUPDATEFOTO               TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEFOTO               9172A6AB4EC064FF39A390CCCEB57938 TRIGGER            BEFOREUPDATEFOTO               TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTGENERAAVISTAMIENTO TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTGENERAAVISTAMIENTO TRIGGER       CALL                 3          6         26                2
BEFOREINSERTGENERAAVISTAMIENTO 4E0F471984E85A650267D98C3DF1267A TRIGGER            BEFOREINSERTGENERAAVISTAMIENTO TRIGGER       DEFINITION           2          1         23                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREINSERTGENERAAVISTAMIENTO 4E0F471984E85A650267D98C3DF1267A TRIGGER            BEFOREINSERTGENERAAVISTAMIENTO TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEGENERAAVISTAMIENTO TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEGENERAAVISTAMIENTO TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEGENERAAVISTAMIENTO FF60E75343539A8C8BE8F87011B8757C TRIGGER            BEFOREUPDATEGENERAAVISTAMIENTO TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEGENERAAVISTAMIENTO FF60E75343539A8C8BE8F87011B8757C TRIGGER            BEFOREUPDATEGENERAAVISTAMIENTO TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTGENERO             TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTGENERO             TRIGGER       CALL                 3          6         26                2
BEFOREINSERTGENERO             CF7EF9813A199B2D2DF06F80A9C9F3B3 TRIGGER            BEFOREINSERTGENERO             TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTGENERO             CF7EF9813A199B2D2DF06F80A9C9F3B3 TRIGGER            BEFOREINSERTGENERO             TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEGENERO             TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEGENERO             TRIGGER       CALL                 3          6         37                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREUPDATEGENERO             E7AD44A0A608F82C68B44F2518755DFA TRIGGER            BEFOREUPDATEGENERO             TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEGENERO             E7AD44A0A608F82C68B44F2518755DFA TRIGGER            BEFOREUPDATEGENERO             TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTMUESTRAAVIS        TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTMUESTRAAVIS        TRIGGER       CALL                 3          6         26                2
BEFOREINSERTMUESTRAAVIS        98BA01ED7C9797C0D62D31142C3C821D TRIGGER            BEFOREINSERTMUESTRAAVIS        TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTMUESTRAAVIS        98BA01ED7C9797C0D62D31142C3C821D TRIGGER            BEFOREINSERTMUESTRAAVIS        TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEMUESTRAAVIS        TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEMUESTRAAVIS        TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEMUESTRAAVIS        34FCF90C822DF58DF65993E83AE68066 TRIGGER            BEFOREUPDATEMUESTRAAVIS        TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEMUESTRAAVIS        34FCF90C822DF58DF65993E83AE68066 TRIGGER            BEFOREUPDATEMUESTRAAVIS        TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTORDEN              TRIGGER       CALL                 4          7         22                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTORDEN              TRIGGER       CALL                 3          6         26                2
BEFOREINSERTORDEN              515DC8D0873F740030D0C6B64E0F3365 TRIGGER            BEFOREINSERTORDEN              TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTORDEN              515DC8D0873F740030D0C6B64E0F3365 TRIGGER            BEFOREINSERTORDEN              TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEORDEN              TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEORDEN              TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEORDEN              F3C9DB57E1EC01AC4220A299FF059BEA TRIGGER            BEFOREUPDATEORDEN              TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEORDEN              F3C9DB57E1EC01AC4220A299FF059BEA TRIGGER            BEFOREUPDATEORDEN              TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTPAIS               TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTPAIS               TRIGGER       CALL                 3          6         26                2
BEFOREINSERTPAIS               E6FC71F0FCA35ECE906A79888BE6F130 TRIGGER            BEFOREINSERTPAIS               TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTPAIS               E6FC71F0FCA35ECE906A79888BE6F130 TRIGGER            BEFOREINSERTPAIS               TRIGGER       DECLARATION          1          1         23                0

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEPAIS               TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEPAIS               TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEPAIS               EAD9EB00B921BB4C2EE2631221FFDB98 TRIGGER            BEFOREUPDATEPAIS               TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEPAIS               EAD9EB00B921BB4C2EE2631221FFDB98 TRIGGER            BEFOREUPDATEPAIS               TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTPARAMETROS         TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTPARAMETROS         TRIGGER       CALL                 3          6         26                2
BEFOREINSERTPARAMETROS         9390F31F1A11E23FAFEFEFD9A4BC5B50 TRIGGER            BEFOREINSERTPARAMETROS         TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTPARAMETROS         9390F31F1A11E23FAFEFEFD9A4BC5B50 TRIGGER            BEFOREINSERTPARAMETROS         TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEPARAMETROS         TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEPARAMETROS         TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEPARAMETROS         B9F188F7907FEBABE1FBA00822AAB5E1 TRIGGER            BEFOREUPDATEPARAMETROS         TRIGGER       DEFINITION           2          1         23                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREUPDATEPARAMETROS         B9F188F7907FEBABE1FBA00822AAB5E1 TRIGGER            BEFOREUPDATEPARAMETROS         TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTPERSONA            TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTPERSONA            TRIGGER       CALL                 3          6         26                2
BEFOREINSERTPERSONA            F7968FD8F954271F9D8F7A79EFC5BD5B TRIGGER            BEFOREINSERTPERSONA            TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTPERSONA            F7968FD8F954271F9D8F7A79EFC5BD5B TRIGGER            BEFOREINSERTPERSONA            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEPERSONA            TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEPERSONA            TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEPERSONA            F0F9ADBE8B3A4402C3E327703340C4B9 TRIGGER            BEFOREUPDATEPERSONA            TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEPERSONA            F0F9ADBE8B3A4402C3E327703340C4B9 TRIGGER            BEFOREUPDATEPERSONA            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTPROVINCIA          TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTPROVINCIA          TRIGGER       CALL                 3          6         26                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREINSERTPROVINCIA          1793C86D08E1DC69360054791E5DE4B6 TRIGGER            BEFOREINSERTPROVINCIA          TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTPROVINCIA          1793C86D08E1DC69360054791E5DE4B6 TRIGGER            BEFOREINSERTPROVINCIA          TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEPROVINCIA          TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEPROVINCIA          TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEPROVINCIA          1FE00B8E0D45DE7046EF081104D3B87A TRIGGER            BEFOREUPDATEPROVINCIA          TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEPROVINCIA          1FE00B8E0D45DE7046EF081104D3B87A TRIGGER            BEFOREUPDATEPROVINCIA          TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTROL                TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTROL                TRIGGER       CALL                 3          6         26                2
BEFOREINSERTROL                08FF795C876F19ECFEABF06DD412B789 TRIGGER            BEFOREINSERTROL                TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTROL                08FF795C876F19ECFEABF06DD412B789 TRIGGER            BEFOREINSERTROL                TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEROL                TRIGGER       CALL                 4          7         33                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEROL                TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEROL                4BA7E872A8974854A34D7549A2FB6B78 TRIGGER            BEFOREUPDATEROL                TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEROL                4BA7E872A8974854A34D7549A2FB6B78 TRIGGER            BEFOREUPDATEROL                TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTSUB_ORDEN          TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTSUB_ORDEN          TRIGGER       CALL                 3          6         26                2
BEFOREINSERTSUB_ORDEN          A14A559060A0431F1F5C910414D622C5 TRIGGER            BEFOREINSERTSUB_ORDEN          TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTSUB_ORDEN          A14A559060A0431F1F5C910414D622C5 TRIGGER            BEFOREINSERTSUB_ORDEN          TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATESUB_ORDEN          TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATESUB_ORDEN          TRIGGER       CALL                 3          6         37                2
BEFOREUPDATESUB_ORDEN          DC9EFE496E8E8F9D10C4E92743BAF621 TRIGGER            BEFOREUPDATESUB_ORDEN          TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATESUB_ORDEN          DC9EFE496E8E8F9D10C4E92743BAF621 TRIGGER            BEFOREUPDATESUB_ORDEN          TRIGGER       DECLARATION          1          1         23                0

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTTAMANNO            TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTTAMANNO            TRIGGER       CALL                 3          6         26                2
BEFOREINSERTTAMANNO            3425DCA2FF3CDC2FBE4EFB44A606DCA8 TRIGGER            BEFOREINSERTTAMANNO            TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTTAMANNO            3425DCA2FF3CDC2FBE4EFB44A606DCA8 TRIGGER            BEFOREINSERTTAMANNO            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATETAMANNO            TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATETAMANNO            TRIGGER       CALL                 3          6         37                2
BEFOREUPDATETAMANNO            A3616F587F96E8B66B5B00A60E5672E0 TRIGGER            BEFOREUPDATETAMANNO            TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATETAMANNO            A3616F587F96E8B66B5B00A60E5672E0 TRIGGER            BEFOREUPDATETAMANNO            TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTZONAAVISTAMIENTO   TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTZONAAVISTAMIENTO   TRIGGER       CALL                 3          6         26                2
BEFOREINSERTZONAAVISTAMIENTO   93AC5B93255FDF6D6DDD870ADFF35A7A TRIGGER            BEFOREINSERTZONAAVISTAMIENTO   TRIGGER       DEFINITION           2          1         23                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREINSERTZONAAVISTAMIENTO   93AC5B93255FDF6D6DDD870ADFF35A7A TRIGGER            BEFOREINSERTZONAAVISTAMIENTO   TRIGGER       DECLARATION          1          1         23                0
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEZONAAVISTAMIENTO   TRIGGER       CALL                 4          7         33                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEZONAAVISTAMIENTO   TRIGGER       CALL                 3          6         37                2
BEFOREUPDATEZONAAVISTAMIENTO   0F491F3E81A50AD1CC4C261302EE6654 TRIGGER            BEFOREUPDATEZONAAVISTAMIENTO   TRIGGER       DEFINITION           2          1         23                1
BEFOREUPDATEZONAAVISTAMIENTO   0F491F3E81A50AD1CC4C261302EE6654 TRIGGER            BEFOREUPDATEZONAAVISTAMIENTO   TRIGGER       DECLARATION          1          1         23                0
RECORD_USUARIO                 012A8663BC9DE10FE22969A3A0CD4843 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          142        159         23              141
INSERTATAMANNO                 9A2FF51F36D3A2D19B3F8BBC5D919E3C PROCEDURE          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         40         25         15                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           39         23         79               38
PID_AVISTAMIENTO               38E4E030D31DAE2FFCFB8F05098817F5 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         38         23         59               35
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           37         23         50               36
PIMAGEN                        FD3F19C26E0A94E3D450E2E6D82383FE FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         36         23         39               35

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
INSERTAFOTOAVISTAMIENTO        33B6218B6CBD66EF59CA99CEF1F4311C PROCEDURE          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         35         23         15                1
TEMP_ID                        CC00E9B8FFCFA56BCC7D88515F66559A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          216        226         20              205
TEMP_ID                        CC00E9B8FFCFA56BCC7D88515F66559A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         215        223         18              205
PID_ESPECIE                    B674ACC9C0EFC9D362861492DCC62F70 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          214        225         32              205
PDESCRIPCION                   480FAFF463498464F08D02CC9B09CEBA FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          213        225         68              205
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          212        220         17              211
TEMP_ID                        CC00E9B8FFCFA56BCC7D88515F66559A VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        211        220          9              205
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          210        219         12              205
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          209        218         67              208
PID_ESPECIE                    B674ACC9C0EFC9D362861492DCC62F70 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        208        218         52              205
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          207        218         43              206

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDESCRIPCION                   480FAFF463498464F08D02CC9B09CEBA FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        206        218         27              205
GETIDTAMANNO                   E440AD0E411D03779BE2B65ECC7393E7 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION         205        218         14                1
TEMP_ID                        905CE3EF3F49784D1E31C9BF30E0F9F8 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          204        215         20              193
TEMP_ID                        905CE3EF3F49784D1E31C9BF30E0F9F8 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         203        212         18              193
PID_ESPECIE                    AD68721EA90DBF288C7ABE61007B0BC4 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          202        214         32              193
PDESCRIPCION                   979D292645F539207EE432682CD64AE4 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          201        214         68              193
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          200        209         17              199
TEMP_ID                        905CE3EF3F49784D1E31C9BF30E0F9F8 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        199        209          9              193
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          198        208         12              193
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          197        207         65              196
PID_ESPECIE                    AD68721EA90DBF288C7ABE61007B0BC4 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        196        207         50              193

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          195        207         41              194
PDESCRIPCION                   979D292645F539207EE432682CD64AE4 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        194        207         25              193
GETIDCOLOR                     A1EE8E5A2CED00F058DCF6A22EC01FB5 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION         193        207         14                1
PDESCRIPCION                   9848D7F03E60B606B7F473C20A14662B FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          191        196         33              184
PID                            EE8B849E2F2C0F2ED7845C596E11C2B8 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          190        197         28              184
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          189        193         12              184
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          188        192         61              187
PID                            EE8B849E2F2C0F2ED7845C596E11C2B8 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        187        192         54              184
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          186        192         44              185
PDESCRIPCION                   9848D7F03E60B606B7F473C20A14662B FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        185        192         28              184
UPDATETAMANNO                  15C23460BF07E4480CA6E4974D4A1694 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION         184        192         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDESCRIPCION                   98D69D9356181E749BCF8896F8531754 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          182        181         33              175
PID                            7B87BE8ECC565384D329C57836BB9603 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          181        182         28              175
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          180        178         12              175
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          179        177         59              178
PID                            7B87BE8ECC565384D329C57836BB9603 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        178        177         52              175
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          177        177         42              176
PDESCRIPCION                   98D69D9356181E749BCF8896F8531754 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        176        177         26              175
UPDATECOLOR                    93FDD468E9A7C16355E80BB4FDC21E49 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  DEFINITION         175        177         14                1
LISTA_NOMBRES                  3D15371E7245A95D0A4DDA0DBBDF7E34 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          174        174         16              139
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          172        172         55              159
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          170        172         40              159

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          168        172         29              159
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          166        171         72              159
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          164        171         50              159
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          162        171         29              159
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          160        170         66              159
T_USUARIO                      E5A51E5C4AB81D4B4F04D12B3C7D528C OBJECT             PAQ_ADMINISTRACION             PACKAGE BODY  CALL               159        170         56              156
LISTA_NOMBRES                  3D15371E7245A95D0A4DDA0DBBDF7E34 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          158        170         31              157
LISTA_NOMBRES                  3D15371E7245A95D0A4DDA0DBBDF7E34 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         156        170         17              152
LISTA_NOMBRES                  3D15371E7245A95D0A4DDA0DBBDF7E34 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          155        169         17              154
TEMP_ID                        E38A0B80BDEB5BB439EA608082A52575 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          153        167         28              152
DATO                           98D1C1FF9D61F5D5DB823E341C6BE45E ITERATOR           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        152        163         14              139

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
ROL_ADMINISTRADOR              22B20230F8D778554987A563304C6E12 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          151        161         44              150
PAQ_ADMINISTRACION             EEAC7F41E96B0E0E0BA343B3097DEA29 PACKAGE            PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          150        161         25              149
GETROLID                       24A8FC7E663D7D74C2C6D6C0B776C525 FUNCTION           PAQ_ADMINISTRACION             PACKAGE BODY  CALL               149        160         45              148
PAQ_ADMINISTRACION             EEAC7F41E96B0E0E0BA343B3097DEA29 PACKAGE            PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          148        160         26              147
TEMP_ID                        E38A0B80BDEB5BB439EA608082A52575 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         147        160          8              145
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE BODY  REFERENCE          146        160         16              145
TEMP_ID                        E38A0B80BDEB5BB439EA608082A52575 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  DECLARATION        145        160          8              139
RECORD_USUARIO                 012A8663BC9DE10FE22969A3A0CD4843 NESTED TABLE       PAQ_ADMINISTRACION             PACKAGE BODY  CALL               144        159         41              143
LISTA_NOMBRES                  3D15371E7245A95D0A4DDA0DBBDF7E34 VARIABLE           PAQ_ADMINISTRACION             PACKAGE BODY  ASSIGNMENT         143        159          8              141
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           73         35         82               68
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           72         35         67               71

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_ESPECIE                    9B9ABF88C2499B8173B5886496593811 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         71         35         52               68
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           70         35         43               69
PDESCRIPCION                   154F6D3E6D3A62EF6095FDD300A071A4 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         69         35         27               68
GETIDTAMANNO                   E440AD0E411D03779BE2B65ECC7393E7 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         68         35         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           67         33         80               62
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           66         33         65               65
PID_ESPECIE                    48F274CF4477B86DDDF719A76A20E690 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         65         33         50               62
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           64         33         41               63
PDESCRIPCION                   8D855E9F726BBF7FEF1987D77F201A39 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         63         33         25               62
GETIDCOLOR                     A1EE8E5A2CED00F058DCF6A22EC01FB5 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         62         33         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           61         31         76               56

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           60         31         61               59
PID                            737CB7BE8294616F72A4C6D1B4D6C873 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         59         31         54               56
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           58         31         44               57
PDESCRIPCION                   F1DC47DBA65F002F66FB0EAAF89CA05B FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         57         31         28               56
UPDATETAMANNO                  15C23460BF07E4480CA6E4974D4A1694 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         56         31         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           55         29         74               50
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           54         29         59               53
PID                            2CB7FF07881F968955AC07DE48E10734 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         53         29         52               50
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           52         29         42               51
PDESCRIPCION                   351FB767837275458017F41F6BEAAA53 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         51         29         26               50
UPDATECOLOR                    93FDD468E9A7C16355E80BB4FDC21E49 FUNCTION           PAQ_ADMINISTRACION             PACKAGE       DECLARATION         50         29         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           49         27         69               48
PID_ESPECIE                    4CE5E63866679AFEBEDB85C369E8F0A5 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         48         27         54               45
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           47         27         44               46
PDESCRIPCION                   C53B805CD5C7BC6A206517FA6A11116F FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         46         27         28               45
INSERTACOLOR                   B4D05865224018839DA47BF38718C59D PROCEDURE          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         45         27         15                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_ADMINISTRACION             PACKAGE       REFERENCE           44         25         71               43
PID_ESPECIE                    8642C0DC031153E6A4C13021C94AF5C7 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         43         25         56               40
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_ADMINISTRACION             PACKAGE       REFERENCE           42         25         46               41
PDESCRIPCION                   CBA06C3E11CA4FBA850DEC72474BAA73 FORMAL IN          PAQ_ADMINISTRACION             PACKAGE       DECLARATION         41         25         30               40
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREINSERTAVISTAMIENTO       TRIGGER       CALL                 4          7         22                2
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREINSERTAVISTAMIENTO       TRIGGER       CALL                 3          6         26                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
BEFOREINSERTAVISTAMIENTO       9F5E36982E1B9CB1AA21A5513B9066C6 TRIGGER            BEFOREINSERTAVISTAMIENTO       TRIGGER       DEFINITION           2          1         23                1
BEFOREINSERTAVISTAMIENTO       9F5E36982E1B9CB1AA21A5513B9066C6 TRIGGER            BEFOREINSERTAVISTAMIENTO       TRIGGER       DECLARATION          1          1         23                0
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          243        236         23              242
TEMP_ID                        8EED56B4A844739BFBEEE3C86AA55165 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         242        236          5              240
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          241        236         13              240
TEMP_ID                        8EED56B4A844739BFBEEE3C86AA55165 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        240        236          5              231
PCORREO                        4DEE371D6CD6B267AE53B77469F9DFBC FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          239        235         40              238
UPPER                          F158BBA05C53DA8CE6AD6217DA5CB1E8 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               238        235         34              237
TEMP_CORREO                    6146F53841E63F43D18F2B7900B7B236 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         237        235          5              235
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          236        235         17              235
TEMP_CORREO                    6146F53841E63F43D18F2B7900B7B236 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        235        235          5              231

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
CHAR                           827DC51E93B66D1B683E1965CB2103FD SUBTYPE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          234        234         55              231
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          233        234         38              232
PCORREO                        4DEE371D6CD6B267AE53B77469F9DFBC FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        232        234         27              231
UPDATECORREO                   C020B6A3426C99D69E2736B3407D231D FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         231        234         14                1
PFECHA                         CF908D0D6FE4359D793025E54E26DC0B FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          230        230         44              221
TEMP_ID                        39F589CF8A294F0A679F46B68C65351E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          229        231         32              221
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               228        227         39              227
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          227        227         27              226
TEMP_ID                        39F589CF8A294F0A679F46B68C65351E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         226        227          9              224
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          225        227         17              224
TEMP_ID                        39F589CF8A294F0A679F46B68C65351E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        224        227          9              221

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          223        226         47              222
PFECHA                         CF908D0D6FE4359D793025E54E26DC0B FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        222        226         37              221
UPDATEFECHANACIMIENTO          7B30099DB44CB4F82F2F9636C8D28C49 PROCEDURE          PAQ_PERSONA                    PACKAGE BODY  DEFINITION         221        226         15                1
TEMP_PROFESION_DESPUES         B51D166EAC66FEE9C9D0EAC256AF3040 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          220        223         45              200
TEMP_PROFESION_ANTES           C914C4F0731D5375AF0537C347C3B30D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          219        223         21              200
TEMP_PROFESION_DESPUES         B51D166EAC66FEE9C9D0EAC256AF3040 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         218        219         18              200
TEMP_ID                        CAB1BCBC72BB692AD99F18989E115F5A VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          217        221         32              200
PPROFESION                     52EFA6F90F8071CE4B87EAA4E6BFCAA7 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          216        215         35              200
TEMP_ID                        CAB1BCBC72BB692AD99F18989E115F5A VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          215        216         32              200
TEMP_PROFESION_ANTES           C914C4F0731D5375AF0537C347C3B30D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         214        210         18              200
TEMP_ID                        CAB1BCBC72BB692AD99F18989E115F5A VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          213        212         32              200

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               212        207         39              211
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          211        207         27              210
TEMP_ID                        CAB1BCBC72BB692AD99F18989E115F5A VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         210        207          9              208
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          209        207         17              208
TEMP_ID                        CAB1BCBC72BB692AD99F18989E115F5A VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        208        207          9              200
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          207        206         32              206
TEMP_PROFESION_DESPUES         B51D166EAC66FEE9C9D0EAC256AF3040 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        206        206          9              200
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          205        205         30              204
TEMP_PROFESION_ANTES           C914C4F0731D5375AF0537C347C3B30D VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        204        205          9              200
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE          203        204         61              200
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          202        204         44              201

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PPROFESION                     52EFA6F90F8071CE4B87EAA4E6BFCAA7 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        201        204         30              200
UPDATEPROFESION                498951A146EACEE1E6C379B89A39F894 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         200        204         14                1
TEMP_APELLIDO_DESPUES          BA4C39348A35F03463F3027A9448ADA7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          199        201         44              179
TEMP_APELLIDO_ANTES            AFD784ECB3DAEDBDD70ACF1EED333F75 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          198        201         21              179
TEMP_APELLIDO_DESPUES          BA4C39348A35F03463F3027A9448ADA7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         197        197         18              179
TEMP_ID                        75876B91ADFA0BC915EF374920423A27 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          196        199         32              179
PSEGUNDO_APELLIDO              6124CE7D679D83444624503762D4A821 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          195        193         41              179
TEMP_ID                        75876B91ADFA0BC915EF374920423A27 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          194        194         32              179
TEMP_APELLIDO_ANTES            AFD784ECB3DAEDBDD70ACF1EED333F75 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         193        188         18              179
TEMP_ID                        75876B91ADFA0BC915EF374920423A27 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          192        190         32              179
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               191        185         39              190

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          190        185         27              189
TEMP_ID                        75876B91ADFA0BC915EF374920423A27 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         189        185          9              187
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          188        185         17              187
TEMP_ID                        75876B91ADFA0BC915EF374920423A27 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        187        185          9              179
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          186        184         31              185
TEMP_APELLIDO_DESPUES          BA4C39348A35F03463F3027A9448ADA7 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        185        184          9              179
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          184        183         29              183
TEMP_APELLIDO_ANTES            AFD784ECB3DAEDBDD70ACF1EED333F75 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        183        183          9              179
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE          182        182         74              179
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          181        182         57              180
PSEGUNDO_APELLIDO              6124CE7D679D83444624503762D4A821 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        180        182         36              179

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
UPDATESEGUNDOAPELLIDO          E9EBAABA63D1AA24E137C84E4E55B0AD FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         179        182         14                1
TEMP_APELLIDO_DESPUES          60DFDCB0056234FC55A701EED526B3A5 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          178        179         44              158
TEMP_APELLIDO_ANTES            5F24FD08FAEF176CC17B7184B7C113B2 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          177        179         21              158
TEMP_APELLIDO_DESPUES          60DFDCB0056234FC55A701EED526B3A5 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         176        175         18              158
TEMP_ID                        704A883F23B665E276FBB3CA0AAE25E9 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          175        177         32              158
PPRIMER_APELLIDO               DB63F729E8AB4308581860565820CD80 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          174        171         40              158
TEMP_ID                        704A883F23B665E276FBB3CA0AAE25E9 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          173        172         32              158
TEMP_APELLIDO_ANTES            5F24FD08FAEF176CC17B7184B7C113B2 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         172        166         18              158
TEMP_ID                        704A883F23B665E276FBB3CA0AAE25E9 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          171        168         32              158
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               170        163         39              169
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          169        163         27              168

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_ID                        704A883F23B665E276FBB3CA0AAE25E9 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         168        163          9              166
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          167        163         17              166
TEMP_ID                        704A883F23B665E276FBB3CA0AAE25E9 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        166        163          9              158
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          165        162         31              164
TEMP_APELLIDO_DESPUES          60DFDCB0056234FC55A701EED526B3A5 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        164        162          9              158
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          163        161         29              162
TEMP_APELLIDO_ANTES            5F24FD08FAEF176CC17B7184B7C113B2 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        162        161          9              158
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE          161        160         72              158
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          160        160         55              159
PPRIMER_APELLIDO               DB63F729E8AB4308581860565820CD80 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        159        160         35              158
UPDATEPRIMERAPELLIDO           10D228FBBDDB3A5CA7026EFED237ACFA FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         158        160         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_NOMBRE_DESPUES            7CEA2042E489DEECAC2E1A662EF13412 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          157        157         42              137
TEMP_NOMBRE_ANTES              9E6EAEED17636AA1BBFCE147DEE24177 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          156        157         21              137
TEMP_NOMBRE_DESPUES            7CEA2042E489DEECAC2E1A662EF13412 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         155        153         18              137
TEMP_ID                        1244BDAB7BEABA6921F58E2C973DB071 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          154        155         32              137
PNOMBRE                        F4764F4610DA18D522BA1BD1D0668E74 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          153        149         32              137
TEMP_ID                        1244BDAB7BEABA6921F58E2C973DB071 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          152        150         32              137
TEMP_NOMBRE_ANTES              9E6EAEED17636AA1BBFCE147DEE24177 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         151        144         18              137
TEMP_ID                        1244BDAB7BEABA6921F58E2C973DB071 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          150        146         32              137
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               149        141         39              148
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          148        141         27              147
TEMP_ID                        1244BDAB7BEABA6921F58E2C973DB071 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         147        141          9              145

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          146        141         17              145
TEMP_ID                        1244BDAB7BEABA6921F58E2C973DB071 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        145        141          9              137
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          144        140         27              143
TEMP_NOMBRE_ANTES              9E6EAEED17636AA1BBFCE147DEE24177 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        143        140          9              137
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          142        139         29              141
TEMP_NOMBRE_DESPUES            7CEA2042E489DEECAC2E1A662EF13412 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        141        139          9              137
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE BODY  REFERENCE          140        138         55              137
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          139        138         38              138
PNOMBRE                        F4764F4610DA18D522BA1BD1D0668E74 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        138        138         27              137
UPDATENOMBRE                   A13D5F59690CBA459EF422D1C7482707 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         137        138         14                1
ID_PERSONACONECTADA            2E1590D7E0ECD8669BC2C52DA9C786C0 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          136        135         20              134

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE           15          4         13               14
DESCRIPCION                    96C042FD02289FD5EAF714855FAD1EAB VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION         14          4          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_CONSULTACOLORTAMANO          TYPE          REFERENCE           13          3         54               12
ESPECIE                        22655898342DEC2E31D5761298BEB926 VARIABLE           T_CONSULTACOLORTAMANO          TYPE          DECLARATION         12          3         46                1
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           40         42         46                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           38         42         58                9
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           80         26         62               79
PID_REPORTE                    B5D632D122C17BE5A76C312A9C3B6469 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         79         26         50               76
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           78         26         42               77
PID_ESPECIE                    FB4EDDBDB8FB2B523859A444FC02A37A FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         77         26         30               76
PID_REPORTE                    3D110E1C794FC27E21E0F21B0E7D4A26 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          351        221         16              345

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_ESPECIE                    6187BE9651546D4C685ECFBD248AC111 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          350        221         28              345
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          349        217         62              348
PID_REPORTE                    3D110E1C794FC27E21E0F21B0E7D4A26 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        348        217         50              345
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          347        217         42              346
PID_ESPECIE                    6187BE9651546D4C685ECFBD248AC111 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        346        217         30              345
MUESTRAREPORTE                 F6C2045309F48260C3D10C7C3ACB5170 PROCEDURE          PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         345        217         15                1
PID_PERSONA                    C61F096D97FED77E4E7978CBA868BB94 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          344        213         16              338
PID_REPORTE                    4BECA2089730AF6313D2AD1819DE7F09 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          343        213         28              338
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          342        209         61              341
PID_REPORTE                    4BECA2089730AF6313D2AD1819DE7F09 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        341        209         49              338
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          340        209         41              339

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_PERSONA                    C61F096D97FED77E4E7978CBA868BB94 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        339        209         29              338
GENERAREPORTE                  CE2B832D90C00CC2BC50D723C455B0B5 PROCEDURE          PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         338        209         15                1
TEMP_ID_REPORTE                7157BEA319B55A79C4609BF0BA51295D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          337        206         39              335
TEMP_ID_PERSONA                8CABA34431EA20FE1B8C88A1864DFAAC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          336        206         23              335
GENERAREPORTE                  CE2B832D90C00CC2BC50D723C455B0B5 PROCEDURE          PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               335        206          9              293
TEMP_ID_REPORTE                7157BEA319B55A79C4609BF0BA51295D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          334        205         40              332
TEMP_ID_ESPECIE                A85C3EB2BB1EC4D45933FAFA4D1664B7 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          333        205         24              332
MUESTRAREPORTE                 F6C2045309F48260C3D10C7C3ACB5170 PROCEDURE          PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               332        205          9              293
TEMP_ID_REPORTE                7157BEA319B55A79C4609BF0BA51295D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          331        202         16              293
PCLASE                         267DF3AD7E37F7BBE73D160A664F3346 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          330        202         32              293
PORDEN                         8204A1FD58DFC07277C4A9344EDC169D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          329        202         39              293

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PSUBORDEN                      6B2258E0157D6C22D7C6FD8E786A6BC1 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          328        202         46              293
PFAMILIA                       F42A5A0184FDD396CCA2188851B96BFC FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          327        202         56              293
PGENERO                        3375FDE5D1D1BCB0725EE807DBA241E8 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          326        202         65              293
PESPECIE                       A5552645BE819ABB90E09749C465B20C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          325        202         73              293
PID_ZONAAVISTAMIENTO           38412A0F160AA1D61846977A67D6D9A9 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          324        203         17              293
TEMP_ID_ESPECIE                A85C3EB2BB1EC4D45933FAFA4D1664B7 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         323        195         14              293
PESPECIE                       A5552645BE819ABB90E09749C465B20C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          322        197         24              293
TEMP_ID_REPORTE                7157BEA319B55A79C4609BF0BA51295D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          321        192         48              320
AVISTAMIENTOGLOBAL             1B58AD2C70F1D8361A34E58AE76AE181 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         320        192         26              319
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          319        192          9              293
TEMP_ID_REPORTE                7157BEA319B55A79C4609BF0BA51295D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         318        189         14              293

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          317        186         21              316
TEMP_ID_REPORTE                7157BEA319B55A79C4609BF0BA51295D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        316        186          5              293
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          315        185         21              314
TEMP_ID_ESPECIE                A85C3EB2BB1EC4D45933FAFA4D1664B7 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        314        185          5              293
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               313        184         43              312
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          312        184         31              311
TEMP_ID_PERSONA                8CABA34431EA20FE1B8C88A1864DFAAC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         311        184          5              309
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          310        184         21              309
TEMP_ID_PERSONA                8CABA34431EA20FE1B8C88A1864DFAAC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        309        184          5              293
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          308        183         53              293
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          307        183         38              306

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID_ZONAAVISTAMIENTO           38412A0F160AA1D61846977A67D6D9A9 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        306        183         17              293
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          305        182         63              304
PESPECIE                       A5552645BE819ABB90E09749C465B20C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        304        182         54              293
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          303        182         44              302
PGENERO                        3375FDE5D1D1BCB0725EE807DBA241E8 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        302        182         36              293
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          301        182         26              300
PFAMILIA                       F42A5A0184FDD396CCA2188851B96BFC FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        300        182         17              293
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          299        181         74              298
PSUBORDEN                      6B2258E0157D6C22D7C6FD8E786A6BC1 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        298        181         64              293
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          297        181         54              296
PORDEN                         8204A1FD58DFC07277C4A9344EDC169D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        296        181         47              293

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          295        181         38              294
PCLASE                         267DF3AD7E37F7BBE73D160A664F3346 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        294        181         31              293
CREAAVISTAMIENTO               093D77CB72605CEEB578CF2325EE4E8C FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         293        181         14                1
LISTA_NOMBRES                  6FBB02C3A463B72073DD9C23ED4D67E6 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          292        178         16              274
DATO                           D7BDA9EF51E12627D099D148774F86FB ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          290        176         65              289
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               289        176         56              286
LISTA_NOMBRES                  6FBB02C3A463B72073DD9C23ED4D67E6 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          288        176         31              287
LISTA_NOMBRES                  6FBB02C3A463B72073DD9C23ED4D67E6 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         286        176         17              282
LISTA_NOMBRES                  6FBB02C3A463B72073DD9C23ED4D67E6 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          285        175         17              284
PID                            43FAC1D892B78E5A59DC633E0B73CD83 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          283        174         85              282
DATO                           D7BDA9EF51E12627D099D148774F86FB ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        282        174         14              274

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               281        172         37              280
LISTA_NOMBRES                  6FBB02C3A463B72073DD9C23ED4D67E6 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         280        172          8              278
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          279        172         23              278
LISTA_NOMBRES                  6FBB02C3A463B72073DD9C23ED4D67E6 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        278        172          8              274
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          277        170         56              274
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          276        170         41              275
PID                            43FAC1D892B78E5A59DC633E0B73CD83 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        275        170         34              274
GET_ZONAAVISTAMIENTO           D3E13F3B5B465237F14700F2653C2474 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         274        170         13                1
LISTA_NOMBRES                  4416637F3600CA792A05CAD766BE8D65 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          273        167         16              255
DATO                           8F14AB38BE67CA8B74A5649A2D16D345 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          271        165         65              270
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               270        165         56              267

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  4416637F3600CA792A05CAD766BE8D65 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          269        165         31              268
LISTA_NOMBRES                  4416637F3600CA792A05CAD766BE8D65 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         267        165         17              263
LISTA_NOMBRES                  4416637F3600CA792A05CAD766BE8D65 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          266        164         17              265
PID                            15F0B104A70D464D1BC7377232F71126 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          264        163         69              263
DATO                           8F14AB38BE67CA8B74A5649A2D16D345 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        263        163         14              255
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               262        161         37              261
LISTA_NOMBRES                  4416637F3600CA792A05CAD766BE8D65 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         261        161          8              259
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          260        161         23              259
LISTA_NOMBRES                  4416637F3600CA792A05CAD766BE8D65 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        259        161          8              255
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          258        159         48              255
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          257        159         33              256

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID                            15F0B104A70D464D1BC7377232F71126 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        256        159         26              255
GET_DISTRITO                   B4EE0A9989C06D6B1FE210B7C574B393 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         255        159         13                1
LISTA_NOMBRES                  E93A977B08237775403C3F1EE716393C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          254        156         16              236
DATO                           472DC20FCCF5660594513DB1A6B8CE75 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          252        154         65              251
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               251        154         56              248
LISTA_NOMBRES                  E93A977B08237775403C3F1EE716393C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          250        154         31              249
LISTA_NOMBRES                  E93A977B08237775403C3F1EE716393C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         248        154         17              244
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           94         52         35               93
GETPROFESION                   33CB6C7156E5C41698B898EEF63F1DB6 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         93         52         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           92         50         40               91
GETFECHANACIMIENTO             39BC4AEDB85C6E425121CFDEC20104E5 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         91         50         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           90         48         40               89
GETSEGUNDOAPELLIDO             62F085223C2E1CFBA9A293845CCE3698 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         89         48         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           88         46         39               87
GETPRIMERAPELLIDO              B28EF48D9D71118EAC2E76C8B18A06EC FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         87         46         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           86         44         31               85
GETNOMBRE                      80386C15F8282412FD317945E992D5D8 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         85         44         14                1
CHAR                           827DC51E93B66D1B683E1965CB2103FD SUBTYPE            PAQ_PERSONA                    PACKAGE       REFERENCE           84         42         55               81
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           83         42         38               82
PCORREO                        09C01BF1FC5D51D257D296DC425C1A1F FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         82         42         27               81
UPDATECORREO                   C020B6A3426C99D69E2736B3407D231D FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         81         42         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           80         40         47               79

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PFECHA                         F78F2FD1A6740A50EB2F547A3CBF3076 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         79         40         37               78
UPDATEFECHANACIMIENTO          7B30099DB44CB4F82F2F9636C8D28C49 PROCEDURE          PAQ_PERSONA                    PACKAGE       DECLARATION         78         40         15                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           77         38         61               74
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           76         38         44               75
PPROFESION                     BD9EAC8D70CE5F7EECCACE7624DD4E96 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         75         38         30               74
UPDATEPROFESION                498951A146EACEE1E6C379B89A39F894 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         74         38         14                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           73         36         74               70
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           72         36         57               71
PSEGUNDO_APELLIDO              83AC54A95B69A0F5656D7D5B895625A1 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         71         36         36               70
UPDATESEGUNDOAPELLIDO          E9EBAABA63D1AA24E137C84E4E55B0AD FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         70         36         14                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           69         34         72               66

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           68         34         55               67
PPRIMER_APELLIDO               A1AF78E05551ED508F32679C9499C6F8 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         67         34         35               66
UPDATEPRIMERAPELLIDO           10D228FBBDDB3A5CA7026EFED237ACFA FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         66         34         14                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           65         32         55               62
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           64         32         38               63
PNOMBRE                        190F0C92CC09A6838812AC9DD716117D FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         63         32         27               62
UPDATENOMBRE                   A13D5F59690CBA459EF422D1C7482707 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         62         32         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE       REFERENCE           61         30         40               60
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         60         30         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           59         28         46               58
PUSUARIO                       C0A2BCF286E8E005D5B2837C0431A2CB FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         58         28         34               57

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
SETIDPERSONAACTUAL             B7F341C767789A1F821E380A980C72D5 PROCEDURE          PAQ_PERSONA                    PACKAGE       DECLARATION         57         28         15                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           56         26         63               53
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           55         26         46               54
PCONTRASENA                    12C248621ACEE322AFCAB42E2B3261BD FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         54         26         31               53
UPDATECONTRASENA               EDA7C7B00F88CDF92A360E1E37DD9D62 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         53         26         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           52         24         57               49
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           51         24         40               50
PUSUARIO                       491144B8E342DF3792C954E48390BB8C FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         50         24         28               49
GETCONTRASENA                  26F7EA199E06D1132251A99DE8DF2D17 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         49         24         14                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           48         22         57               45
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           47         22         40               46

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PUSUARIO                       68F6B45432F10D6E8992F06A0AE39EB3 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         46         22         28               45
VALIDAUSUARIO                  E0921AAB219A9939A6E172F360C4000D FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         45         22         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           44         20         38               43
GETUSUARIOACTUAL               23407D986441AB14C7033A9AE4BCCFD7 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         43         20         14                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           42         18         44               41
PUSUARIO                       5DDE23E989CD226CC6517D90398D65CC FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         41         18         32               40
SETUSUARIOACTUAL               6E0268A2743664A38A14ECC8911D6468 PROCEDURE          PAQ_PERSONA                    PACKAGE       DECLARATION         40         18         15                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE       REFERENCE           39         16         67               38
PID_PERSONA                    B8AD1DB8FE7ED465222D1A564EAAFB03 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         38         16         52               35
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           37         16         42               36
PDIRECCION                     C35DB4A1F1319993CD23666A4056E49F FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         36         16         28               35

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
AGREGACORREO                   0D8AE40370F7C36DF16002844C0B4481 PROCEDURE          PAQ_PERSONA                    PACKAGE       DECLARATION         35         16         15                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE       REFERENCE           34         14         93               33
PID_PERSONA                    FC862613251FF48CE1C8CFB4FF8C62D6 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         33         14         78               28
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           32         14         68               31
PCONTRASENA                    06E805B44F60EC941898C2AB2685C228 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         31         14         53               28
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           30         14         44               29
PUSUARIO                       D395D4331E95A471502C2A40C0B9C208 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         29         14         32               28
AGREGACREDENCIAL               0AD5F2316263D734757101A5B8164EB2 PROCEDURE          PAQ_PERSONA                    PACKAGE       DECLARATION         28         14         15                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           27         12         58               24
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           26         12         41               25
PDIRECCION                     C5F656587AF73ADB9E5D8D582638FE50 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         25         12         27               24

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VALIDACORREO                   49CE30E62EB7DCD65BD4FD7A0EB84404 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         24         12         14                1
BOOLEAN                        EE1C5F13825B7DF0BF06D82DE633992E BOOLEAN DATATYPE   PAQ_PERSONA                    PACKAGE       REFERENCE           23         10         60               20
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           22         10         43               21
PUSUARIO                       0BDED757EA3287EB289E56562388B2EB FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         21         10         31               20
VALIDACREDENCIAL               B404BCF99CCB9B38D530DAC2CFDC08BB FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION         20         10         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE       REFERENCE           19          8         28                6
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE       REFERENCE           18          7         95               17
ID_ROL_IN                      9900DADFCC6FDA59D1662647A8242A98 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         17          7         85                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           16          7         76               15
PROFESION_IN                   F880D2E65EE71D9115AAA4DE68F4A918 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         15          7         63                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           14          7         54               13

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
FECHA_NACIMIENTO_IN            05E508A870F659A8B87DDBBFB6DCFF3F FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         13          7         34                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           12          7         24               11
SEGUNDOAPELLIDO_IN             115C752CAB7EFC1C16EF0445A90D8EA7 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION         11          7          5                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE           10          6         66                9
PRIMERAPELLIDO_IN              3A1057A3359545682D0DD32E5082DB35 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION          9          6         48                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE            8          6         39                7
NOMBRE_IN                      0A7B1A91BDE2F2B7E5D4E251367EB488 FORMAL IN          PAQ_PERSONA                    PACKAGE       DECLARATION          7          6         29                6
AGREGARPERSONA                 7DA8B75D9B6D9E69DD7864F31EE0DE77 FUNCTION           PAQ_PERSONA                    PACKAGE       DECLARATION          6          6         14                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE       REFERENCE            5          4         25                4
ID_PERSONACONECTADA            2E1590D7E0ECD8669BC2C52DA9C786C0 VARIABLE           PAQ_PERSONA                    PACKAGE       DECLARATION          4          4          5                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE       REFERENCE            3          3         23                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
USUARIO_CONECTADO              C58AEEC553D0DE5A54B9F63CD591AAA6 VARIABLE           PAQ_PERSONA                    PACKAGE       DECLARATION          2          3          5                1
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE       DECLARATION          1          1          9                0
USER                           473C1C744346FBECE3221D23961D48B2 FUNCTION           BEFOREUPDATEAVISTAMIENTO       TRIGGER       CALL                 3          6         37                2
TEMP_USER                      5733F1CA1F15966E1BB0023AE638947E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  DECLARATION        110        105          9              102
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_PERSONA                    PACKAGE BODY  REFERENCE          135        133         40              134
GETIDPERSONAACTUAL             70C7EE462C5915AF8D822BB3BBA84896 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  DEFINITION         134        133         14                1
ID_PERSONACONECTADA            2E1590D7E0ECD8669BC2C52DA9C786C0 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         133        127         18              129
PUSUARIO                       6673B745AAF2D4F1B3682334FCB5312F FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          132        129         29              129
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          131        124         46              130
PUSUARIO                       6673B745AAF2D4F1B3682334FCB5312F FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  DECLARATION        130        124         34              129
SETIDPERSONAACTUAL             B7F341C767789A1F821E380A980C72D5 PROCEDURE          PAQ_PERSONA                    PACKAGE BODY  DEFINITION         129        124         15                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
TEMP_CONTRA_DESPUES            7112CE1B0797B981B8F26D61EC95DBEE VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          128        121         42              102
TEMP_CONTRA_ANTES              E6CACD8330F47217CFBE240DE71411E5 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          127        121         21              102
DECRYPT                        919F064CD87E01FD4470CC244F8B7A01 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               126        116         28              125
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          125        116         20              124
TEMP_CONTRA_DESPUES            7112CE1B0797B981B8F26D61EC95DBEE VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         124        117         18              102
TEMP_USER                      5733F1CA1F15966E1BB0023AE638947E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          123        119         29              102
PCONTRASENA                    FCEDFCCAE4AE57C45CB06BA7D5DA3574 FORMAL IN          PAQ_PERSONA                    PACKAGE BODY  REFERENCE          122        113         46              121
ENCRYPT                        61ECD8FF3EA8962888A0BBB5DE4D7C50 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               121        113         38              120
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          120        113         30              102
TEMP_USER                      5733F1CA1F15966E1BB0023AE638947E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          119        114         29              102
DECRYPT                        919F064CD87E01FD4470CC244F8B7A01 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               118        107         28              117

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DBCRYPT                        9D7912BBE45A71A37D245558329E3C88 PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          117        107         20              116
TEMP_CONTRA_ANTES              E6CACD8330F47217CFBE240DE71411E5 VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         116        108         18              102
TEMP_USER                      5733F1CA1F15966E1BB0023AE638947E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  REFERENCE          115        110         29              102
GETUSUARIOACTUAL               23407D986441AB14C7033A9AE4BCCFD7 FUNCTION           PAQ_PERSONA                    PACKAGE BODY  CALL               114        105         48              113
PAQ_PERSONA                    FAF3D2CC24752F8E35F0197FBBE26FCE PACKAGE            PAQ_PERSONA                    PACKAGE BODY  REFERENCE          113        105         36              112
TEMP_USER                      5733F1CA1F15966E1BB0023AE638947E VARIABLE           PAQ_PERSONA                    PACKAGE BODY  ASSIGNMENT         112        105          9              110
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_PERSONA                    PACKAGE BODY  REFERENCE          111        105         19              110
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           BEFOREUPDATEAVISTAMIENTO       TRIGGER       CALL                 4          7         33                2
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL                 9          4         42                8
LISTA_NOMBRES                  01D7B6E9D1C9328265AB53A90BDD9011 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT           8          4          8                6
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE            7          4         23                6

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  01D7B6E9D1C9328265AB53A90BDD9011 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION          6          4          8                2
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE            5          2         58                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE            4          2         41                3
PCLASE                         A36E6E66D75A7CF526F2CDCA468DFBD0 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION          3          2         31                2
CONSULTA_X_CLASE               D1DC1BAC827C30DE53B569E30177BAE9 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION           2          2         14                1
PAQ_CONSULTA                   45EC846CDC6B08A003A5FA2EECAA3AE7 PACKAGE            PAQ_CONSULTA                   PACKAGE BODY  DEFINITION           1          1         14                0
LISTA_NOMBRES                  C28C18A1AF56C527F20FB4D437D09DCC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          216        134         16              201
DATO                           99F143675E15630B65F249FF87C63C8E ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          214        132         65              213
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               213        132         56              210
LISTA_NOMBRES                  C28C18A1AF56C527F20FB4D437D09DCC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          212        132         31              211
MUESTRAREPORTE                 F6C2045309F48260C3D10C7C3ACB5170 PROCEDURE          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         76         26         15                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           75         25         61               74
PID_REPORTE                    729DFE5C5F9961A5E7C3BF6632570D5C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         74         25         49               71
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           73         25         41               72
PID_PERSONA                    7287EE44E7E3E7D092B29185B97DE7BC FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         72         25         29               71
GENERAREPORTE                  CE2B832D90C00CC2BC50D723C455B0B5 PROCEDURE          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         71         25         15                1
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           70         23         53               55
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           69         23         38               68
PID_ZONAAVISTAMIENTO           26BCFA278844616B57D2A9E18559CECA FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         68         23         17               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           67         22         63               66
PESPECIE                       4769DAE39446A85592527D44E36B33FA FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         66         22         54               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           65         22         44               64

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PGENERO                        C0C9C2063431A02BD620E4D7B211D2E5 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         64         22         36               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           63         22         26               62
PFAMILIA                       67D210587162A9C207DF290630862C0B FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         62         22         17               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           61         21         74               60
PSUBORDEN                      76A944258C0B6CD20AB9AD6FC62D7DF3 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         60         21         64               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           59         21         54               58
PORDEN                         39B06BF75E06B77DA024503D5D9169C2 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         58         21         47               55
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           57         21         38               56
PCLASE                         159F89D25366405676BAA72A88F04E80 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         56         21         31               55
CREAAVISTAMIENTO               093D77CB72605CEEB578CF2325EE4E8C FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         55         21         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           54         19         54               51

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           53         19         39               52
PID                            0ADF4EAAA6B5E49D6F11FDC1EF44A88A FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         52         19         35               51
GET_ZONAAVISTAMIENTO           D3E13F3B5B465237F14700F2653C2474 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         51         19         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           50         18         46               47
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           49         18         31               48
PID                            D899DE2912A9E57D0F8DC44D67E3152D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         48         18         27               47
GET_DISTRITO                   B4EE0A9989C06D6B1FE210B7C574B393 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         47         18         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           46         17         44               43
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           45         17         29               44
PID                            72C46CC607A4958D9EC6B8E4008F672E FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         44         17         25               43
GET_CANTON                     0D622ED7CAB06D67370C675B16233C83 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         43         17         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           42         16         47               39
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           41         16         32               40
PID                            44BB268F52EE24783D1423D272F29E60 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         40         16         28               39
GET_PROVINCIA                  954D4B6BA009B4DFAF30C816B4A21F39 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         39         16         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           38         15         30               37
GET_PAIS                       AF973648EF2E567B1E3D88AD7AFF36BF FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         37         15         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           36         13         48               33
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           35         13         33               34
PID                            6FC6D7EC87D7E9426E104B2F593E3E14 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         34         13         26               33
GET_TAMANNO                    001E76CBC10D332294DFB5E59FDB8282 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         33         13         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           32         12         46               29

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           31         12         31               30
PID                            DF1CE35682EFAB7816BB20AE07F50F33 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         30         12         24               29
GET_COLOR                      9EE3BDD1AD6AEEADF1ED5C2CC364C73B FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         29         12         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           28         11         48               25
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           27         11         33               26
PID                            4EABE9E6626C858148D8F4D835F34374 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         26         11         26               25
GET_ESPECIE                    19AEF4116A936A99CA65799EF4B8A940 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         25         11         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           24         10         47               21
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           23         10         32               22
PID                            E40B2C020877AE0BB2DB2B1D35DC6BAD FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         22         10         25               21
GET_GENERO                     76B557CE14BF71B910F54CEC52B6D9B4 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         21         10         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           20          9         48               17
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           19          9         33               18
PID                            0C10B96685A4DE8501F3CCAC20994E32 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         18          9         26               17
GET_FAMILIA                    CB991677461CDBA7E60739755881AC54 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         17          9         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           16          8         49               13
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           15          8         34               14
PID                            2AE898085F4A8A334DDF62B97474E52E FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         14          8         27               13
GET_SUBORDEN                   6D672BC3553FB838E47B22A96740F1BF FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         13          8         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           12          7         43                9
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE           11          7         28               10
PID                            26E369753F05821C85666844E81A2FE7 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE       DECLARATION         10          7         24                9

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GET_ORDEN                      5F6E0C9886B0F30B4F7782E70A100552 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION          9          7         14                1
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE       REFERENCE            8          6         31                7
GET_CLASE                      D7F6086DEC469B4EAB4719652F7FF622 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION          7          6         14                1
CHAR                           827DC51E93B66D1B683E1965CB2103FD SUBTYPE            PAQ_AVISTAMIENTO               PACKAGE       REFERENCE            6          4         20                5
EXTINCIONCHECK                 87286C0F508A535C8AA34298C2405601 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION          5          4          5                1
AVISTAMIENTOGLOBAL             1B58AD2C70F1D8361A34E58AE76AE181 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE       ASSIGNMENT           4          3          5                2
LISTA_NOMBRES                  E93A977B08237775403C3F1EE716393C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          247        153         17              246
PID                            5D33405B896A1CC3A3D487757B2B41BD FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          245        152         70              244
DATO                           472DC20FCCF5660594513DB1A6B8CE75 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        244        152         14              236
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               243        150         37              242
LISTA_NOMBRES                  E93A977B08237775403C3F1EE716393C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         242        150          8              240

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          241        150         23              240
LISTA_NOMBRES                  E93A977B08237775403C3F1EE716393C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        240        150          8              236
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          239        148         46              236
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          238        148         31              237
PID                            5D33405B896A1CC3A3D487757B2B41BD FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        237        148         24              236
GET_CANTON                     0D622ED7CAB06D67370C675B16233C83 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         236        148         13                1
LISTA_NOMBRES                  15EA9A14E2241FEA45E4B808FCC01087 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          235        145         16              217
DATO                           0A7A04C7B40D5C691D1AC8545B5CC1CA ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          233        143         65              232
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               232        143         56              229
LISTA_NOMBRES                  15EA9A14E2241FEA45E4B808FCC01087 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          231        143         31              230
LISTA_NOMBRES                  15EA9A14E2241FEA45E4B808FCC01087 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         229        143         17              225

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  15EA9A14E2241FEA45E4B808FCC01087 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          228        142         17              227
PID                            199278D8B9F1AD4584462FCD73A99A7D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          226        141         68              225
DATO                           0A7A04C7B40D5C691D1AC8545B5CC1CA ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        225        141         14              217
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               224        139         37              223
LISTA_NOMBRES                  15EA9A14E2241FEA45E4B808FCC01087 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         223        139          8              221
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          222        139         23              221
LISTA_NOMBRES                  15EA9A14E2241FEA45E4B808FCC01087 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        221        139          8              217
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          220        137         49              217
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          219        137         34              218
PID                            199278D8B9F1AD4584462FCD73A99A7D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        218        137         27              217
GET_PROVINCIA                  954D4B6BA009B4DFAF30C816B4A21F39 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         217        137         13                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           36         43         13                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           34         43         28                9
LISTA_NOMBRES                  C28C18A1AF56C527F20FB4D437D09DCC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         210        132         17              207
LISTA_NOMBRES                  C28C18A1AF56C527F20FB4D437D09DCC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          209        131         17              208
DATO                           99F143675E15630B65F249FF87C63C8E ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        207        130         14              201
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               206        128         37              205
LISTA_NOMBRES                  C28C18A1AF56C527F20FB4D437D09DCC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         205        128          8              203
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          204        128         23              203
LISTA_NOMBRES                  C28C18A1AF56C527F20FB4D437D09DCC VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        203        128          8              201
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          202        126         30              201
GET_PAIS                       AF973648EF2E567B1E3D88AD7AFF36BF FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         201        126         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  8749570956319395AE8DB480605C9AE1 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          200        123         16              182
DATO                           19173EDCBE562D24C8B0178710852F3F ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          198        121         65              197
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               197        121         56              194
LISTA_NOMBRES                  8749570956319395AE8DB480605C9AE1 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          196        121         31              195
LISTA_NOMBRES                  8749570956319395AE8DB480605C9AE1 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         194        121         17              190
LISTA_NOMBRES                  8749570956319395AE8DB480605C9AE1 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          193        120         17              192
PID                            390B30C5978EB849630FD33DC3C44A18 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          191        119         74              190
DATO                           19173EDCBE562D24C8B0178710852F3F ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        190        119         14              182
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               189        117         37              188
LISTA_NOMBRES                  8749570956319395AE8DB480605C9AE1 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         188        117          8              186
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          187        117         23              186

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  8749570956319395AE8DB480605C9AE1 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        186        117          8              182
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          185        115         47              182
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          184        115         32              183
PID                            390B30C5978EB849630FD33DC3C44A18 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        183        115         25              182
GET_TAMANNO                    001E76CBC10D332294DFB5E59FDB8282 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         182        115         13                1
LISTA_NOMBRES                  29323139A94C424C127F89A8BAE51DCA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          181        112         16              163
DATO                           46A3ECBED9854C1360FF2651814CC537 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          179        110         65              178
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               178        110         56              175
LISTA_NOMBRES                  29323139A94C424C127F89A8BAE51DCA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          177        110         31              176
LISTA_NOMBRES                  29323139A94C424C127F89A8BAE51DCA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         175        110         17              171
LISTA_NOMBRES                  29323139A94C424C127F89A8BAE51DCA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          174        109         17              173

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID                            0D63F54A8768B42ABA3533820E10A35D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          172        108         72              171
DATO                           46A3ECBED9854C1360FF2651814CC537 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        171        108         14              163
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               170        106         37              169
LISTA_NOMBRES                  29323139A94C424C127F89A8BAE51DCA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         169        106          8              167
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          168        106         23              167
LISTA_NOMBRES                  29323139A94C424C127F89A8BAE51DCA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        167        106          8              163
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          166        104         45              163
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          165        104         30              164
PID                            0D63F54A8768B42ABA3533820E10A35D FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        164        104         23              163
GET_COLOR                      9EE3BDD1AD6AEEADF1ED5C2CC364C73B FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         163        104         13                1
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          162        101         16              134

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           1E69976960ECF4ABDEF8E80275018953 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          160         98         65              159
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               159         98         56              156
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          158         98         31              157
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         156         98         17              153
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          155         97         17              154
DATO                           1E69976960ECF4ABDEF8E80275018953 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        153         96         17              134
DATO                           21BAA965227014A3C695F68B66788969 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          151         93         65              150
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               150         93         56              147
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          149         93         31              148
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         147         93         17              143
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          146         92         17              145

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PID                            052D3A27E0246E1BCB5EBA80EDBD1C17 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          144         91         71              143
DATO                           21BAA965227014A3C695F68B66788969 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        143         91         17              134
PID                            052D3A27E0246E1BCB5EBA80EDBD1C17 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          142         90         12              134
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               141         88         37              140
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         140         88          8              138
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          139         88         23              138
LISTA_NOMBRES                  18548B9A1DBCEEFAC5D4D623F923FE67 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        138         88          8              134
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          137         86         47              134
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          136         86         32              135
PID                            052D3A27E0246E1BCB5EBA80EDBD1C17 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        135         86         25              134
GET_ESPECIE                    19AEF4116A936A99CA65799EF4B8A940 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         134         86         13                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          133         83         16              105
DATO                           FE00F8E7AF22C6B61BD6E73F27B74B13 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          131         80         65              130
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               130         80         56              127
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          129         80         31              128
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         127         80         17              124
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          126         79         17              125
DATO                           FE00F8E7AF22C6B61BD6E73F27B74B13 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        124         78         17              105
DATO                           27E49F46288FB9CC7188A88CFA150674 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          122         75         65              121
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               121         75         56              118
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          120         75         31              119
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         118         75         17              114

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          117         74         17              116
PID                            B870DCCA03427EC96E29DC31D8793FA6 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          115         73         71              114
DATO                           27E49F46288FB9CC7188A88CFA150674 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        114         73         17              105
PID                            B870DCCA03427EC96E29DC31D8793FA6 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          113         72         13              105
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               112         70         37              111
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT         111         70          8              109
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          110         70         23              109
LISTA_NOMBRES                  01CCD06E010D168269AB82D3C96E7D06 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        109         70          8              105
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          108         68         46              105
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          107         68         31              106
PID                            B870DCCA03427EC96E29DC31D8793FA6 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION        106         68         24              105

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
GET_GENERO                     76B557CE14BF71B910F54CEC52B6D9B4 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION         105         68         13                1
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          104         65         16               76
DATO                           1EF156ADD911829349C515AE8700312A ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          102         62         65              101
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL               101         62         56               98
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE          100         62         31               99
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           14         47         13                9
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE       REFERENCE            3          3         24                2
AVISTAMIENTOGLOBAL             1B58AD2C70F1D8361A34E58AE76AE181 VARIABLE           PAQ_AVISTAMIENTO               PACKAGE       DECLARATION          2          3          5                1
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            PAQ_AVISTAMIENTO               PACKAGE       DECLARATION          1          1          9                0
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          98         62         17               95
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           97         61         17               96

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           1EF156ADD911829349C515AE8700312A ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         95         60         17               76
DATO                           4E2AE389E7091ACE47813D2C15E3B4C2 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           93         57         65               92
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                92         57         56               89
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           91         57         31               90
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          89         57         17               85
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           88         56         17               87
PID                            1DA2116812F47CE14A189F38DF3F8EA0 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           86         55         73               85
DATO                           4E2AE389E7091ACE47813D2C15E3B4C2 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         85         55         17               76
PID                            1DA2116812F47CE14A189F38DF3F8EA0 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           84         54         13               76
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                83         52         37               82
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          82         52          8               80

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           81         52         23               80
LISTA_NOMBRES                  F9A510E47DA697EEE1C28D79E3051F4D VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         80         52          8               76
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           79         50         47               76
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           78         50         32               77
PID                            1DA2116812F47CE14A189F38DF3F8EA0 FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         77         50         25               76
GET_FAMILIA                    CB991677461CDBA7E60739755881AC54 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION          76         50         13                1
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           75         47         16               47
DATO                           678793C416BCC16F9FD220A77C236345 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           73         44         65               72
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                72         44         56               69
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           71         44         31               70
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          69         44         17               66

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           68         43         17               67
DATO                           678793C416BCC16F9FD220A77C236345 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         66         42         17               47
DATO                           C83C3C7338EE17F83F8C14D26B2D7E24 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           64         39         65               63
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                63         39         56               60
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           62         39         31               61
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          60         39         17               56
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           59         38         17               58
PID                            9A05947AF2DADC15B0623E5A7E4A80AF FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           57         37         72               56
DATO                           C83C3C7338EE17F83F8C14D26B2D7E24 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         56         37         17               47
PID                            9A05947AF2DADC15B0623E5A7E4A80AF FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           55         36         13               47
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                54         34         37               53

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          53         34          8               51
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           52         34         23               51
LISTA_NOMBRES                  1928240CE7CB5ED90965CB584D27E88C VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         51         34          8               47
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           50         32         48               47
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           49         32         33               48
PID                            9A05947AF2DADC15B0623E5A7E4A80AF FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         48         32         26               47
GET_SUBORDEN                   6D672BC3553FB838E47B22A96740F1BF FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION          47         32         13                1
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           46         29         16               18
DATO                           AB688630637B9A05D610540E239AD927 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           44         26         65               43
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                43         26         56               40
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           42         26         31               41

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          40         26         17               37
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           39         25         17               38
DATO                           AB688630637B9A05D610540E239AD927 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         37         24         17               18
DATO                           91B75127C6A7A48229DC0017D5B9644C ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           35         21         65               34
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                34         21         56               31
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           33         21         31               32
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          31         21         17               27
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           30         20         17               29
PID                            FCBE8CC971DD50AFBFF386D66791517C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           28         19         68               27
DATO                           91B75127C6A7A48229DC0017D5B9644C ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         27         19         17               18
PID                            FCBE8CC971DD50AFBFF386D66791517C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           26         18         13               18

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                25         16         37               24
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          24         16          8               22
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           23         16         23               22
LISTA_NOMBRES                  F1FE06CD8DE441D9914699643609215E VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         22         16          8               18
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           21         14         45               18
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           20         14         30               19
PID                            FCBE8CC971DD50AFBFF386D66791517C FORMAL IN          PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION         19         14         23               18
GET_ORDEN                      5F6E0C9886B0F30B4F7782E70A100552 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION          18         14         13                1
LISTA_NOMBRES                  A3D742C27297FCFBA7572DD3B2754CEA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           17         11         16                2
DATO                           9C28FC139A3D505F26F686F0C2131C88 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           15          9         65               14
T_NOMBRE                       ECAFC93F13C637B5B310001B3D291D22 OBJECT             PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                14          9         56               11

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  A3D742C27297FCFBA7572DD3B2754CEA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           13          9         31               12
LISTA_NOMBRES                  A3D742C27297FCFBA7572DD3B2754CEA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT          11          9         17                8
LISTA_NOMBRES                  A3D742C27297FCFBA7572DD3B2754CEA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE           10          8         17                9
DATO                           9C28FC139A3D505F26F686F0C2131C88 ITERATOR           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION          8          7         14                2
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  CALL                 7          5         37                6
LISTA_NOMBRES                  A3D742C27297FCFBA7572DD3B2754CEA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  ASSIGNMENT           6          5          8                4
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE            5          5         23                4
LISTA_NOMBRES                  A3D742C27297FCFBA7572DD3B2754CEA VARIABLE           PAQ_AVISTAMIENTO               PACKAGE BODY  DECLARATION          4          5          8                2
RECORD_NOM                     5E294AFA52F767D7548B1900201C408C NESTED TABLE       PAQ_AVISTAMIENTO               PACKAGE BODY  REFERENCE            3          3         31                2
GET_CLASE                      D7F6086DEC469B4EAB4719652F7FF622 FUNCTION           PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION           2          3         14                1
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            PAQ_AVISTAMIENTO               PACKAGE BODY  DEFINITION           1          1         14                0

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           32         44         13                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           30         44         25                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           28         44         39                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           26         45         13                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           24         45         28                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           22         45         40                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           20         46         13                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           18         46         24                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           16         46         36                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           12         47         35                9
AVISTAMIENTOGLOBAL             1B58AD2C70F1D8361A34E58AE76AE181 VARIABLE           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           11         37         54               10

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            INSERTESPECIEEXTINCION         TRIGGER       REFERENCE           10         37         37                9
DATO                           5096F0D798EEDB58AF5017AB975F63E0 ITERATOR           INSERTESPECIEEXTINCION         TRIGGER       DECLARATION          9         13         13                2
AVISTAMIENTOGLOBAL             1B58AD2C70F1D8361A34E58AE76AE181 VARIABLE           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE            8         12         26                7
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            INSERTESPECIEEXTINCION         TRIGGER       REFERENCE            7         12          9                2
EXTINCIONCHECK                 87286C0F508A535C8AA34298C2405601 VARIABLE           INSERTESPECIEEXTINCION         TRIGGER       ASSIGNMENT           6          7         27                5
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            INSERTESPECIEEXTINCION         TRIGGER       REFERENCE            5          7         10                2
AVISTAMIENTOGLOBAL             1B58AD2C70F1D8361A34E58AE76AE181 VARIABLE           INSERTESPECIEEXTINCION         TRIGGER       REFERENCE            4         10         44                3
PAQ_AVISTAMIENTO               4858F7D285880FE85A9B19658888C306 PACKAGE            INSERTESPECIEEXTINCION         TRIGGER       REFERENCE            3         10         27                2
INSERTESPECIEEXTINCION         1CA13228E6FFDC4E689BE57A7C09D8FB TRIGGER            INSERTESPECIEEXTINCION         TRIGGER       DEFINITION           2          1         23                1
INSERTESPECIEEXTINCION         1CA13228E6FFDC4E689BE57A7C09D8FB TRIGGER            INSERTESPECIEEXTINCION         TRIGGER       DECLARATION          1          1         23                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           31          6         69               30

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
CORREO                         CC78E93D48D195B562C1B125EA1A8F24 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         30          6         62                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           29          6         48               28
SEGUNDO_APELLIDO               66699A333ED573D9B8243B73E92C8446 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         28          6         31                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           27          6         17               26
PRIMER_APELLIDO                4A9F5A47F89138E8A4127F34643CBF4F VARIABLE           T_EXTINCION                    TYPE          DECLARATION         26          6          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           25          5         50               24
NOMBRE                         BDF1D83353CE9AA66FFF55509082F8B7 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         24          5         43                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           23          5         29               22
ZONA                           854BB3AFEE97954A3638CB6598E5924B VARIABLE           T_EXTINCION                    TYPE          DECLARATION         22          5         24                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           21          5         10               20
DISTRITO                       DEA772AB297104560E4DF36C80844A57 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         20          5          1                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           19          4         51               18
CANTON                         7261B370DA844CBEB828F76A5756C941 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         18          4         44                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           17          4         30               16
PROVINCIA                      81C06AFB6B6432FB18556E6DD643AADA VARIABLE           T_EXTINCION                    TYPE          DECLARATION         16          4         20                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           15          4          6               14
PAIS                           C41D9E005634A6AE3026403A5C5681A4 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         14          4          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           13          3         52               12
ESPECIE                        39FB4CF47B3129F78B8056CB683B1E48 VARIABLE           T_EXTINCION                    TYPE          DECLARATION         12          3         44                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE           11          3         30               10
GENERO                         FFA50901F196A8E0FDC261A559B7EF1E VARIABLE           T_EXTINCION                    TYPE          DECLARATION         10          3         23                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE            9          3          9                8

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
FAMILIA                        475639F4E4BFF84A7E7007841BE9D071 VARIABLE           T_EXTINCION                    TYPE          DECLARATION          8          3          1                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE            7          2         50                6
SUBORDEN                       7E5539501990B1E920A24E8624A2D905 VARIABLE           T_EXTINCION                    TYPE          DECLARATION          6          2         41                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE            5          2         27                4
ORDEN                          052B4F9A0CA46AA999E0E8350E55FAF3 VARIABLE           T_EXTINCION                    TYPE          DECLARATION          4          2         21                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_EXTINCION                    TYPE          REFERENCE            3          2          7                2
CLASE                          AA32C604DB180C7F447A88EC2E6F6D6C VARIABLE           T_EXTINCION                    TYPE          DECLARATION          2          2          1                1
T_EXTINCION                    F3FA21DEBA66E0EBE8E614EB67796004 OBJECT             T_EXTINCION                    TYPE          DECLARATION          1          1          6                0
T_EXTINCION                    F3FA21DEBA66E0EBE8E614EB67796004 OBJECT             RECORD_EXTINCION               TYPE          REFERENCE            2          1         35                1
RECORD_EXTINCION               BF0C8FFD32648F0FB0FDE76FA6D4D915 NESTED TABLE       RECORD_EXTINCION               TYPE          DECLARATION          1          1          6                0
RECORD_EXTINCION               BF0C8FFD32648F0FB0FDE76FA6D4D915 NESTED TABLE       PAQ_REPORTE                    PACKAGE       REFERENCE            6          4         40                5

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
CONSULTA_EXTINCION             4D9670A80588B91458CAD54FBF521D4F FUNCTION           PAQ_REPORTE                    PACKAGE       DECLARATION          5          4         14                1
RECORD_CUMPLEANNOS             25B7C654C7A65EAF4F80BE715E28F13B NESTED TABLE       PAQ_REPORTE                    PACKAGE       REFERENCE            4          3         41                3
CONSULTA_CUMPLEANOS            034CF12C5C5C4C88591D923BDE2D20DF FUNCTION           PAQ_REPORTE                    PACKAGE       DECLARATION          3          3         14                1
REPORTE_CUMPLEANNOS            833B948557CD54A73CDC65FB128CCCE9 PROCEDURE          PAQ_REPORTE                    PACKAGE       DECLARATION          2          2         15                1
PAQ_REPORTE                    A731ACAA383B60ED7DEB2079522C7BEE PACKAGE            PAQ_REPORTE                    PACKAGE       DECLARATION          1          1          9                0
LISTA_NOMBRES                  D9C2CF83943B0A83EAD1930A77474B11 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           92         61         16               49
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           90         59         74               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           88         59         51               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           86         59         29               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           84         59         17               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           82         58         68               61

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           80         58         54               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           78         58         42               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           76         58         27               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           74         58         17               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           72         57         68               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           70         57         56               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           68         57         43               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           66         57         28               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           64         57         17               61
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           62         56         68               61
T_EXTINCION                    F3FA21DEBA66E0EBE8E614EB67796004 OBJECT             PAQ_REPORTE                    PACKAGE BODY  CALL                61         56         56               58

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  D9C2CF83943B0A83EAD1930A77474B11 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           60         56         31               59
LISTA_NOMBRES                  D9C2CF83943B0A83EAD1930A77474B11 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  ASSIGNMENT          58         56         17               55
LISTA_NOMBRES                  D9C2CF83943B0A83EAD1930A77474B11 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           57         55         17               56
DATO                           D38330E17D1CC051F568E7F7BC2001AD ITERATOR           PAQ_REPORTE                    PACKAGE BODY  DECLARATION         55         49         17               49
RECORD_EXTINCION               BF0C8FFD32648F0FB0FDE76FA6D4D915 NESTED TABLE       PAQ_REPORTE                    PACKAGE BODY  CALL                54         47         43               53
LISTA_NOMBRES                  D9C2CF83943B0A83EAD1930A77474B11 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  ASSIGNMENT          53         47          8               51
RECORD_EXTINCION               BF0C8FFD32648F0FB0FDE76FA6D4D915 NESTED TABLE       PAQ_REPORTE                    PACKAGE BODY  REFERENCE           52         47         23               51
LISTA_NOMBRES                  D9C2CF83943B0A83EAD1930A77474B11 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  DECLARATION         51         47          8               49
RECORD_EXTINCION               BF0C8FFD32648F0FB0FDE76FA6D4D915 NESTED TABLE       PAQ_REPORTE                    PACKAGE BODY  REFERENCE           50         45         40               49
CONSULTA_EXTINCION             4D9670A80588B91458CAD54FBF521D4F FUNCTION           PAQ_REPORTE                    PACKAGE BODY  DEFINITION          49         45         14                1
LISTA_NOMBRES                  A48F9A96CB79E0672FA9B412F7295298 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           48         42         16               25

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           5AEA0A2AD10DB9B657BAF7615EBDD0B9 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           46         40         38               37
DATO                           5AEA0A2AD10DB9B657BAF7615EBDD0B9 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           44         40         17               37
DATO                           5AEA0A2AD10DB9B657BAF7615EBDD0B9 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           42         39         38               37
DATO                           5AEA0A2AD10DB9B657BAF7615EBDD0B9 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           40         39         17               37
DATO                           5AEA0A2AD10DB9B657BAF7615EBDD0B9 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           38         38         70               37
T_CUMPLEANNOS                  BBB285D1C11DD2886F84E91876105684 OBJECT             PAQ_REPORTE                    PACKAGE BODY  CALL                37         38         56               34
LISTA_NOMBRES                  A48F9A96CB79E0672FA9B412F7295298 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           36         38         31               35
LISTA_NOMBRES                  A48F9A96CB79E0672FA9B412F7295298 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  ASSIGNMENT          34         38         17               31
LISTA_NOMBRES                  A48F9A96CB79E0672FA9B412F7295298 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           33         37         17               32
DATO                           5AEA0A2AD10DB9B657BAF7615EBDD0B9 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  DECLARATION         31         32         17               25
RECORD_CUMPLEANNOS             25B7C654C7A65EAF4F80BE715E28F13B NESTED TABLE       PAQ_REPORTE                    PACKAGE BODY  CALL                30         30         45               29

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  A48F9A96CB79E0672FA9B412F7295298 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  ASSIGNMENT          29         30          8               27
RECORD_CUMPLEANNOS             25B7C654C7A65EAF4F80BE715E28F13B NESTED TABLE       PAQ_REPORTE                    PACKAGE BODY  REFERENCE           28         30         23               27
LISTA_NOMBRES                  A48F9A96CB79E0672FA9B412F7295298 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  DECLARATION         27         30          8               25
RECORD_CUMPLEANNOS             25B7C654C7A65EAF4F80BE715E28F13B NESTED TABLE       PAQ_REPORTE                    PACKAGE BODY  REFERENCE           26         28         41               25
CONSULTA_CUMPLEANOS            034CF12C5C5C4C88591D923BDE2D20DF FUNCTION           PAQ_REPORTE                    PACKAGE BODY  DEFINITION          25         28         14                1
CONT_CUMPLEANNOS               56A54AFD63E0AE0DA17277D9967020DD VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           24         23         21               11
DATO                           A48C244FF1B9D8DEF3694B44EF573AC1 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           22         23         38               11
DATO                           A48C244FF1B9D8DEF3694B44EF573AC1 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           20         23         50               11
DATO                           A48C244FF1B9D8DEF3694B44EF573AC1 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           18         24         13               11
DATO                           A48C244FF1B9D8DEF3694B44EF573AC1 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           16         24         35               11
DATO                           A48C244FF1B9D8DEF3694B44EF573AC1 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           14         24         48               11

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
CONT_CUMPLEANNOS               56A54AFD63E0AE0DA17277D9967020DD VARIABLE           PAQ_REPORTE                    PACKAGE BODY  ASSIGNMENT          13         18         18               11
FECHA_ACTUAL                   AD7ABEDAB883A6C629B7173A015349B1 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  REFERENCE           12         15         60               11
DATO                           A48C244FF1B9D8DEF3694B44EF573AC1 ITERATOR           PAQ_REPORTE                    PACKAGE BODY  DECLARATION         11         10         13                2
FECHA_ACTUAL                   AD7ABEDAB883A6C629B7173A015349B1 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  ASSIGNMENT          10          8         14                2
SYSDATE                        E5BBBF7372D1237A76C0E590F1B60C68 FUNCTION           PAQ_REPORTE                    PACKAGE BODY  CALL                 9          7         24                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_REPORTE                    PACKAGE BODY  REFERENCE            8          5         27                7
FECHA_CUMPLEANNOS              D699B3BA8C124C2FDC36D446F1A737F3 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  DECLARATION          7          5          9                2
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_REPORTE                    PACKAGE BODY  REFERENCE            6          4         22                5
FECHA_ACTUAL                   AD7ABEDAB883A6C629B7173A015349B1 VARIABLE           PAQ_REPORTE                    PACKAGE BODY  DECLARATION          5          4          9                2
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    PAQ_REPORTE                    PACKAGE BODY  REFERENCE            4          3         26                3
CONT_CUMPLEANNOS               56A54AFD63E0AE0DA17277D9967020DD VARIABLE           PAQ_REPORTE                    PACKAGE BODY  DECLARATION          3          3          9                2

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
REPORTE_CUMPLEANNOS            833B948557CD54A73CDC65FB128CCCE9 PROCEDURE          PAQ_REPORTE                    PACKAGE BODY  DEFINITION           2          2         15                1
PAQ_REPORTE                    A731ACAA383B60ED7DEB2079522C7BEE PACKAGE            PAQ_REPORTE                    PACKAGE BODY  DEFINITION           1          1         14                0
RECORD_TOPREPORTES             6B77687119C14EBCEEB9B5113C128E6B NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           47         15         42               46
CONSULTA_TOPPERSONAS           A131E93D0D8D1EDEFE39E830C0E30F29 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         46         15         14                1
RECORD_AVISTAMIENTO            EFA462166569ADCA9A6367E4C3CB12B7 NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           45         13          5               34
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           44         12         58               43
PZONA                          41D4409FC39CF810BA501B2BD30022D4 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         43         12         49               34
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           42         12         39               41
PDISTRITO                      BB81B359FDF24BDB5D0C94D0EF911D39 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         41         12         26               34
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           40         12         16               39
PCANTON                        39EF4CE705B8EFDDB77CF909217EF512 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         39         12          5               34

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           38         11         66               37
PPROVINCIA                     02BCBAE16399D090A7169ADC7288208E FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         37         11         52               34
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           36         11         43               35
PPAIS                          ACEEC8DAF0C4DF359062E39D17108978 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         35         11         34               34
CONSULTA_X_ZONAVIDA            133313E0A6B9AD4A0E0BB3A93BBD4046 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         34         11         14                1
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           33          9         58               30
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           32          9         41               31
PCOLOR                         5377910EC3F5CC501181066FB369449B FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         31          9         31               30
CONSULTA_X_COLOR               09A41982B6056CC4B311C16DA935B1EB FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         30          9         14                1
DATO                           F9BE12C2F6F03278000B8CFC6B099760 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          310        300         73              301
DATO                           F9BE12C2F6F03278000B8CFC6B099760 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          308        300         59              301

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           F9BE12C2F6F03278000B8CFC6B099760 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          306        300         37              301
DATO                           F9BE12C2F6F03278000B8CFC6B099760 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          304        300         17              301
DATO                           F9BE12C2F6F03278000B8CFC6B099760 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          302        299         70              301
T_TOPREPORTES                  7EB7D9E36EA684891FB2C462E74A1DE4 OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               301        299         56              298
LISTA_NOMBRES                  4614F60BB784C7FDF6636F95597C404D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          300        299         31              299
LISTA_NOMBRES                  4614F60BB784C7FDF6636F95597C404D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         298        299         17              294
LISTA_NOMBRES                  4614F60BB784C7FDF6636F95597C404D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          297        298         17              296
ROWNUM                         65C4C98CB2DD1DA9D438AAEFCCC8BD58 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  CALL               295        296         62              294
DATO                           F9BE12C2F6F03278000B8CFC6B099760 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        294        286         13              288
RECORD_TOPREPORTES             6B77687119C14EBCEEB9B5113C128E6B NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL               293        284         41              292
LISTA_NOMBRES                  4614F60BB784C7FDF6636F95597C404D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         292        284          4              290

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_TOPREPORTES             6B77687119C14EBCEEB9B5113C128E6B NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          291        284         19              290
LISTA_NOMBRES                  4614F60BB784C7FDF6636F95597C404D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        290        284          4              288
RECORD_TOPREPORTES             6B77687119C14EBCEEB9B5113C128E6B NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          289        283         41              288
CONSULTA_TOPPERSONAS           A131E93D0D8D1EDEFE39E830C0E30F29 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION         288        283         13                1
LISTA_NOMBRES                  FA07AB2805761D1C4DFC32F3AB975135 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          287        280         16              238
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          285        278         68              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          283        278         54              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          281        278         42              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          279        278         27              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          277        278         17              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          275        277         68              264

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          273        277         55              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          271        277         42              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          269        277         28              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          267        277         17              264
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          265        276         71              264
T_AVISTAMIENTO                 FEC2B08FAD2A99EC12992045A04EF0CF OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               264        276         56              261
LISTA_NOMBRES                  FA07AB2805761D1C4DFC32F3AB975135 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          263        276         31              262
LISTA_NOMBRES                  FA07AB2805761D1C4DFC32F3AB975135 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         261        276         17              254
LISTA_NOMBRES                  FA07AB2805761D1C4DFC32F3AB975135 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          260        275         17              259
PPAIS                          6DB6EADDC03937B67ECA51C0E219B336 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          258        269         44              254
PPROVINCIA                     B90F6F7090B2CEDA6EB658D554D20F12 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          257        270         38              254

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PCANTON                        6ABC61BC7DD2716D5EEF256999828111 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          256        271         38              254
PDISTRITO                      5B782F4FAF5B56EFE6B82BE31CAE8A0B FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          255        272         37              254
DATO                           1D34963657919016141A0C0DDE1B0768 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        254        237         14              238
RECORD_AVISTAMIENTO            EFA462166569ADCA9A6367E4C3CB12B7 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL               253        235         43              252
LISTA_NOMBRES                  FA07AB2805761D1C4DFC32F3AB975135 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         252        235          5              250
RECORD_AVISTAMIENTO            EFA462166569ADCA9A6367E4C3CB12B7 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          251        235         20              250
LISTA_NOMBRES                  FA07AB2805761D1C4DFC32F3AB975135 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        250        235          5              238
RECORD_AVISTAMIENTO            EFA462166569ADCA9A6367E4C3CB12B7 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          249        233         75              238
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          248        233         58              247
PZONA                          0AC8C20AF14379E1B49FBB7B1891726E FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        247        233         49              238
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          246        233         39              245

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PDISTRITO                      5B782F4FAF5B56EFE6B82BE31CAE8A0B FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        245        233         26              238
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          244        233         16              243
PCANTON                        6ABC61BC7DD2716D5EEF256999828111 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        243        233          5              238
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          242        232         66              241
PPROVINCIA                     B90F6F7090B2CEDA6EB658D554D20F12 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        241        232         52              238
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          240        232         43              239
PPAIS                          6DB6EADDC03937B67ECA51C0E219B336 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        239        232         34              238
CONSULTA_X_ZONAVIDA            133313E0A6B9AD4A0E0BB3A93BBD4046 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION         238        232         14                1
LISTA_NOMBRES                  4C3F95C5F332FA9C35AFA3B7B617E5DB VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          237        229         16              207
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           29          8         61               26
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           28          8         44               27

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PTAMANO                        F5DFC393736226B5389F23E157812D69 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         27          8         33               26
CONSULTA_X_TAMANNO             D39C75627E96BD3FFD04E0145DD59815 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         26          8         14                1
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           25          7         61               22
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           24          7         45               23
PESPECIE                       8CE2A00B6DE11E8F59A437D337DA59FD FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         23          7         33               22
CONSULTA_X_ESPECIE             A8790EE93B1A4C3D66AE8A102B2D09BA FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         22          7         14                1
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           21          6         59               18
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           20          6         43               19
PGENERO                        1251AEDBC0E4E5C8C27222BF8F02FCB4 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         19          6         32               18
CONSULTA_X_GENERO              E753FBE5A745933D56C371E1AD0D5CBF FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         18          6         14                1
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           17          5         61               14

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           16          5         45               15
PFAMILIA                       AED4C4DB118F416ADF652A878CF6983F FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         15          5         33               14
CONSULTA_X_FAMILIA             B131AB080BFFC01133DC5EEF8E6C0197 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         14          5         14                1
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE           13          4         64               10
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE           12          4         47               11
PSUBORDEN                      9D97511E304459465FB323A026B92497 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION         11          4         34               10
CONSULTA_X_SUBORDEN            6174F73A2CEBF8ED156A72BD6B765B79 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION         10          4         14                1
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE            9          3         58                6
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE            8          3         41                7
PORDEN                         4E6A90C4F7F82C4AC13773751F3AF190 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION          7          3         31                6
CONSULTA_X_ORDEN               0D55F12DC4D6EE92263F8EF09E77E609 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION          6          3         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE       REFERENCE            5          2         58                2
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          235        227         17              222
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          233        226         68              222
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          231        226         55              222
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          229        226         42              222
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          227        226         28              222
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          225        226         17              222
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          223        225         78              222
T_CONSULTACOLORTAMANO          415968482F8C50C3C7F657CE2C0AEDDA OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               222        225         56              219
LISTA_NOMBRES                  4C3F95C5F332FA9C35AFA3B7B617E5DB VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          221        225         31              220
LISTA_NOMBRES                  4C3F95C5F332FA9C35AFA3B7B617E5DB VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         219        225         17              215

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  4C3F95C5F332FA9C35AFA3B7B617E5DB VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          218        224         17              217
PCOLOR                         D9611FCE2F152B1C2B1F177ECF1B3CC3 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          216        221         45              215
DATO                           439C07F26AA3C53F830053ADEA0C3213 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        215        205         14              207
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL               214        203         53              213
LISTA_NOMBRES                  4C3F95C5F332FA9C35AFA3B7B617E5DB VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         213        203          8              211
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          212        203         23              211
LISTA_NOMBRES                  4C3F95C5F332FA9C35AFA3B7B617E5DB VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        211        203          8              207
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          210        202         12              207
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          209        201         41              208
PCOLOR                         D9611FCE2F152B1C2B1F177ECF1B3CC3 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        208        201         31              207
CONSULTA_X_COLOR               09A41982B6056CC4B311C16DA935B1EB FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION         207        201         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  8E79793E4DC58074853111EBCA567918 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          206        198         16              176
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          204        196         17              191
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          202        195         68              191
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          200        195         55              191
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          198        195         42              191
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          196        195         28              191
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          194        195         17              191
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          192        194         78              191
T_CONSULTACOLORTAMANO          415968482F8C50C3C7F657CE2C0AEDDA OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               191        194         56              188
LISTA_NOMBRES                  8E79793E4DC58074853111EBCA567918 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          190        194         31              189
LISTA_NOMBRES                  8E79793E4DC58074853111EBCA567918 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         188        194         17              184

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  8E79793E4DC58074853111EBCA567918 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          187        193         17              186
PTAMANO                        3A3133E87FEA12D704D083CC7E39A42C FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          185        190         43              184
DATO                           671B71A5AD5E55B7C3161955D330364D ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        184        174         14              176
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL               183        172         53              182
LISTA_NOMBRES                  8E79793E4DC58074853111EBCA567918 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         182        172          8              180
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          181        172         23              180
LISTA_NOMBRES                  8E79793E4DC58074853111EBCA567918 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        180        172          8              176
RECORD_CONSULTACOLORTAMANO     19EAB630DABD2ACCFF03C7FB207670F5 NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          179        171         12              176
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          178        170         44              177
PTAMANO                        3A3133E87FEA12D704D083CC7E39A42C FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        177        170         33              176
CONSULTA_X_TAMANNO             D39C75627E96BD3FFD04E0145DD59815 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION         176        170         14                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  B3003F18BBFF8471F58E1A2691DE7A3D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          175        167         16              147
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          173        165         68              162
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          171        165         55              162
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          169        165         42              162
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          167        165         28              162
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          165        165         17              162
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          163        164         67              162
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               162        164         56              159
LISTA_NOMBRES                  B3003F18BBFF8471F58E1A2691DE7A3D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          161        164         31              160
LISTA_NOMBRES                  B3003F18BBFF8471F58E1A2691DE7A3D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         159        164         17              155
LISTA_NOMBRES                  B3003F18BBFF8471F58E1A2691DE7A3D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          158        163         17              157

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PESPECIE                       71597431DB248C84D91EB81C5C8F7685 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          156        160         38              155
DATO                           A4911B34771F333F0E99F561A57D141E ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        155        146         14              147
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL               154        144         42              153
LISTA_NOMBRES                  B3003F18BBFF8471F58E1A2691DE7A3D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         153        144          8              151
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          152        144         23              151
LISTA_NOMBRES                  B3003F18BBFF8471F58E1A2691DE7A3D VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        151        144          8              147
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          150        142         61              147
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          149        142         45              148
PESPECIE                       71597431DB248C84D91EB81C5C8F7685 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        148        142         33              147
CONSULTA_X_ESPECIE             A8790EE93B1A4C3D66AE8A102B2D09BA FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION         147        142         14                1
LISTA_NOMBRES                  FF205F6C0C3A275309B5924008EAB430 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          146        139         16              118

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          144        137         68              133
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          142        137         55              133
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          140        137         42              133
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          138        137         28              133
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          136        137         17              133
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          134        136         67              133
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               133        136         56              130
LISTA_NOMBRES                  FF205F6C0C3A275309B5924008EAB430 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          132        136         31              131
LISTA_NOMBRES                  FF205F6C0C3A275309B5924008EAB430 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         130        136         17              126
LISTA_NOMBRES                  FF205F6C0C3A275309B5924008EAB430 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          129        135         17              128
PGENERO                        3F28805599B529722486FD2D2B33EA1C FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          127        132         38              126

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           2237809C876BEBAF7D70C90240E9482A ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        126        118         14              118
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL               125        116         42              124
LISTA_NOMBRES                  FF205F6C0C3A275309B5924008EAB430 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         124        116          8              122
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          123        116         23              122
LISTA_NOMBRES                  FF205F6C0C3A275309B5924008EAB430 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        122        116          8              118
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          121        114         59              118
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          120        114         43              119
PGENERO                        3F28805599B529722486FD2D2B33EA1C FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION        119        114         32              118
CONSULTA_X_GENERO              E753FBE5A745933D56C371E1AD0D5CBF FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION         118        114         14                1
LISTA_NOMBRES                  0C489F46B1D089530FF3A606BD4DAC04 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          117        111         16               89
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          107        109         17              104

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          105        108         67              104
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL               104        108         56              101
LISTA_NOMBRES                  0C489F46B1D089530FF3A606BD4DAC04 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          103        108         31              102
LISTA_NOMBRES                  0C489F46B1D089530FF3A606BD4DAC04 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT         101        108         17               97
LISTA_NOMBRES                  0C489F46B1D089530FF3A606BD4DAC04 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          100        107         17               99
PFAMILIA                       182B1808F4CC73FFDF69E6CE28E5549F FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           98        104         37               97
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         97         90         14               89
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL                96         88         42               95
LISTA_NOMBRES                  0C489F46B1D089530FF3A606BD4DAC04 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT          95         88          8               93
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           94         88         23               93
LISTA_NOMBRES                  0C489F46B1D089530FF3A606BD4DAC04 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         93         88          8               89

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           92         86         61               89
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           91         86         45               90
PFAMILIA                       182B1808F4CC73FFDF69E6CE28E5549F FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         90         86         33               89
CONSULTA_X_FAMILIA             B131AB080BFFC01133DC5EEF8E6C0197 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION          89         86         14                1
LISTA_NOMBRES                  DE5F194326F7D8E8C119AA745ECB3DC4 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           88         83         16               60
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           86         81         68               75
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           84         81         55               75
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           82         81         42               75
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           80         81         28               75
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           78         81         17               75
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           76         80         67               75

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL                75         80         56               72
LISTA_NOMBRES                  DE5F194326F7D8E8C119AA745ECB3DC4 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           74         80         31               73
LISTA_NOMBRES                  DE5F194326F7D8E8C119AA745ECB3DC4 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT          72         80         17               68
LISTA_NOMBRES                  DE5F194326F7D8E8C119AA745ECB3DC4 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           71         79         17               70
PSUBORDEN                      459DEE05DEA41172858DC87C5D6C7806 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           69         76         37               68
DATO                           8D4495A2DD827275A4253193098167B9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         68         62         14               60
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL                67         60         42               66
LISTA_NOMBRES                  DE5F194326F7D8E8C119AA745ECB3DC4 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT          66         60          8               64
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           65         60         23               64
LISTA_NOMBRES                  DE5F194326F7D8E8C119AA745ECB3DC4 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         64         60          8               60
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           63         58         64               60

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           62         58         47               61
PSUBORDEN                      459DEE05DEA41172858DC87C5D6C7806 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         61         58         34               60
CONSULTA_X_SUBORDEN            6174F73A2CEBF8ED156A72BD6B765B79 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION          60         58         14                1
LISTA_NOMBRES                  AD2EC479420BA19581CA4456688D92C6 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           59         55         16               31
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           57         53         68               46
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           55         53         55               46
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           53         53         42               46
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           51         53         28               46
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           49         53         17               46
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           47         52         67               46
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL                46         52         56               43

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  AD2EC479420BA19581CA4456688D92C6 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           45         52         31               44
LISTA_NOMBRES                  AD2EC479420BA19581CA4456688D92C6 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT          43         52         17               39
LISTA_NOMBRES                  AD2EC479420BA19581CA4456688D92C6 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           42         51         17               41
PORDEN                         4158861E16229EE3ADE05126EB597F05 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           40         48         38               39
DATO                           DDF9B2B984168519D961B787FFCD34C9 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         39         34         14               31
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  CALL                38         32         42               37
LISTA_NOMBRES                  AD2EC479420BA19581CA4456688D92C6 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT          37         32          8               35
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           36         32         23               35
LISTA_NOMBRES                  AD2EC479420BA19581CA4456688D92C6 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         35         32          8               31
RECORD_CONSULTA                6F748D72DF6F158F836268B86075BB4F NESTED TABLE       PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           34         30         58               31
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           33         30         41               32

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
PORDEN                         4158861E16229EE3ADE05126EB597F05 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         32         30         31               31
CONSULTA_X_ORDEN               0D55F12DC4D6EE92263F8EF09E77E609 FUNCTION           PAQ_CONSULTA                   PACKAGE BODY  DEFINITION          31         30         14                1
LISTA_NOMBRES                  01D7B6E9D1C9328265AB53A90BDD9011 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           30         27         16                2
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           28         25         68               17
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           26         25         55               17
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          115        109         68              104
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          113        109         55              104
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          111        109         42              104
DATO                           6072C7CA3632BDE8325D9D571FE61DC0 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE          109        109         28              104
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           24         25         42               17
NUMBER                         47BFC756469F1D97B6C84EF73A9C5D48 NUMBER DATATYPE    T_TOPREPORTES                  TYPE          REFERENCE           11          3         32               10

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
CANTIDAD                       E8554A98BD4C1E157949D0967A239347 VARIABLE           T_TOPREPORTES                  TYPE          DECLARATION         10          3         23                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_TOPREPORTES                  TYPE          REFERENCE            9          3          9                8
USUARIO                        B92842C6545ACEE670D46A05AD8183AC VARIABLE           T_TOPREPORTES                  TYPE          DECLARATION          8          3          1                1
VARCHAR                        6B7CC3BDACE4F62A858D20FF9DF00A73 SUBTYPE            T_TOPREPORTES                  TYPE          REFERENCE            7          2         69                6
SEGUNDO_APELLIDO               01DD37048746A1B1EA89E7D5FC88FEE9 VARIABLE           T_TOPREPORTES                  TYPE          DECLARATION          6          2         52                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_TOPREPORTES                  TYPE          REFERENCE            5          2         38                4
PRIMER_APELLIDO                5700D95F78495555BA3A375900B66FE6 VARIABLE           T_TOPREPORTES                  TYPE          DECLARATION          4          2         22                1
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE T_TOPREPORTES                  TYPE          REFERENCE            3          2          8                2
NOMBRE                         DD8A2222A6ED4D492E056085CCDEBCE1 VARIABLE           T_TOPREPORTES                  TYPE          DECLARATION          2          2          1                1
T_TOPREPORTES                  7EB7D9E36EA684891FB2C462E74A1DE4 OBJECT             T_TOPREPORTES                  TYPE          DECLARATION          1          1          6                0
T_TOPREPORTES                  7EB7D9E36EA684891FB2C462E74A1DE4 OBJECT             RECORD_TOPREPORTES             TYPE          REFERENCE            2          1         37                1

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
RECORD_TOPREPORTES             6B77687119C14EBCEEB9B5113C128E6B NESTED TABLE       RECORD_TOPREPORTES             TYPE          DECLARATION          1          1          6                0
VARCHAR2                       FEE1E7EB248ADD1FACE928CB4F8C0D50 CHARACTER DATATYPE PAQ_CONSULTA                   PACKAGE       REFERENCE            4          2         41                3
PCLASE                         1C519A93B9396651E9FE4E88B35B4D08 FORMAL IN          PAQ_CONSULTA                   PACKAGE       DECLARATION          3          2         31                2
CONSULTA_X_CLASE               D1DC1BAC827C30DE53B569E30177BAE9 FUNCTION           PAQ_CONSULTA                   PACKAGE       DECLARATION          2          2         14                1
PAQ_CONSULTA                   45EC846CDC6B08A003A5FA2EECAA3AE7 PACKAGE            PAQ_CONSULTA                   PACKAGE       DECLARATION          1          1          9                0
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           22         25         28               17
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           20         25         17               17
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           18         24         67               17
T_CONSULTA                     AC0BEC22A2F2603F5A39243B8ACCFB7E OBJECT             PAQ_CONSULTA                   PACKAGE BODY  CALL                17         24         56               14
LISTA_NOMBRES                  01D7B6E9D1C9328265AB53A90BDD9011 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           16         24         31               15
LISTA_NOMBRES                  01D7B6E9D1C9328265AB53A90BDD9011 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  ASSIGNMENT          14         24         17               10

NAME                           SIGNATURE                        TYPE               OBJECT_NAME                    OBJECT_TYPE   USAGE         USAGE_ID       LINE        COL USAGE_CONTEXT_ID
------------------------------ -------------------------------- ------------------ ------------------------------ ------------- ----------- ---------- ---------- ---------- ----------------
LISTA_NOMBRES                  01D7B6E9D1C9328265AB53A90BDD9011 VARIABLE           PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           13         23         17               12
PCLASE                         A36E6E66D75A7CF526F2CDCA468DFBD0 FORMAL IN          PAQ_CONSULTA                   PACKAGE BODY  REFERENCE           11         20         33               10
DATO                           3C6D483BDA74C03194C6F1824157B818 ITERATOR           PAQ_CONSULTA                   PACKAGE BODY  DECLARATION         10          6         17                2

2,412 filas seleccionadas. 

