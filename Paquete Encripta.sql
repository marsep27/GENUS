Create or replace package dbcrypt is
  Function encrypt( p_data IN VARCHAR2 ) Return RAW ;
  Function decrypt( p_data IN RAW ) Return VARCHAR2 ;
end dbcrypt;
/
 
Create or replace package body dbcrypt is
    
    encrypt_key varchar2(255) := 'AYTSVNGE';
      
    Function encrypt( p_data IN VARCHAR2 ) Return RAW
    IS
      passWord_encrypted RAW(2000);
      raw_key raw(8) := UTL_RAW.CAST_TO_RAW(encrypt_key);
      largo        INTEGER := LENGTH(p_data);
      entero        INTEGER;
    BEGIN
        entero := 8-MOD(largo, 8);
        passWord_encrypted := UTL_RAW.CAST_TO_RAW(p_data||RPAD(CHR(entero), entero, CHR(entero)));
        DBMS_OBFUSCATION_TOOLKIT.DESENCRYPT(
               INPUT          => passWord_encrypted,
               KEY            => raw_key,
               ENCRYPTED_DATA => passWord_encrypted);
        Return passWord_encrypted;
    END encrypt;
 
    Function decrypt( p_data IN RAW ) Return VARCHAR2
    IS  
        raw_key raw(8) := UTL_RAW.CAST_TO_RAW(encrypt_key);
        largo number;
        raw_codification raw(2000) := UTL_RAW.CAST_TO_RAW(UTL_RAW.CAST_TO_VARCHAR2(p_data));
        passWord_decrypted varchar2(2000);
    BEGIN
      DBMS_OBFUSCATION_TOOLKIT.DESDECRYPT(
               INPUT          =>  p_data,
               KEY            =>  raw_key,
               DECRYPTED_DATA =>  raw_codification);
      passWord_decrypted := UTL_RAW.CAST_TO_VARCHAR2(raw_codification);
      largo := LENGTH(passWord_decrypted);
      passWord_decrypted := RPAD(passWord_decrypted,largo-ASCII(SUBSTR(passWord_decrypted,largo)));
      Return passWord_decrypted;
    END decrypt;
End dbcrypt;
 /